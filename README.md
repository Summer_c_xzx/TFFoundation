#TFFoundation 时光流影IOS框架

## 使用
> pod 'TFFoundation', :git => 'https://gitee.com/Summer_c_xzx/TFFoundation.git'
> 
> 然后执行pod install即可，

## 框架目录

![框架结构图](Document/TFFoundation结构图.png)

#### 重要类的使用

##### TFStyle
> 样式定制类，可通过currentStyle类属性切换样式，默认样式定制如下：
> ![TFStyle](Document/TFStyle说明.png)

#### UIViewController+TFCore

> ViewController的类别，主要定制了VC间的跳转方式。
> > * 跳转一个VC，先要在AppViewControllerConfig.h文件中，@class xxxViewController;默认跳转方式为Push，也可以指定为Present方式
> > 
> > * 在需要跳转的地方：TFOpenVC(xxxViewController,params);
> > 
> > * 也可直接打开一个网址：TFOpenURL(@"https://www.baidu.com",nil);打开网址时会自动跳转TFWebViewController,
> > * 若想跳转其他APP，例如Safari浏览器打开一个网址，通过kTFOpenURLTypeKey指定跳转类型为TFOpenURLTypeAppURL即可，TFOpenURL(@"https://www.baidu.com",@{kTFOpenURLTypeKey:@(TFOpenURLTypeAppURL)});
> > 
> 参数一览：![TFFoundation使用教程截图1](Document/TFFoundation使用教程截图/TFFoundation使用教程截图1.png)

#### TFTableViewController

> 列表视图器，只需制定对应的请求的url和参数，以及列表对应的cell或者item，列表中的上拉刷新，下拉加载，数据处理等等已处理。
> 
> 结构图如下：
> 
> ![TFTableViewController说明](Document/TFTableViewController说明.png)

#### 其他

> * Category 中的非UI相关部分，请参考对应的单元测试文件
> 
> * 其余部分请参考项目中的demo演示 

#### 

## 项目以及文件模板

### 项目模板结构图

![项目模板结构图](Document/项目模板结构.png)

### 使用教程

> * 获取项目模板在AttachedFiles文件夹下，Templates.zip
> 
> * 解压缩到项目模板存放位置:/Users/你的用户名/Library/Developer/Xcode/Templates
> 
> ![项目模板使用教程](Document/项目模板教程截图/项目模板教程截图1.png)
> 
> * 新建项目时选择Templates下的TimefaceProject模板新建即可
> 
> ![项目模板使用教程](Document/项目模板教程截图/项目模板教程截图2.png)
> 
> * 项目创建好之后，因为项目模板无法添加文件夹索引，所以需要删除模板项目中的文件夹，重新添加索引。
> 
> ![项目模板使用教程](Document/项目模板教程截图/项目模板教程截图3.png)
> 
> ![项目模板使用教程](Document/项目模板教程截图/项目模板教程截图4.png)
> 
> * 通过命令行cd到对应的项目目录下，执行pod install即可
>  
> > * AppBaseModel.h文件中，配置你的基础数据模型，以及真正返回数据的路径
>
> > * AppInterfaceConfig.m文件中，配置所有请求的基地址
> 
> > * AppBaseRequest.m文件中，配置你的请求信息，比如头部参数等等
> 
> > * AppDefaultTableDataRequest.m文件中，修改列表请求对应的参数和返回字段，例如页数、每页数量等
> 
> > * AppDefaultStyle.m 中，修改对应的全局样式和某些样式定制

> > Tips：以上这些，已在项目模板中写了警告。具体可参考《项目模板示例项目》文件夹中的示例项目
> > 

#### Tips:

> * 项目模板主要用来快速新建项目，此项目模板创建时只需pod install 即可完成项目的建立，项目中会包含常用的工具类和项目分类的文件夹。
> 
> * 项目模板的pod Profile文件中主要包含框架：
> > * [**TFTableViewManager** 将Model映射为Item快速创建TableView工具类](https://gitee.com/Summer_c_xzx/TFTableViewManager.git) 
> > * [**TFPhotoBrowser** 大图预览和本地相册的预览以及UI定制，以及指定区域的图片剪裁](https://gitee.com/Summer_c_xzx/TFPhotoBrowser.git) 
> > * [**TFFoundation** 基础框架](https://gitee.com/Summer_c_xzx/TFFoundation.git) 

#附：

### Custom Code Block 自定义代码块

> * [方式1->系统自带](http://www.jianshu.com/p/9409d3310fba)
> * 方式2-> Dash中的SNIPPETS功能，效果如下
> 
> ![Dash使用教程](Document/Dash使用教程/Dash使用教程.gif)
> 
> > * 安装Dash：[点击去下载](https://kapeli.com/dash)
> > 
> > * 将《AttachedFiles》目录下《Dash模板》中的library.dash，移动到电脑路径：/Users/你的用户名/Library/Application Support/Dash,替换原有的library.dash，即可使用。


## CodeStyle 代码规范

## Naming 命名
总的来说, iOS命名两大原则是:

1.可读性高和防止命名冲突(通过加前缀来保证). Objective-C 的命名通常都比较长, 名称遵循驼峰式命名法. 一个好的命名标准很简单, 就是做到在开发者一看到名字时, 就能够懂得它的含义和使用方法。

2.每个模块都要加上自己的前缀, 前缀在编程接口中非常重要, 可以区分软件的功能范畴并防止不同文件或者类之间命名发生冲突。

## Naming Rule 命名规则

文件名都应以'项目名称'+'功能描述'+'类的类型' 命名，首字母大写。

### BaseClass 基类

所有被普遍继承的类，应以'项目名'+'Base'+'类别' 命名

如：TFBaseRequest;

### ViewController 视图控制器

以'项目名称'+'功能描述'+'ViewController'命名

如：TFLoginViewController;

### Model 数据模型

以'项目名称'+'功能描述'+'Model'命名

如：TFUserModel;

### Singleton 单例

以'项目名称'+'功能描述'+'Manager'命名

如：TFUserManager;

### Utility class 工具类

以'项目名称'+'功能描述'+'Helper'命名

如：TFCityHelper;


## General contract 通用的约定

尽可能遵守 Apple 的命名约定,可多查看Apple的官方document。

> 定义指针类型的变量，`*` 应该放在变量前。

```Objective-C
// 正确：
UIButton *settingsButton;

// 错误：
UIButton * settingsButton;
UIButton* settingsButton;
```

> 推荐使用长的、描述性的方法和变量名

```Objective-C
// 正确：
UIButton *settingsButton;

// 错误：
UIButton *setBut;
```

> 多个赋值语句间应以"="对齐

```Objective-C
// 正确：
BOOL nameContainsSwift  = [sessionName containsString:@"Swift"];
BOOL isCurrentYear      = [sessionDateCompontents year] == 2014;
BOOL isSwiftSession     = nameContainsSwift && isCurrentYear;

// 错误：
BOOL nameContainsSwift = [sessionName containsString:@"Swift"];
BOOL isCurrentYear = [sessionDateCompontents year] == 2014;
BOOL isSwiftSession = nameContainsSwift && isCurrentYear;
```

## Constant 常量和宏

> 常量应该以驼峰法命名，并以k+相关类名作为前缀。

 ```Objective-C
// 正确：
static const NSTimeInterval kLoginViewControllerGetCaptchaTime = 60;

// 错误：
static const NSTimeInterval loginViewControllerGetCaptchaTime = 60;
```

> 宏应该全部大写，并以`_`分割

```Objective-C
// 正确：
#define BASE_URL @"timeface.com"

// 错误：
#define baseUrl @"timeface.com"
```


## Function 方法

让方法的命名像一句话，即见名知意

> 方法名与方法类型 (-/+ 符号)之间应该以空格间隔;

```Objective-C
// 正确：
- (NSComparisonResult)compare:(NSString *)string options:(NSStringCompareOptions)mask;

// 错误：
-(NSComparisonResult)compare:(NSString *)string options:(NSStringCompareOptions)mask;

//注：方法段之间也应该以空格间隔（以符合 Apple 风格）。
```

> 格式沿用驼峰，首字母小写

```Objective-C
正确：- (nullable instancetype)initWithContentsOfURL:(NSURL *)url encoding:(NSStringEncoding)enc;

错误：- (nullable instancetype)initwithcontentsOfURL:(NSURL *)url Encoding:(NSStringEncoding)enc;

注：参数前应该总是有一个描述性的关键词
```

> 方法实现时，参数过长则每个参数用一行，以冒号对齐。

```Objective-C
// 正确：
+ (NSStringEncoding)stringEncodingForData:(NSData *)data
                          encodingOptions:(NSDictionary *)opts
                          convertedString:(NSString *)string
                      usedLossyConversion:(BOOL *)usedLossyConversion;

// 错误：
+ (NSStringEncoding)stringEncodingForData:(NSData *)data encodingOptions:(NSDictionary *)opts convertedString:(NSString *)string usedLossyConversion:(BOOL *)usedLossyConversion;
```

> 私有方法不允许外界访问的方法，在方法前面加标识符

```Objective-C
// 正确：
- (void)_layout;

// 错误：
- (void)layout;
```

## Property 属性


> 属性应该尽可能描述性地命名，避免缩写，并且是小写字母开头的驼峰命名。

```Objective-C
// 正确：
@property (nonatomic, readonly, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIButton *loginButton;

// 错误：
@property (nonatomic, readonly, strong) UIScrollView *ScrollView;

@property (nonatomic, strong) UIButton *loginBut;
```

## Instance Variable 实例变量

> 实例变量应以`_`开头

```Objective-C
// 正确：
    NSString *_currentPageNum;

// 错误：
    NSString *currentPageNum;
```

## Enum 枚举

> 当使用 `enum` 的时候，建议使用苹果的枚举声明，因为它有更强大的类型检查和代码补全

```Objective-C
// 正确：
typedef NS_ENUM(NSInteger, UIViewAnimationTransition) {

      UIViewAnimationTransitionNone,

      UIViewAnimationTransitionFlipFromLeft,

      UIViewAnimationTransitionFlipFromRight,

      UIViewAnimationTransitionCurlUp,

      UIViewAnimationTransitionCurlDown,

};

//注：枚举类型命名要加相关类名前缀并且枚举值命名要加枚举类型前缀.

// 错误：
typedef enum : {

      UIViewAnimationTransitionNone,

      UIViewAnimationTransitionFlipFromLeft,

      UIViewAnimationTransitionFlipFromRight,

      UIViewAnimationTransitionCurlUp,

      UIViewAnimationTransitionCurlDown,

} UIViewAnimationTransition;

//注：这是C语言的枚举类型定义，对与oc来说是可以兼容c，但是此方法不能指定底层数据类型
```

## conditional statement 条件语句

> 条件语句体应该总是被大括号包围。

```Objective-C
// 正确：
if (!error) {
    return success;
}
// 错误：
if (!error)
    return success;

if (!error) return success;

// 注：尽管有时候你可以不使用大括号（比如，条件语句体只有一行内容），但是这样做会带来问题隐患。比如，增加一行代码时，你可能会误以为它是 if 语句体里面的。此外，更危险的是，如果把 if 后面的那行代码注释掉，之后的一行代码会成为 if 语句里的代码。    
```

## nil 和 BOOL 检查

> 不要直接把它和 YES或者NO 比较

```Objective-C
// 正确：
if (someObject) {
  ...// do something.
}

if (![someObject boolValue]) {
   ...// do something.
}

if (!someObject) {
  ...// do something.
}

// 错误：
if (someObject == YES) {
  ... // Wrong
}

if (myRawValue == YES) {
  ... // Never do this.
}

if ([someObject boolValue] == NO) {
  ... // do something.
}

// 注：因为 nil 是 解释到 NO，所以没必要在条件语句里面把它和其他值比较。同时，不要直接把它和 YES 比较，因为 YES 的定义是 1， 而 BOOL 是 8 bit的，实际上是 char 类型。同时这样也能提高一致性，以及提升可读性。
```

## Literals 字面值

> 使用字面值来创建不可变的 NSString, NSDictionary, NSArray, 和 NSNumber 对象，可用简写

```Objective-C
// 正确：
NSArray *names = @[@"Brian", @"Matt", @"Chris", @"Alex", @"Steve", @"Paul"];
NSDictionary *productManagers = @{@"iPhone" : @"Kate", @"iPad" : @"Kamal", @"Mobile Web" : @"Bill"};
NSNumber *shouldUseLiterals = @YES;
NSNumber *buildingZIPCode = @10018;

// 错误：
NSArray *names = [NSArray arrayWithObjects:@"Brian", @"Matt", @"Chris", @"Alex", @"Steve", @"Paul", nil];
NSDictionary *productManagers = [NSDictionary dictionaryWithObjectsAndKeys: @"Kate", @"iPhone", @"Kamal", @"iPad", @"Bill", @"Mobile Web", nil];
NSNumber *shouldUseLiterals = [NSNumber numberWithBool:YES];
NSNumber *buildingZIPCode = [NSNumber numberWithInteger:10018];

// 注：如果要用到这些类的可变副本，我们推荐使用 NSMutableArray, NSMutableString 这样的类。
// 应该避免下面这样:
// NSMutableArray *aMutableArray = [@[] mutableCopy];
// 上面这种书写方式的效率和可读性的都存在问题。
// 效率方面，一个不必要的不可变对象被创建后立马被废弃了；虽然这并不会让你的 App 变慢（除非这个方法被频繁调用），但是确实没必要为了少打几个字而这样做。
// 可读性方面，存在两个问题：第一个问题是当你浏览代码并看见 @[] 的时候，你首先联想到的是 NSArray 实例，但是在这种情形下你需要停下来深思熟虑的检查；另一个问题是，一些新手以他的水平看到你的代码后可能会对这是一个可变对象还是一个不可变对象产生分歧。他/她可能不熟悉可变拷贝构造的含义（这并不是说这个知识不重要）。当然，不存在绝对的错误，我们只是讨论代码的可用性（包括可读性)。
```

## Code annotations 代码注释

代码的注释经常被人忽略，以至于在后期维护的时候较为困难。所以制定一套规范的注释体系，致力于达到就算维护人员改变也能快速上手的效果。
代码注释主要分为以下几种：属性注释、方法集注释、方法注释、普通注释（单行注释）。

## SingleLine annotations 单行注释

在很多时候只需写一个简要描述就够了，这时最好使用单行注释。单行注释分为以下两种：`//`和`///`。
其中`///`相较`//`注释的优点是此属性可以在后面的引用时，在智能提示的下方显示对应的注释内容。

> 示例

```Objective-C
/// 枚举类型属性示例.
@property (nonatomic, assign) TestEnumType* myType;

// 创建一个请求.
NSURLRequest *request = [NSURLRequest requestWithURL:url];
// 注：文本最好统一以英文句号（.）结尾。这样做有助于代码阅读，明确地得知该段文本已经结束，而且有助于避免乱码时的换行符丢失问题。
```

## Multiple Lines annotations 多行注释

当需要写详细描述时，这时就需要使用多行注释了。

> 示例

```Objective-C
/** 简要描述.
 *
 * 详细描述或其他.
 */
 - (id)testMethordComments;
// 注：当我们在使用appledoc生成文档时，默认会将注释中的第一段识别为简要描述，注释中的所有内容即第一段+后面内容识别为详细描述，所以为了避免生成的文档注释内容重复应采用以下形式。

/**
 * @brief 简要描述.
 *
 * 详细描述或其他.
 */
 - (id)testMethordComments;

// 注：
// 1.appledoc对@brief指令的支持存在缺陷——@brief不能出现类、协议的注释中，会导致后续内容丢失。 @brief多行注释仅能安全的用在属性、方法的注释中。
// 2.多行注释存在“段”的概念，以内容为空的行作为分段依据。如果没有空行隔开的话，会将连续有内容的行连接起来组成一段.
// 3.如果省略中间各行行首的星号（*），appledoc也能识别。但考虑到注释缩进、美观性、兼容性，还是建议不要省略行首星号。
```

## Class annotations 类注释


> 对于类（协议、分类）来说，一般只需要写简要描述就行了，这时可以使用单行注释。

```Objective-C
/// 文档A.
@interface DocA : NSObject {

}
@end
```

> 当需要留下详细描述时，可换成多行注释

```Objective-C
/** 文档B.
 *
 * 文档B的详细描述.
 */
@interface DocB : NSObject {

}
@end
```

## Enum annotations 枚举注释

对于枚举而言其注释格式与类注释类似。

> 单行注释

```Objective-C
/// 注释枚举.
typedef NS_ENUM(NSInteger, TestEnumType) {
    TestEnumType1,///<枚举Type1
    TestEnumType2,///<枚举Type2
};
// 注：给枚举中的元素加注释时，应使用 `///<`，作为行尾注释，或者使用 `///`作为行前注释

/// 注释枚举.
typedef NS_ENUM(NSInteger, TestEnumType) {
    /// 枚举Type1
    TestEnumType1,
    /// 枚举Type2
    TestEnumType2,
};
```

> 多行注释

```Objective-C
/** 注释枚举.
 *
 * 注释枚举的详细描述.
 */
typedef NS_ENUM(NSInteger, TestEnumType) {
    TestEnumType1,///<枚举Type1
    TestEnumType2,///<枚举Type2
};
```


## Property annotations 属性注释

对于属性来说，本来使用行尾注释是最好的，能使内容更加紧凑。可惜目前appledoc不支持行尾注释。只好退而求其次，选择单行注释了。

> 单行注释

```Objective-C
/// 数值属性.
@property (nonatomic,assign) NSInteger num;
```

> 当需要留下详细描述时，可换成多行注释

```Objective-C
/**
 * @brief 字符串属性.
 *
 * 属性的详细描述.
 */
@property (nonatomic,strong) NSString* str;
```


## Function annotations 方法注释

> 对于没有参数、返回值的简单方法，可以使用单行注释。

```Objective-C
/// 简单方法.
- (void)someMethod;
```

> 若方法具有参数或返回值，这时就得使用多行注释了

```Objective-C
/**
 * @brief 带整数参数的方法.
 *
 * @param  value 值.
 *
 * @return 返回value.
 */
- (int)someMethodByInt:(int)value;

// appledoc 指令说明：
// @param <name> <description>: 参数描述.
// @return <description>: 返回值描述.

// 在某些时候，我们还需要在方法注释种填写异常、参见、警告 等信息

/**
 * @brief 带字符串参数的方法.
 *
 * @param  value 值.
 *
 * @return 返回value.
 *
 * @exception NSException 可能抛出的异常.
 *
 * @see someMethod
 * @see someMethodByInt:
 * @warning 警告: appledoc中显示为蓝色背景.
 * @bug 缺陷: appledoc中显示为黄色背景.
 */
- (NSString*)someMethodByStr:(NSString*)value;

// appledoc 指令说明：
// @exception <name> <description>: 异常描述.
// @see <name>: 参见.
// @warning <text>: 警告.
// @bug <text>: 警告.

// 注：参见指令的格式为：
// @see <name>
// @sa <name>
// <name>可为：
// 1.当前类（或协议）中的属性或方法。（注意Objective-C方法签名的写法，一般为“方法名:参数1:参数2:⋯⋯”的格式）
// 2.类（或协议）名。（注意appledoc不支持当前类）
```

## Appledoc支持的排版格式


## Code Text 代码文本

> 有时需要在一段话中引入一小段代码，这时可以用重音符 "\`" 将那一段代码给包起来。

```Objective-C
/**
 * 引用短代码, 如 `someMethodByStr:`.
 */
```

## Code Block 代码块

> 代码块适用于需要在注释中放置多行代码的情况。具体办法是在每行内容的前面加一个tab字符

```Objective-C
/**
 * 示例代码:
 *
 *     int sum=0;
 *     for(int i=1; i<=10; ++i) {
 *         sum += i;
 *     }
 */
```

## Table 列表

> 在内容的每一行开头使用“-”、“+”或“\*”字符，可创建无序列表。

```Objective-C
/**
 * 无序列表:
 *
 * - abc
 * - xyz
 * - rgb
 */
```

> 使用数字与小数点，可创建有序列表。

```Objective-C
/**
 * 有序列表:
 *
 * 1. first.
 * 2. second.
 * 3. third.
 */
```

> 使用tab字符配合使用无序列表或多级列表，可创建多级列表。

```Objective-C
/**
 * 多级列表:
 *
 * - xyz
 *    - x
 *    - y
 *    - z
 * - rgb
 *    - red
 *        1. first.
 *            1. alpha.
 *            2. beta.
 *        2. second.
 *        3. third.
 *    - green
 *    - blue
 */
```

## Link 链接

链接分为两种形式：

1、直接链接。格式为 <link>。会将链接地址直接作为文本来显示。

2、文本链接。格式为 [text](<link>)。使用自定义的文本作为链接名。

> 在注释中直接写上url便会自动创建链接。

```Objective-C
/**
 * http://appledoc.gentlebytes.com/ : 直接写url链接.
 */
```

> 在注释中直接写上类（或协议）名，并注意左右两侧留空格，appledoc便会自动生成指向该类（或协议）的链接。

```Objective-C
/**
 * DocA : 类.
 */
 ```


