//
//  AppInitManager.m
//  TestTemplateProject
//
//  Created by Summer on 2018/6/9.
//Copyright © 2018年 Summer. All rights reserved.
//

#import "AppInitManager.h"
//style
#import "AppUIStyle.h"

@implementation AppInitManager

+ (void)setupAppConfigWithApplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    //设置appStyle
    TFUISetCurrentStyle([[AppUIStyle alloc] init]);
    //设置rootVC
    [self resetRootViewControllerWithAnimated:NO completion:nil];
    //配置第三方
    [self p_initVendors];
}

+ (void)resetRootViewControllerWithAnimated:(BOOL)animated completion:(void (^)(BOOL))completion{
    UIWindow *window = [UIApplication sharedApplication].tf_currentWindow;
    //root vc
    
}

+ (void)p_initVendors {
    
}

@end
