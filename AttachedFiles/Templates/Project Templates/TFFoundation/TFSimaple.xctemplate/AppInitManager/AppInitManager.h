//
//  AppInitManager.h
//  TestTemplateProject
//
//  Created by Summer on 2018/6/9.
//Copyright © 2018年 Summer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppInitManager : NSObject

/**
 配置App常用的配置
 */
+ (void)setupAppConfigWithApplication:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

/**
 重设rootVC
 
 @param animated 是否使用动画
 @param completion 完成的block
 */
+ (void)resetRootViewControllerWithAnimated:(BOOL)animated completion:(void (^)(BOOL finished))completion;

@end
