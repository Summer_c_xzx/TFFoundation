//
//  AppUserManager.m
//  TestTemplateProject
//
//  Created by Summer on 2018/6/10.
//  Copyright © 2018年 Summer. All rights reserved.
//

#import "AppStorageManager.h"
//util
#import "AppInitManager.h"
#import <YYModel/YYModel.h>

static NSString * const kAppUserModelSaveKey = @"AppUserModelSave";

@implementation AppStorageManager

+ (AppUserModel *)currentUserModel {
    return [AppUserModel yy_modelWithJSON:[[NSUserDefaults standardUserDefaults] objectForKey:kAppUserModelSaveKey]];
}

+ (BOOL)saveUserModel:(AppUserModel *)userModel {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[userModel yy_modelToJSONObject] forKey:kAppUserModelSaveKey];
    return [userDefaults synchronize];
}

+ (BOOL)saveUserModel:(AppUserModel *)userModel andResetRootVCWithAnimated:(BOOL)animated {
    BOOL success = [AppStorageManager saveUserModel:userModel];
    [AppInitManager resetRootViewControllerWithAnimated:animated completion:nil];
    return success;
}

+ (BOOL)deleteUserModel {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:kAppUserModelSaveKey];
    return [userDefaults synchronize];
}

+ (BOOL)deleteUserModelAndResetRootVCWithAnimated:(BOOL)animated {
    BOOL success = [AppStorageManager deleteUserModel];
    [AppInitManager resetRootViewControllerWithAnimated:animated completion:nil];
    return success;
}

@end
