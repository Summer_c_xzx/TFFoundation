//
//  AppUserManager.h
//  TestTemplateProject
//
//  Created by Summer on 2018/6/10.
//  Copyright © 2018年 Summer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppUserModel.h"

@interface AppStorageManager : NSObject

/**
 当前用户模型
 
 @return IEUserModel
 */
+ (AppUserModel *)currentUserModel;

/**
 保存用户模型
 
 @param userModel 用户模型
 @return 是否保存成功
 */
+ (BOOL)saveUserModel:(AppUserModel *)userModel;

/**
 保存用户模型并更新rootVC
 
 @param userModel 用户模型
 @param animated 是否动画
 @return 是否保存成功
 */
+ (BOOL)saveUserModel:(AppUserModel *)userModel andResetRootVCWithAnimated:(BOOL)animated;

/**
 删除用户模型
 
 @return 是否删除成功
 */
+ (BOOL)deleteUserModel;

/**
 删除用户模型
 
 @param animated 是否动画
 @return 是否删除成功
 */
+ (BOOL)deleteUserModelAndResetRootVCWithAnimated:(BOOL)animated;

@end
