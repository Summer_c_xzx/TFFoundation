//
//  AppBaseModel.m
//  Template002
//
//  Created by Summer on 2018/1/8.
//Copyright © 2018年 Summer. All rights reserved.
//

#import "AppBaseModel.h"

#warning 1.配置你的基础数据模型和真正有用的数据返回路径

NSString * const kAppBaseModelResponseObjectPath = @"data";

NSString * const kAppTableDataResponseObjectPath = @"data.list";

@implementation AppBaseModel

- (BOOL)status {
    return (self.code==200);
}

@end
