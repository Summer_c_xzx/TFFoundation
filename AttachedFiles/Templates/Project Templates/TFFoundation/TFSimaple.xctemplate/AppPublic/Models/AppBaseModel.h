//
//  AppBaseModel.h
//  Template002
//
//  Created by Summer on 2018/1/8.
//Copyright © 2018年 Summer. All rights reserved.
//

#import <Foundation/Foundation.h>


///<app默认真正有用的数据的返回路径，如返回对象为如下格式：@{@"status":@1,@"msg":@"操作正确",@"code":@0,@"data":@{@"userName":@"xxx"}};返回的结果在data在，那么此时path值就是data
extern NSString * const kAppBaseModelResponseObjectPath;

extern NSString * const kAppTableDataResponseObjectPath;

@interface AppBaseModel : NSObject

/**
 状态 0错误 1正确
 */
@property (nonatomic, assign, readonly) BOOL status;

/**
 信息描述
 */
@property (nonatomic, copy) NSString *msg;

/**
 错误编码
 */
@property (nonatomic, assign) NSInteger code;

@end
