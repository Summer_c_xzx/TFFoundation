//
//  AppUIStyle.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/20.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import <TFFoundation/TFUIStyle.h>
#import <TFFoundation/TFUIStateView.h>

NS_ASSUME_NONNULL_BEGIN

#define APPSTYLE TFUICurrentStyle()

typedef NS_ENUM(NSUInteger, AppUIPopoverType) {
    AppUIPopoverTypeToast = 0,
    AppUIPopoverTypeStatus,
    AppUIPopoverTypeSuccess,
    AppUIPopoverTypeFail,
};

//popOver
void AppUIShowPopoverView(AppUIPopoverType popoverType, NSString *text);
void AppUIShowPopoverViewInView(AppUIPopoverType popoverType, NSString *text, __kindof UIView *view);
void AppUIDismissPopoverView(void);
void AppUIDismissPopoverViewInView(__kindof UIView *view);

//stateView
void AppUIAddStateViewInView(__kindof TFUIStateView * _Nonnull stateView, UIView * _Nonnull view);
void AppUIRemoveStateViewInView(UIView * _Nonnull view);


@interface AppUIStyle : TFUIStyle



@end

NS_ASSUME_NONNULL_END
