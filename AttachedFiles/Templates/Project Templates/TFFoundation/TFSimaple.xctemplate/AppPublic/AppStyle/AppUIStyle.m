//
//  AppUIStyle.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/20.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "AppUIStyle.h"
#import <MBProgressHUD/MBProgressHUD.h>

void AppUIShowPopoverView(AppUIPopoverType popoverType, NSString *text) {
    AppUIShowPopoverViewInView(popoverType, text, [UIApplication sharedApplication].tf_currentWindow);
}

void AppUIShowPopoverViewInView(AppUIPopoverType popoverType, NSString *text, __kindof UIView *view) {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.removeFromSuperViewOnHide = YES;
    MBProgressHUDMode mode = MBProgressHUDModeCustomView;
    NSString *imageName = nil;
    BOOL autoDismiss = NO;
    switch (popoverType) {
        case AppUIPopoverTypeStatus:
            mode = MBProgressHUDModeIndeterminate;
            break;
        case AppUIPopoverTypeToast:
            mode = MBProgressHUDModeText;
            autoDismiss = YES;
            break;
        case AppUIPopoverTypeSuccess: {
            mode = MBProgressHUDModeCustomView;
            if (text.length>15) {
                mode = MBProgressHUDModeText;
            }
            imageName = @"public_load_success";
            autoDismiss = YES;
        }
            break;
        case AppUIPopoverTypeFail: {
            MBProgressHUDMode mode = MBProgressHUDModeCustomView;
            if (text.length>15) {
                mode = MBProgressHUDModeText;
            }
            imageName = @"public_load_fail";
            autoDismiss = YES;
        }
            break;
        default:
            break;
    }
    
    hud.mode = mode;
    hud.bezelView.blurEffectStyle = UIBlurEffectStyleExtraLight;
    hud.animationType = MBProgressHUDAnimationZoom;
    hud.label.text = text;
    hud.label.font = [UIFont systemFontOfSize:17];
    hud.label.numberOfLines = 0;
    if (hud.mode == MBProgressHUDModeCustomView) {
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    }
    if (autoDismiss) {
        [hud hideAnimated:YES afterDelay:1.5f];
    }
}

void AppUIDismissPopoverView(void) {
    AppUIDismissPopoverViewInView([UIApplication sharedApplication].tf_currentWindow);
}

void AppUIDismissPopoverViewInView(__kindof UIView *view) {
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[MBProgressHUD class]]) {
            [subView removeFromSuperview];
        }
    }
}


void AppUIAddStateViewInView(__kindof TFUIStateView * stateView, UIView *view) {
    [view addSubview:stateView];
    [view bringSubviewToFront:stateView];
    if ([view isKindOfClass:[UIScrollView class]]) {
        [stateView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.mas_equalTo(0.0);
            make.width.mas_equalTo(view.mas_width);
            make.height.mas_equalTo(view.mas_height);
        }];
    }
    else {
        [stateView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsZero);
        }];
    }
}


void AppUIRemoveStateViewInView(UIView *view) {
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[TFUIStateView class]]) {
            [subView removeFromSuperview];
        }
    }
}


@implementation AppUIStyle

#warning 3.修改对应的全局样式和某些样式定制

- (void)updateStyle {
    [super updateStyle];
    [[NSNotificationCenter defaultCenter] addObserver:TFUICurrentStyle() selector:@selector(p_viewDidLoadNoti:) name:kTFViewControllerViewDidLoadNotification object:nil];
}

- (void)p_viewDidLoadNoti:(NSNotification *)noti {
    UIViewController *vc = noti.object;
    NSBundle *viewControllerBundle = [NSBundle bundleForClass:[vc class]];
    if ([viewControllerBundle.bundlePath isEqualToString:[NSBundle mainBundle].bundlePath]) {
        vc.view.backgroundColor = APPSTYLE.backgroundColor;
        NSString *backTitle = APPSTYLE.barButtonBackItemTitle;
        if (backTitle) {
            vc.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:backTitle style:UIBarButtonItemStylePlain target:nil action:nil];
        }
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    TFLog(@"");
}

@end
