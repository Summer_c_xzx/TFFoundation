//
//  AppBaseTableViewController.h
//  Template002
//
//  Created by Summer on 2018/1/8.
//Copyright © 2018年 Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
//util
#import <TFTableViewManager/TFTableViewManagerKit.h>
#import "AppTableDataRequest.h"


/**
 基础列表VC，可实现：
 配置请求之后，可自动处理数据的加载和刷新，以及错误的处理
 */
@interface AppBaseTableViewController : UIViewController

/**
 UITableView
 */
@property (nonatomic , strong, readonly) UITableView *tableView;

/**
 UITableView的样式
 */
@property(nonatomic, assign) UITableViewStyle tableViewStyle;

/**
 tableViewManager模式下的tableviewManager
 */
@property (nonatomic , strong, readonly) TFTableViewManager *tableViewManager;

/**
 基础请求类
 */
@property (nonatomic , strong, readonly) AppTableDataRequest *request;

/**
 是否一个section对应一个model，默认为NO，则一个row对应一个model.
 */
@property(nonatomic, assign) BOOL isSectionForModel;

/**
 开始请求数据
 */
- (void)startRequestData;
- (void)reloadData;

/**
 TableViewManager中的ItemClass
 
 @return Class
 */
- (Class)tableViewManagerItemClass;

/**
 Manager的中section被创建时调用此方法，子类可通过此方法修改section的footer等。
 
 @param section TFTableViewSection
 */
- (void)tableViewManagerSectionDidInit:(TFTableViewSection *)section;

/**
 Manager中item被创建时调用此方法，子类可通过此方法修改item中cellHeight等
 
 @param item TFTableViewItem
 */
- (void)tableViewManagerItemDidInit:(__kindof TFTableViewItem *)item;

/**
 选中某个item
 
 @param item item
 @param indexPath NSIndexPath
 */
- (void)tableViewManagerDidSelectItem:(__kindof TFTableViewItem *)item atIndexPath:(NSIndexPath *)indexPath;


- (TFUIStateView *)emptyStateView;///<空白占位图
- (TFUIStateView *)errorStateViewWithError:(NSError *)error;///<错误占位图


@end
