//
//  AppTableDataRequest.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/20.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "AppBaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppTableDataRequest : AppBaseRequest

@property (nonatomic, strong) NSMutableDictionary *params;

@property(nonatomic, assign) NSInteger pageNumber;///<页数

@property(nonatomic, assign, readonly) BOOL isFirstPage;///<是否第一页

@property(nonatomic, assign, readonly) NSInteger firstPageNumber;///<pageNumber从几开始，默认从1开始

@property(nonatomic, assign) NSInteger pageSize;///<页大小

@property(nonatomic, assign, readonly) NSInteger totalPage;///<总页数

@property(nonatomic, strong, readonly) NSArray *listArray;///<列表

@end

NS_ASSUME_NONNULL_END
