//
//  AppTableDataRequest.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/20.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "AppTableDataRequest.h"
//model
#import <YYModel/YYModel.h>
#import "AppBaseModel.h"

@implementation AppTableDataRequest

@synthesize params = _params;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.params = [NSMutableDictionary dictionary];
        self.pageNumber = self.firstPageNumber;
        self.pageSize = 20;
        self.responseObjectPath = kAppTableDataResponseObjectPath;
        self.requestMethod = TFRequestMethodGet;
    }
    return self;
}

- (void)setPageSize:(NSInteger)pageSize {
    _pageSize = pageSize;
    [self.params setObject:@(pageSize) forKey:@"pageSize"];
}

- (void)setPageNumber:(NSInteger)pageNumber {
    _pageNumber = pageNumber;
    [self.params setObject:@(pageNumber) forKey:@"pageNum"];
}

- (NSInteger)totalPage {
    NSDictionary *dataDic = self.responseObject[kAppBaseModelResponseObjectPath];
    if (dataDic[@"totalPage"]) {
        return [dataDic[@"totalPage"] integerValue];
    }
    return 0;
}

- (BOOL)isFirstPage {
    return (self.pageNumber == self.firstPageNumber);
}

- (NSInteger)firstPageNumber {
    return 1;
}

- (NSArray *)listArray {
    if (self.responseMapObject) {
        return self.responseMapObject;
    }
    else {
        return self.responseObject;
    }
}



@end
