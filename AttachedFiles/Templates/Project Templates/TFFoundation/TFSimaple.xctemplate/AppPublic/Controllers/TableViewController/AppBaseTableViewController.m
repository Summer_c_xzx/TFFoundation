//
//  AppBaseTableViewController.m
//  Template002
//
//  Created by Summer on 2018/1/8.
//Copyright © 2018年 Summer. All rights reserved.
//

#import "AppBaseTableViewController.h"
//request
#import <MJRefresh/MJRefresh.h>

@interface AppBaseTableViewController ()

@property (nonatomic , strong, readwrite) UITableView *tableView;

@property (nonatomic , strong, readwrite) TFTableViewManager *tableViewManager;

@property (nonatomic , strong, readwrite) AppTableDataRequest *request;

@end

@implementation AppBaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.tableView];
    [self startRequestData];
}

- (void)startRequestData {
    typeof(self) __weak weakSelf = self;
    self.request.assoicatedView = self.tableView;
    [self.request startFastRequestAndResponseHandle:^(id response, NSError *error) {
        [weakSelf p_handleResponse:weakSelf.request response:response error:error];
    }];
}

- (void)reloadData {
    [self p_mj_headerAction];
}

- (void)p_handleResponse:(AppTableDataRequest *)request response:(id)response error:(NSError *)error {
    
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    AppUIRemoveStateViewInView(self.view);
    NSInteger pageNumber = request.pageNumber;
    BOOL isFirstPage = request.isFirstPage;
    typeof(self) __weak weakSelf = self;
    if (error) {
        //有错误
        if (isFirstPage) {
            //第一页，移除上拉刷新和下拉加载
            [self p_removeRefreshHeader];
            [self p_removeRefreshFooter];
            [self.tableViewManager removeAllSections];
            AppUIAddStateViewInView([self errorStateViewWithError:error], request.assoicatedView);
        }
        else {
            if ([self.tableView.mj_footer isKindOfClass:[MJRefreshAutoNormalFooter class]]||[self.tableView.mj_footer isKindOfClass:[MJRefreshAutoStateFooter class]]) {
                MJRefreshAutoStateFooter *footer = (MJRefreshAutoStateFooter *)self.tableView.mj_footer;
                footer.stateLabel.text = error.localizedDescription;
            }
        }
    }
    else {
        NSInteger pageSize = request.pageSize;
        NSInteger totalPage = request.totalPage;
        NSArray *listArray = [request listArray];
        //添加下拉刷新，上拉加载
        [self p_addRefreshHeader];
        if (listArray.count) {
            NSInteger currentDataSize = (pageNumber+(1-request.firstPageNumber))*pageSize;
            if ((currentDataSize<totalPage&&totalPage)||((listArray.count==pageSize)&&!totalPage)) {
                [self p_addRefreshFooter];
            }
            else {
                //no more data
                if (listArray.count<pageSize||(currentDataSize>=totalPage&&totalPage)) {
                    [self p_noMoreData];
                }
            }
            
            //创建item、section
            if (self.isSectionForModel) {
                if (isFirstPage) {
                    [self.tableViewManager removeAllSections];
                }
                for (id model in listArray) {
                    TFTableViewSection *section = [TFTableViewSection section];
                    TFTableViewItem *item = [[[self tableViewManagerItemClass] alloc] init];
                    item.model = model;
                    item.selectionHandler = ^(__kindof TFTableViewItem *item, NSIndexPath *indexPath) {
                        [weakSelf tableViewManagerDidSelectItem:item atIndexPath:indexPath];
                    };
                    [section addItem:item];
                    [self tableViewManagerItemDidInit:item];
                    [self.tableViewManager addSection:section];
                    [self tableViewManagerSectionDidInit:section];
                }
            }
            else {
                TFTableViewSection *section = [self.tableViewManager.sections lastObject];
                if (!section) {
                    section = [TFTableViewSection section];
                    [self.tableViewManager addSection:section];
                    [self tableViewManagerSectionDidInit:section];
                }
                if (isFirstPage) {
                    [section removeAllItems];
                }
                for (id model in listArray) {
                    TFTableViewItem *item = [[[self tableViewManagerItemClass] alloc] init];
                    item.model = model;
                    item.selectionHandler = ^(__kindof TFTableViewItem *item, NSIndexPath *indexPath) {
                        [weakSelf tableViewManagerDidSelectItem:item atIndexPath:indexPath];
                    };
                    [section addItem:item];
                    [self tableViewManagerItemDidInit:item];
                }
                
            }
        }
        else {
            //添加empty
            if (isFirstPage) {
                [self.tableViewManager removeAllSections];
                [self p_removeRefreshFooter];
                [self p_removeRefreshHeader];
                AppUIAddStateViewInView([self emptyStateView], request.assoicatedView);
            }
            else {
                [self p_noMoreData];
            }
        }
    }
    [self.tableView reloadData];
}

- (Class)tableViewManagerItemClass {
    return [TFTableViewItem class];
}

- (void)tableViewManagerSectionDidInit:(TFTableViewSection *)section {
    
}

- (void)tableViewManagerItemDidInit:(__kindof TFTableViewItem *)item {
    
}

- (void)tableViewManagerDidSelectItem:(__kindof TFTableViewItem *)item atIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - stateView

- (TFUIStateView *)emptyStateView {
    TFUIStateView *stateView = APPSTYLE.emptyStateView;
    return stateView;
}

- (TFUIStateView *)errorStateViewWithError:(NSError *)error {
    TFUIStateView *stateView = APPSTYLE.errorStateView;
    stateView.titleLabel.text = error.localizedDescription;
    typeof(self) __weak weakSelf = self;
    
    stateView.buttonActionBlock = ^(TFUIStateView * _Nonnull stateView, TFUIButton * _Nonnull sender) {
        AppUIRemoveStateViewInView(weakSelf.request.assoicatedView);
        [weakSelf startRequestData];
    };
    return stateView;
}

#pragma mark - refresh

- (void)p_addRefreshHeader {
    if (!self.tableView.mj_header) {
        self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(p_mj_headerAction)];
    }
}

- (void)p_addRefreshFooter {
    if (!self.tableView.mj_footer) {
        self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(p_mj_footerAction)];
    }
}

- (void)p_mj_headerAction {
    self.request.pageNumber = self.request.firstPageNumber;
    [self startRequestData];
}

- (void)p_mj_footerAction {
    if (!self.request.error) {
        self.request.pageNumber += 1;
        if (self.request.pageNumber>self.request.totalPage&&self.request.totalPage) {
            self.request.pageNumber = self.request.totalPage;
        }
        [self startRequestData];
    }
}

- (void)p_removeRefreshHeader {
    self.tableView.mj_header = nil;
}

- (void)p_removeRefreshFooter {
    self.tableView.mj_footer = nil;
    self.tableView.mj_insetB = 0.0;
}

- (void)p_noMoreData {
    [self p_removeRefreshFooter];
}


#pragma mark - lazy load.

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:self.tableViewStyle];
        _tableView.backgroundColor = APPSTYLE.clearColor;
        switch (_tableViewStyle) {
            case UITableViewStylePlain: {
                _tableView.tableFooterView = [[UIView alloc] init];
            }
                break;
            case UITableViewStyleGrouped: {
                _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, CGFLOAT_MIN)];
            }
                break;
            default:
                break;
        }
        
    }
    return _tableView;
}

- (TFTableViewManager *)tableViewManager {
    if (!_tableViewManager) {
        _tableViewManager = [[TFTableViewManager alloc] initWithTableView:self.tableView];
    }
    return _tableViewManager;
}

- (AppTableDataRequest *)request {
    if (!_request) {
        _request = [[AppTableDataRequest alloc] init];
    }
    return _request;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
