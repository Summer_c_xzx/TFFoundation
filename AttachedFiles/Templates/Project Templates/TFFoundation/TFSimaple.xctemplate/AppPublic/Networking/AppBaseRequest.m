//
//  AppBaseRequest.m
//  Template002
//
//  Created by Summer on 2018/1/8.
//Copyright © 2018年 Summer. All rights reserved.
//

#import "AppBaseRequest.h"
#import <YYModel/YYModel.h>
#import "AppBaseModel.h"
#import "AppUIStyle.h"

#warning 2.配置你的请求信息，比如头部参数等等

NSString * const kAppBaseRequestPostProgressString = @"提交中...";

NSString * const kAppBaseRequestLoadingProgressString = @"加载中...";

@implementation AppBaseRequest

- (instancetype)init {
    self = [super init];
    if (self) {
        //配置你的请求
        self.baseURL = @"";
    }
    return self;
}

+ (void)startFastRequestWithPath:(NSString *)path method:(TFRequestMethod)method params:(NSDictionary *)params progressString:(NSString *)progressString assoicatedView:(UIView *)assoicatedView mapClassName:(NSString *)mapClassName handleErrorAutomatically:(BOOL)handleErrorAutomatically responseHandle:(void (^)(id, NSError *))responseHandle {
    AppBaseRequest *request = [[[self class] alloc] init];
    request.path = path;
    request.params = params;
    request.progressString = progressString;
    request.assoicatedView = assoicatedView;
    request.handleErrorAutomatically = handleErrorAutomatically;
    request.requestMethod = method;
    if (mapClassName.length) {
        request.responseJsonObjectMapClassName = mapClassName;
        Class mapClass = NSClassFromString(mapClassName);
        if (![mapClass isSubclassOfClass:[AppBaseModel class]]) {
            request.responseObjectPath = kAppBaseModelResponseObjectPath;
        }
    }
    [request startFastRequestAndResponseHandle:responseHandle];
}

+ (void)startFastHandleRequestWithPath:(NSString *)path method:(TFRequestMethod)method params:(NSDictionary *)params progressString:(NSString *)progressString mapClassName:(NSString *)mapClassName successHandle:(void (^)(id))successHandle {
    [self startFastRequestWithPath:path method:method params:params progressString:progressString assoicatedView:nil mapClassName:mapClassName handleErrorAutomatically:YES responseHandle:^(id response, NSError *error) {
        if (successHandle) {
            successHandle(response);
        }
    }];
}

+ (void)startFastPageInfoRequestWithPath:(NSString *)path method:(TFRequestMethod)method params:(NSDictionary *)params mapClassName:(NSString *)mapClassName assoicatedView:(UIView *)assoicatedView successHandle:(void (^)(id))successHandle {
    [self startFastRequestWithPath:path method:method params:params progressString:nil assoicatedView:assoicatedView mapClassName:mapClassName handleErrorAutomatically:YES responseHandle:^(id response, NSError *error) {
        if (successHandle) {
            successHandle(response);
        }
    }];
}

+  (void)startUploadImageRequestWithImage:(UIImage *)image successHandle:(void (^)(NSString *))successHandle {
    AppBaseRequest *request = [[[self class] alloc] init];
    request.path = @"index/upload";
    request.responseObjectPath = kAppBaseModelResponseObjectPath;
    request.construtingBodyBlock = ^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 0.6) name:@"file" fileName:@"file.jpg" mimeType:@"image/jpg"];
    };
    request.progressString = @"上传中...";
    request.handleErrorAutomatically = YES;
    request.requestMethod = TFRequestMethodPost;
    [request startFastRequestAndResponseHandle:^(id response, NSError *error) {
        if (successHandle) {
            successHandle(response[@"img_url"]);
        }
    }];
}

- (void)mapResponseJsonObject {
    id responseObject = [self p_objectForResponseObjectPath];
    if (self.responseJsonObjectMapClassName.length) {
        Class responseMapClass = NSClassFromString(self.responseJsonObjectMapClassName);
        if (responseMapClass&&responseObject) {
            if ([responseObject isKindOfClass:[NSArray class]]) {
                self.responseMapObject = [NSArray yy_modelArrayWithClass:responseMapClass json:responseObject];
            }
            else {
                self.responseMapObject = [responseMapClass yy_modelWithJSON:responseObject];
            }
        }
    }
}

- (id)p_objectForResponseObjectPath {
    id responseObject = self.responseObject;
    if (self.responseObjectPath.length && [responseObject isKindOfClass:[NSDictionary class]]) {
        NSArray *keysArray = [self.responseObjectPath componentsSeparatedByString:@"."];
        NSInteger keysCount = keysArray.count;
        id objectAtKeyPath = responseObject;
        
        NSInteger keyIndex = 0;
        while (keysCount > 0) {
            objectAtKeyPath = [objectAtKeyPath objectForKey:keysArray[keyIndex]];
            if ([objectAtKeyPath isKindOfClass:[NSDictionary class]]) {
                keyIndex ++;
                keysCount --;
            }
            else {
                break;
            }
        }
        responseObject = objectAtKeyPath;
    }
    return responseObject;
}

- (void)startFastRequestAndResponseHandle:(void (^)(id reponse, NSError *error))responseHandle {
    if (self.construtingBodyBlock) {
        self.timeoutInterval = 60.0*10;
    }
    else {
        self.timeoutInterval = 10.0;
    }
    if (self.assoicatedView) {
        AppUIRemoveStateViewInView(self.assoicatedView);
        AppUIAddStateViewInView(APPSTYLE.loadingStateView, self.assoicatedView);
    }
    else {
        if (self.progressString.length) {
            AppUIDismissPopoverView();
            AppUIShowPopoverView(AppUIPopoverTypeStatus, self.progressString);
        }
    }
    
    [self startRequestWithResponseBlock:^( AppBaseRequest * _Nonnull request, id  _Nullable response, NSError * _Nullable error) {
        
        NSError *responseError = error;
        AppBaseModel *responseModel = nil;
        if ([response isKindOfClass:[AppBaseModel class]]) {
            responseModel = response;
        }
        else {
            responseModel = [AppBaseModel yy_modelWithDictionary:response];
        }
        if (!responseModel.status&&responseModel) {
            responseError = [NSError errorWithDomain:[self tf_classNameString] code:responseModel.code userInfo:@{NSLocalizedDescriptionKey:responseModel.msg}];
        }
        if (request.progressString.length) {
            AppUIDismissPopoverView();
        }
        if (request.assoicatedView) {
            AppUIRemoveStateViewInView(request.assoicatedView);
        }
        if (responseError) {
#ifdef DEBUG
            TFLog(@"failedRequestWithUrl:%@%@,\nparams:%@\ncostTime:%gs\nresponse:%@,error:%@",request.baseURL,request.path,request.params,request.costTime,request.responseObject,error);
#endif
            //失败
            if (request.handleErrorAutomatically) {
                if (request.assoicatedView) {
                    TFUIStateView *errorStateView = APPSTYLE.errorStateView;
                    errorStateView.titleLabel.text = responseError.localizedDescription;
                    errorStateView.buttonActionBlock = ^(TFUIStateView * _Nonnull stateView, TFUIButton * _Nonnull sender) {
                        AppUIRemoveStateViewInView(request.assoicatedView);
                        [request startFastRequestAndResponseHandle:responseHandle];
                    };
                    errorStateView.titleLabel.text = responseError.localizedDescription;
                    AppUIAddStateViewInView(errorStateView, request.assoicatedView);
                }
                else {
                    AppUIShowPopoverView(AppUIPopoverTypeFail, responseError.localizedDescription);
                }
            }
            else {
                if (responseHandle) {
                    responseHandle(nil,responseError);
                }
            }
        }
        else {
            //成功
#ifdef DEBUG
            if ([response isKindOfClass:[NSData class]]) {
                TFLog(@"finishRequestWithUrl:%@%@,\nparams:%@\ncostTime:%gs\nresponse:data file.",request.baseURL,request.path,request.params,request.costTime);
            }
            else {
                TFLog(@"finishRequestWithUrl:%@%@,\nparams:%@\ncostTime:%gs\nresponse:%@",request.baseURL,request.path,request.params,request.costTime,request.responseObject);
            }
#endif
            if (request.responseMapObject) {
                if ([request.responseMapObject isKindOfClass:[NSArray class]]) {
                    if (![request.responseMapObject count]&&request.assoicatedView) {
                        //显示空占位
                        TFUIStateView *emptyStateView = APPSTYLE.emptyStateView;
                        if (request.emptyTitle.length) {
                            emptyStateView.titleLabel.text = request.emptyTitle;
                        }
                        if (request.emptyImageName.length) {
                            emptyStateView.imageView.image = [UIImage imageNamed:request.emptyImageName];
                        }
                        AppUIAddStateViewInView(emptyStateView, request.assoicatedView);
                    }
                }
                responseHandle(request.responseMapObject,responseError);
            }
            else {
                id responseObject = response;
                if (self.responseObjectPath.length) {
                    responseObject = [self p_objectForResponseObjectPath];
                }
                responseHandle(responseObject,responseError);
            }
        }
    }];
    
#ifdef DEBUG
    
    TFLog(@"startRequestWithUrl:%@%@,\nrequestHeaders:%@\nparams:%@",self.baseURL,self.path,self.sessionManager.requestSerializer.HTTPRequestHeaders,self.params);
    
#endif
}

@end
