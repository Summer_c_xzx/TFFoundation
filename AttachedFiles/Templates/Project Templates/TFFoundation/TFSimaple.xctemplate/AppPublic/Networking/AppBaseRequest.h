//
//  AppBaseRequest.h
//  Template002
//
//  Created by Summer on 2018/1/8.
//Copyright © 2018年 Summer. All rights reserved.
//

#import <TFFoundation/TFRequest.h>

extern NSString * const kAppBaseRequestLoadingProgressString;

extern NSString * const kAppBaseRequestPostProgressString;

@interface AppBaseRequest : TFRequest

/**
 状态加载字符串文字描述
 */
@property(nonatomic, strong) NSString *progressString;

/**
 关联的view，用于列表、详情等页面，自动处理加载、空数据、错误显示
 */
@property(nonatomic, weak) __kindof UIView *assoicatedView;

/**
 自动处理错误，默认为YES
 1.若未设置associatedView,加载时显示：progressString，错误显示toast
 2.若设置了associatedView，显示不同状态下的TFUIStateView
 @warning 设置为YES后，当数据有错误、空的情况下，response的block回调不会调用。
 */
@property(nonatomic, assign) BOOL handleErrorAutomatically;

@property(nonatomic, strong) NSString *emptyTitle;///<数据空白时标题
@property(nonatomic, strong) NSString *emptyImageName;///<数据空白时图片名称

/**
 返回的数据截取路径，默认为nil，不截取。
 */
@property(nonatomic, strong) NSString *responseObjectPath;

/**
 快速请求并处理返回数据
 
 @param responseHandle 返回的block
 */
- (void)startFastRequestAndResponseHandle:(void(^)(id response, NSError *error))responseHandle;

/**
 快速发起请求
 
 @param path 路径
 @param method 方法
 @param params 参数
 @param progressString 请求加载文本
 @param assoicatedView 关联的tableView
 @param mapClassName 对应返回模型
 @param handleErrorAutomatically 是否自动处理错误
 @param responseHandle 返回结果处理
 */
+ (void)startFastRequestWithPath:(NSString *)path method:(TFRequestMethod)method params:(NSDictionary *)params progressString:(NSString *)progressString assoicatedView:(UIView *)assoicatedView mapClassName:(NSString *)mapClassName handleErrorAutomatically:(BOOL)handleErrorAutomatically responseHandle:(void(^)(id response, NSError *error))responseHandle;

/**
 快速发起操作类的请求，例如登录、注册等，实现效果：会显示一个loading并自动处理错误，若错误弹出toast，且responseHandle不会调用。
 
 @param path 路径
 @param params 参数
 @param progressString 进度字符串
 @param mapClassName 映射的mapClassName
 @param successHandle 成功回调
 */
+ (void)startFastHandleRequestWithPath:(NSString *)path method:(TFRequestMethod)method params:(NSDictionary *)params progressString:(NSString *)progressString mapClassName:(NSString *)mapClassName successHandle:(void(^)(id response))successHandle;

/**
 快速请求页面信息类的请求，例如：列表、详情等，实现效果：会显示一个loading的stateView，若数据为空显示空stateView，若错误显示错误stateViewm，且responseHandle不会调用。
 
 @param path 路径
 @param params 参数
 @param mapClassName map类名称
 @param successHandle 成功回调
 */
+ (void)startFastPageInfoRequestWithPath:(NSString *)path method:(TFRequestMethod)method params:(NSDictionary *)params mapClassName:(NSString *)mapClassName assoicatedView:(UIView *)assoicatedView successHandle:(void(^)(id response))successHandle;

@end
