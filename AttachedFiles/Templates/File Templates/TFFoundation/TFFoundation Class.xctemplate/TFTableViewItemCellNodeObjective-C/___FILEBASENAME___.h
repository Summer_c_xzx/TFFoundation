//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#import <TFTableViewManager/___VARIABLE_cocoaTouchSubclass___.h>
#import "<#TableViewItemClass#>"


@interface ___FILEBASENAMEASIDENTIFIER___ : ___VARIABLE_cocoaTouchSubclass___

@property (nonatomic, strong) <#TableViewItemClass#> *tableViewItem;


@end
