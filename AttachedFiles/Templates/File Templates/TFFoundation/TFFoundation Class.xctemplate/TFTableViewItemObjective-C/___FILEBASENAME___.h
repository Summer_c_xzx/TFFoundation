//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#import <TFTableViewManager/___VARIABLE_cocoaTouchSubclass___.h>
#import "<#ItemModelClass#>"


@interface ___FILEBASENAMEASIDENTIFIER___ : ___VARIABLE_cocoaTouchSubclass___

@property (nonatomic,strong) <#ItemModelClass#> *model;


@end
