//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#import "___FILEBASENAME___.h"

@implementation ___FILEBASENAMEASIDENTIFIER___

@dynamic tableViewItem;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
    
}

#pragma mark - Cell life cycle

- (void)cellLoadSubViews {
    [super cellLoadSubViews];
    //add subviews and it's constraints at here.
    
}

- (void)cellWillAppear {
    [super cellWillAppear];
    //update subviews UI at here.
    
}

//- (void)cellDidDisappear {
//    [super cellDidDisappear];
//    //clear subviews UI at here.
//
//}


@end
