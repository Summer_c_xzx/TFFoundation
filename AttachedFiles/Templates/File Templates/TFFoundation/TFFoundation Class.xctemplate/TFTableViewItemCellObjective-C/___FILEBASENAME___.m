//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

#import "___FILEBASENAME___.h"

@interface ___FILEBASENAMEASIDENTIFIER___ ()



@end


@implementation ___FILEBASENAMEASIDENTIFIER___

@dynamic tableViewItem;

#pragma mark - Cell life cycle

- (void)cellLoadSubViews {
    [super cellLoadSubViews];
    //add subviews and it's constraints at here.
    
}

- (void)cellWillAppear {
    [super cellWillAppear];
    //update subviews UI at here.
    
}

//- (void)cellDidDisappear {
//    [super cellDidDisappear];
//    //clear subviews UI at here.
//
//}

@end
