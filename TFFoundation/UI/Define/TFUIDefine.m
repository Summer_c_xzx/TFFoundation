//
//  TFUIDefine.m
//  Pods-TFFoundation
//
//  Created by 夏之祥 on 2019/4/16.
//

#import "TFUIDefine.h"

UIFont * TFUIFont(CGFloat size) {
    return [UIFont systemFontOfSize:size];
}

UIFont * TFUIBlodFont(CGFloat size) {
    return [UIFont boldSystemFontOfSize:size];
}

UIFont * TFUISystemFont(CGFloat size, UIFontWeight weight) {
    return [UIFont systemFontOfSize:size weight:weight];
}

UIColor * TFUIRGBColor(CGFloat red, CGFloat green, CGFloat blue) {
    return TFUIRGBAColor(red, green, blue, 1.0);
}

UIColor * TFUIRGBAColor(CGFloat red, CGFloat green, CGFloat blue, CGFloat alpha) {
    return [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:alpha];
}

UIColor * TFUIHexColor(NSString * hexString) {
    return TFUIHexAColor(hexString, 1.0);
}

UIColor * TFUIHexAColor(NSString *hexString, CGFloat alpha) {
    //剔除#
    hexString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    unsigned int red, green, blue;
    NSRange range;
    range.length =2;
    
    range.location =0;
    [[NSScanner scannerWithString:[hexString substringWithRange:range]]scanHexInt:&red];
    range.location =2;
    [[NSScanner scannerWithString:[hexString substringWithRange:range]]scanHexInt:&green];
    range.location =4;
    [[NSScanner scannerWithString:[hexString substringWithRange:range]]scanHexInt:&blue];
    return TFUIRGBAColor(red, green, blue, alpha);
}

float TFUIInterpolation(float from, float to, float percent) {
    percent = MAX(0, MIN(1, percent));
    return from + (to - from)*percent;
}

UIColor * TFUIInterpolationColor(UIColor * __nonnull fromColor, UIColor * __nonnull toColor, float percent) {
    CGFloat fromColorRed = 0.0;
    CGFloat fromColorGreen = 0.0;
    CGFloat fromColorBlue = 0.0;
    CGFloat fromColorAlpha = 0.0;
    [fromColor getRed:&fromColorRed green:&fromColorGreen blue:&fromColorBlue alpha:&fromColorAlpha];
    
    CGFloat toColorRed = 0.0;
    CGFloat toColorGreen = 0.0;
    CGFloat toColorBlue = 0.0;
    CGFloat toColorAlpha = 0.0;
    [toColor getRed:&toColorRed green:&toColorGreen blue:&toColorBlue alpha:&toColorAlpha];
    
    CGFloat red = TFUIInterpolation(fromColorRed, toColorRed, percent);
    CGFloat green = TFUIInterpolation(fromColorGreen, toColorGreen, percent);
    CGFloat blue = TFUIInterpolation(fromColorBlue, toColorBlue, percent);
    CGFloat alpha = TFUIInterpolation(fromColorAlpha, toColorAlpha, percent);
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

NSAttributedString * __nonnull TFNSAttributedStringMake(NSString * __nonnull string, NSDictionary<NSAttributedStringKey, id> * __nullable attrs) {
    return [[NSAttributedString alloc] initWithString:string attributes:attrs];
}

NSAttributedString * __nonnull TFNSAttributedStringFontAndColorMake(NSString * __nonnull string, UIFont * __nonnull font, UIColor * __nonnull color) {
    return TFNSAttributedStringMake(string, @{NSFontAttributeName:font,NSForegroundColorAttributeName:color});
}

NSMutableAttributedString * __nonnull TFNSMutableAttributedStringMake(NSArray<NSAttributedString *> * __nonnull attrsStringsArray) {
    
    NSMutableAttributedString *mAttrs = [[NSMutableAttributedString alloc] init];
    for (NSAttributedString *attrsString in attrsStringsArray) {
        [mAttrs appendAttributedString:attrsString];
    }
    return mAttrs;
}

@implementation TFUIDefine

+ (UIViewController *)visibleViewController {
    UIViewController *rootViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
    return [rootViewController visibleViewController];
}

@end


@implementation UIViewController (TFUI)

- (nullable __kindof UIViewController *)visibleViewController {
    
    if ([self isKindOfClass:[UINavigationController class]]) {
        return [((UINavigationController *)self).visibleViewController visibleViewController];
    }
    else if ([self isKindOfClass:[UITabBarController class]]) {
        return [((UITabBarController *)self).selectedViewController visibleViewController];
    }
    else {
        if (self.presentedViewController) {
            return [self.presentedViewController visibleViewController];
        }
        else {
            return self;
        }
    }
}

@end
