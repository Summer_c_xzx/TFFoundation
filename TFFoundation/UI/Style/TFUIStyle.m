//
//  TFStyle.m
//  Pods-TFFoundation
//
//  Created by 夏之祥 on 2019/4/16.
//

#import "TFUIStyle.h"
#import <Masonry/Masonry.h>
#import "TFUITextView.h"

static __kindof TFUIStyle *_currentStyle = nil;

void TFUISetCurrentStyle(TFUIStyle * __nonnull style) {
    _currentStyle = nil;
    _currentStyle = style;
    [_currentStyle updateStyle];
}

TFUIStyle * __nonnull TFUICurrentStyle(void) {
    return _currentStyle;
}

@interface TFUIStyle ()

@property(nonatomic, copy ,readwrite) TFUIStyleMakeFont font;
@property(nonatomic, copy ,readwrite) TFUIStyleMakeFont boldFont;
@property(nonatomic, copy ,readwrite) TFUIStyleMakeFont heavyFont;
@property(nonatomic, copy ,readwrite) TFUIStyleMakeHexColor hexColor;
@property(nonatomic, copy ,readwrite) TFUIStyleMakeHexAColor hexAColor;
@property(nonatomic, copy ,readwrite) TFUIStyleMakeFontAndWeight fontAndWeight;

@end

@implementation TFUIStyle

- (void)updateStyle {
    UIViewController *visibleVC = [TFUIDefine visibleViewController];
    UINavigationBar *navigationBar = visibleVC.navigationController.navigationBar;
    
    self.font = ^UIFont * _Nonnull(CGFloat fontSize) {
        return TFUIFont(fontSize);
    };
    self.boldFont = ^UIFont * _Nonnull(CGFloat fontSize) {
        return TFUIBlodFont(fontSize);
    };
    
    self.fontAndWeight = ^UIFont * _Nonnull(CGFloat fontSize, UIFontWeight weight) {
        return TFUISystemFont(fontSize, weight);
    };
    
    self.hexColor = ^UIColor * _Nonnull(NSString *hexString) {
        return TFUIHexColor(hexString);
    };
    
    self.hexAColor = ^UIColor * _Nonnull(NSString *hexString, CGFloat alpha) {
        return TFUIHexAColor(hexString, alpha);
    };
    
    //UINavigationBar样式定制
      if (_currentStyle.navigationBarBackgroudImage) {
        [[UINavigationBar appearance] setBackgroundImage:_currentStyle.navigationBarBackgroudImage forBarMetrics:UIBarMetricsDefault];
        [navigationBar setBackgroundImage:_currentStyle.navigationBarBackgroudImage forBarMetrics:UIBarMetricsDefault];
    }
    if (_currentStyle.navigationBarTintColor) {
        [[UINavigationBar appearance] setBarTintColor:_currentStyle.navigationBarTintColor];
        [navigationBar setBarTintColor:_currentStyle.navigationBarTintColor];
    }
    
    NSDictionary *navigationBarTitleAttributesDic = @{ NSForegroundColorAttributeName: _currentStyle.navigationBarTitleColor,
                                                       NSFontAttributeName:_currentStyle.navigationBarTitleFont};
    [[UINavigationBar appearance] setTitleTextAttributes:navigationBarTitleAttributesDic];
    [navigationBar setTitleTextAttributes:navigationBarTitleAttributesDic];
    
    [[UIBarButtonItem appearance] setTintColor:_currentStyle.barButtonItemTintColor];
    [visibleVC.navigationItem.leftBarButtonItem setTintColor:_currentStyle.barButtonItemTintColor];
    [visibleVC.navigationItem.rightBarButtonItem setTintColor:_currentStyle.barButtonItemTintColor];
    
    //设置UIBarButtonItem样式
    NSDictionary *barButtonItemNormalDic = @{NSFontAttributeName:_currentStyle.barButtonItemTitleFont,NSForegroundColorAttributeName:_currentStyle.barButtonItemTitleColor};
    
    [[UIBarButtonItem appearance] setTitleTextAttributes:barButtonItemNormalDic forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:barButtonItemNormalDic forState:UIControlStateHighlighted];
    
    [visibleVC.navigationItem.leftBarButtonItem setTitleTextAttributes:barButtonItemNormalDic forState:UIControlStateNormal];
    [visibleVC.navigationItem.rightBarButtonItem setTitleTextAttributes:barButtonItemNormalDic forState:UIControlStateNormal];
    [visibleVC.navigationItem.leftBarButtonItem setTitleTextAttributes:barButtonItemNormalDic forState:UIControlStateHighlighted];
    [visibleVC.navigationItem.rightBarButtonItem setTitleTextAttributes:barButtonItemNormalDic forState:UIControlStateHighlighted];
    
    for (UIBarButtonItem *barButtonItem in visibleVC.navigationItem.leftBarButtonItems) {
        [barButtonItem setTitleTextAttributes:barButtonItemNormalDic forState:UIControlStateNormal];
    }
    
    for (UIBarButtonItem *barButtonItem in visibleVC.navigationItem.rightBarButtonItems) {
        [barButtonItem setTitleTextAttributes:barButtonItemNormalDic forState:UIControlStateNormal];
    }

    NSDictionary *barButtonItemDisabledDic = @{NSFontAttributeName:_currentStyle.barButtonItemTitleFont,NSForegroundColorAttributeName:_currentStyle.barButtonItemDisableTitleColor};
    [[UIBarButtonItem appearance] setTitleTextAttributes:barButtonItemDisabledDic forState:UIControlStateDisabled];
    [visibleVC.navigationItem.leftBarButtonItem setTitleTextAttributes:barButtonItemDisabledDic forState:UIControlStateDisabled];
    [visibleVC.navigationItem.rightBarButtonItem setTitleTextAttributes:barButtonItemDisabledDic forState:UIControlStateDisabled];

    for (UIBarButtonItem *barButtonItem in visibleVC.navigationItem.leftBarButtonItems) {
        [barButtonItem setTitleTextAttributes:barButtonItemNormalDic forState:UIControlStateDisabled];
    }
    
    for (UIBarButtonItem *barButtonItem in visibleVC.navigationItem.rightBarButtonItems) {
        [barButtonItem setTitleTextAttributes:barButtonItemNormalDic forState:UIControlStateDisabled];
    }


    
    //UIBarButtonItem样式定制
    if (_currentStyle.barButtonBackItemImage) {
        UIImage *backImage = [_currentStyle.barButtonBackItemImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        if (@available(iOS 11.0, *)) {
            [[UINavigationBar appearance] setBackIndicatorImage:backImage];
            [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:backImage];
            [navigationBar setBackIndicatorImage:backImage];
            [navigationBar setBackIndicatorTransitionMaskImage:backImage];
        }
        else {
            [[UIBarButtonItem appearance] setBackButtonBackgroundImage:[backImage resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, backImage.size.width, 0.0, 0.0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
            [visibleVC.navigationItem.backBarButtonItem setBackButtonBackgroundImage:[backImage resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, backImage.size.width, 0.0, 0.0)] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
            
            [navigationBar setBackIndicatorImage:[UIImage new]];
            [navigationBar setBackIndicatorTransitionMaskImage:[UIImage new]];
        }
    }
    
    //UITabBarItem样式定制
    NSDictionary *tabbarItemTitleAttributesDic = @{NSFontAttributeName:_currentStyle.tabBarItemTitleFont,NSForegroundColorAttributeName:_currentStyle.tabBarItemNormalTitleColor};
    NSDictionary *tabbarItemSelectTitleAttributesDic = @{NSFontAttributeName:_currentStyle.tabBarItemTitleFont,NSForegroundColorAttributeName:_currentStyle.tabBarItemSelectTitleColor};
    if (@available(iOS 13.0, *)) {
        UITabBarAppearance *appearance = [[UITabBarAppearance alloc] init];
        [appearance.stackedLayoutAppearance.normal setTitleTextAttributes:tabbarItemTitleAttributesDic];
        [appearance.stackedLayoutAppearance.selected setTitleTextAttributes:tabbarItemSelectTitleAttributesDic];
        [[UITabBar appearance] setStandardAppearance:appearance];
    }
    else {
        [[UITabBarItem appearance] setTitleTextAttributes:tabbarItemTitleAttributesDic forState:UIControlStateNormal];
        [visibleVC.tabBarController.tabBarItem setTitleTextAttributes:tabbarItemSelectTitleAttributesDic forState:UIControlStateNormal];
        
        [[UITabBarItem appearance] setTitleTextAttributes:tabbarItemSelectTitleAttributesDic forState:UIControlStateSelected];
        [visibleVC.tabBarController.tabBarItem setTitleTextAttributes:tabbarItemSelectTitleAttributesDic forState:UIControlStateSelected];
    }
    
    
    //设置状态栏样式
    [[UIApplication sharedApplication] setStatusBarStyle:_currentStyle.statusBarStyle];

    //tableView
    [[UITableViewCell appearance] setSelectionStyle:UITableViewCellSelectionStyleNone];
    [[UITableViewHeaderFooterView appearance] setTintColor:[UIColor clearColor]];
    [[UITableView appearance] setSeparatorColor:_currentStyle.separatorColor];
    
    //文本
    [[UITextField appearance] setGlobalPlaceholderAttributes:@{NSForegroundColorAttributeName:_currentStyle.placeholderColor}];
    [[TFUITextView appearance]setGlobalPlaceholderColor:_currentStyle.placeholderColor];
}

#pragma mark - 全局颜色

- (UIColor *)clearColor {
    return [UIColor clearColor];
}

- (UIColor *)whiteColor {
    return [UIColor whiteColor];
}

- (UIColor *)blackColor {
    return [UIColor blackColor];
}

- (UIColor *)grayColor {
    return TFUIHexColor(@"666666");
}

- (UIColor *)lightGrayColor {
    return TFUIHexColor(@"999999");
}

- (UIColor *)redColor {
    return [UIColor redColor];
}

- (UIColor *)greenColor {
    return [UIColor greenColor];
}

- (UIColor *)blueColor {
    return [UIColor blueColor];
}

- (UIColor *)orangeColor {
    return [UIColor orangeColor];
}

- (UIColor *)yellowColor {
    return [UIColor yellowColor];
}



- (UIColor *)backgroundColor {
    return _currentStyle.whiteColor;
}

- (UIColor *)separatorColor {
    return TFUIHexColor(@"cccccc");
}

- (UIColor *)placeholderColor {
    return _currentStyle.lightGrayColor;
}



- (UIImage *)navigationBarBackgroudImage {
    return nil;
}

- (UIColor *)navigationBarTintColor {
    return _currentStyle.whiteColor;
}

- (UIColor *)navigationBarTitleColor {
    return _currentStyle.blackColor;
}

- (UIFont *)navigationBarTitleFont {
    return TFUIFont(18);
}

- (UIColor *)barButtonItemTitleColor {
    return [UIColor blackColor];
}

- (UIColor *)barButtonItemDisableTitleColor {
    return [UIColor colorWithWhite:0.48 alpha:0.35];
}

- (UIFont *)barButtonItemTitleFont {
    return TFUIFont(16);
}

- (UIColor *)barButtonItemTintColor {
    return nil;
}

- (UIImage *)barButtonBackItemImage {
    return nil;
}

- (NSString *)barButtonBackItemTitle {
    return @"";
}

- (UIFont *)tabBarItemTitleFont {
    return TFUIFont(11);
}

- (UIColor *)tabBarItemNormalTitleColor {
    return [UIColor blackColor];
}

- (UIColor *)tabBarItemSelectTitleColor {
    return [UIColor blueColor];
}


- (TFUIStateView *)loadingStateView {
    
    UIView *customView = [[UIView alloc] init];
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityView.translatesAutoresizingMaskIntoConstraints = NO;
    [activityView startAnimating];
    [customView addSubview:activityView];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    titleLabel.text = @"正在加载...";
    titleLabel.font = TFUIFont(16);
    titleLabel.textColor = self.grayColor;
    [customView addSubview:titleLabel];
    
    [activityView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0.0);
        make.centerY.mas_equalTo(0.0);
    }];
    
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(activityView.mas_right).mas_offset(4.0);
        make.right.top.bottom.mas_equalTo(0.0);
    }];
    
    TFUIStateView *stateView = [[TFUIStateView alloc] initWithCustomView:customView];
    
    [customView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(CGPointZero);
    }];
    return stateView;
}

- (TFUIStateView *)emptyStateView {
    TFUIStateView *stateView = [[TFUIStateView alloc] init];
    stateView.titleLabel.text = @"无数据";
    stateView.titleLabel.textColor = self.grayColor;
    return stateView;
}

- (TFUIStateView *)errorStateView {
    TFUIStateView *stateView = [[TFUIStateView alloc] init];
    stateView.titleLabel.text = @"页面有错误";
    stateView.titleLabel.textColor = self.grayColor;
    stateView.titleLabel.font = TFUIFont(14);
    [stateView.button setTitle:@"重新加载" forState:UIControlStateNormal];
    [stateView.button setTitleColor:self.lightGrayColor forState:UIControlStateNormal];
    stateView.button.titleLabel.font = TFUIFont(14);
    return stateView;
}

@end


@implementation UITextField (TFUISyle)

- (void)setGlobalPlaceholderAttributes:(NSDictionary *)placeholderAttributes {
    if (self.placeholder.length) {
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:placeholderAttributes];
    }
}

@end


@implementation TFUITextView (TFUISyle)

- (void)setGlobalPlaceholderColor:(UIColor *)placeholderColor {
    self.placeholderColor = placeholderColor;
}

@end
