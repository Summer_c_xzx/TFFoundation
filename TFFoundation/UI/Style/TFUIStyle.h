//
//  TFStyle.h
//  Pods-TFFoundation
//
//  Created by 夏之祥 on 2019/4/16.
//

#import <UIKit/UIKit.h>
#import "TFUIDefine.h"
#import "TFUIStateView.h"
#import "TFUITextView.h"

NS_ASSUME_NONNULL_BEGIN

@class TFUIStyle;

/**
 设置当前样式类，会更新当前的配置的样式

 @param style 样式类
 */
void TFUISetCurrentStyle(__kindof TFUIStyle * __nonnull style);

/**
 获取当前样式类
n
 @return TFUIStyle
 */
__kindof TFUIStyle * __nonnull TFUICurrentStyle(void);

typedef UIFont  * _Nonnull (^TFUIStyleMakeFont)(CGFloat fontSize);
typedef UIFont  * _Nonnull (^TFUIStyleMakeFontAndWeight)(CGFloat fontSize, UIFontWeight weight);
typedef UIColor * _Nonnull (^TFUIStyleMakeHexColor)(NSString * __nonnull hexString);
typedef UIColor * _Nonnull (^TFUIStyleMakeHexAColor)(NSString * __nonnull hexString, CGFloat alpha);

@interface TFUIStyle : NSObject

/**
 更新样式的方法
 */
- (void)updateStyle;

#pragma mark - 全局颜色

@property(nonatomic, strong, readonly, nonnull) UIColor *clearColor;
@property(nonatomic, strong, readonly, nonnull) UIColor *whiteColor;
@property(nonatomic, strong, readonly, nonnull) UIColor *blackColor;
@property(nonatomic, strong, readonly, nonnull) UIColor *grayColor;
@property(nonatomic, strong, readonly, nonnull) UIColor *lightGrayColor;
@property(nonatomic, strong, readonly, nonnull) UIColor *redColor;
@property(nonatomic, strong, readonly, nonnull) UIColor *greenColor;
@property(nonatomic, strong, readonly, nonnull) UIColor *blueColor;
@property(nonatomic, strong, readonly, nonnull) UIColor *orangeColor;
@property(nonatomic, strong, readonly, nonnull) UIColor *yellowColor;

@property(nonatomic, strong, readonly, nonnull) UIColor *backgroundColor;
@property(nonatomic, strong, readonly, nonnull) UIColor *separatorColor;
@property(nonatomic, strong, readonly, nonnull) UIColor *placeholderColor;

@property(nonatomic, copy, readonly, nullable) TFUIStyleMakeHexColor hexColor;
@property(nonatomic, copy, readonly, nullable) TFUIStyleMakeHexAColor hexAColor;


#pragma mark - UINavigationBar样式定制

@property (nonatomic, strong, readonly, nullable) UIImage *navigationBarBackgroudImage;///< 导航栏默认背景图片，若设置此属性，则导航栏会变为不透明
@property (nonatomic, strong, readonly, nullable) UIColor *navigationBarTintColor;///< 导航栏默认背景色，若设置此属性，则导航栏会变为透明，默认[UIColor whiteColor]
@property (nonatomic, strong, readonly, nonnull) UIColor *navigationBarTitleColor;///< 导航栏标题颜色，默认[UIColor blackColor]
@property (nonatomic, strong, readonly, nonnull) UIFont *navigationBarTitleFont;///< 导航栏字体大小，默认18

#pragma mark - UIBarButtonItem样式定制

@property (nonatomic, strong, readonly, nonnull) UIColor *barButtonItemTitleColor;///< UIBarButtonItem标题类型按钮字体颜色，默认[UIColor blackColor]
@property (nonatomic, strong, readonly, nonnull) UIColor *barButtonItemDisableTitleColor;///< UIBarButtonItem标题类型按钮不可点击状态下字体颜色，默认[UIColor colorWithWhite:0.48 alpha:0.35]
@property (nonatomic, strong, readonly, nonnull) UIFont *barButtonItemTitleFont;///< UIBarButtonItem标题类型按钮字体大小，默认16
@property (nonatomic, strong, readonly, nullable) UIColor *barButtonItemTintColor;///< UIBarButtonItem着色
@property (nonatomic, strong, readonly, nullable) UIImage *barButtonBackItemImage;///< 返回按钮图片
@property (nonatomic, strong, readonly, nullable) NSString *barButtonBackItemTitle;///< 返回按钮标题

#pragma mark - UITabBarItem样式定制

@property (nonatomic, strong, readonly, nonnull) UIFont *tabBarItemTitleFont;///< 底部标签栏字体大小，默认11
@property (nonatomic, strong, readonly, nonnull) UIColor *tabBarItemNormalTitleColor;///< 底部标签栏普通状态下字体的颜色，默认[UIColor blackColor]"
@property (nonatomic, strong, readonly, nonnull) UIColor *tabBarItemSelectTitleColor;///< 底部标签栏选择状态下字体的颜色，默认[UIColor blueColor]

#pragma mark - 其他

@property (nonatomic, assign, readonly) UIStatusBarStyle statusBarStyle;///< 状态栏样式

@property(nonatomic, strong, readonly, nonnull) TFUIStateView *loadingStateView;///<加载时
@property(nonatomic, strong, readonly, nonnull) TFUIStateView *emptyStateView;///<空占位
@property(nonatomic, strong, readonly, nonnull) TFUIStateView *errorStateView;///<错误

@property(nonatomic, copy ,readonly, nullable) TFUIStyleMakeFont font;///<获取字体block
@property(nonatomic, copy ,readonly, nullable) TFUIStyleMakeFont boldFont;///<获取粗字体block
@property(nonatomic, copy ,readonly, nullable) TFUIStyleMakeFontAndWeight fontAndWeight;///<获取字体block


@end


@interface UITextField (TFUISyle)

/// 设置占位文字的样式
/// @param placeholderAttributes 样式
- (void)setGlobalPlaceholderAttributes:(nonnull NSDictionary *)placeholderAttributes UI_APPEARANCE_SELECTOR;

@end

@interface TFUITextView (TFUISyle)

- (void)setGlobalPlaceholderColor:(nonnull UIColor *)placeholderColor UI_APPEARANCE_SELECTOR;

@end

NS_ASSUME_NONNULL_END
