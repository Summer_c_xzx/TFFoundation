//
//  TFUIPageViewController.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/5/7.
//

#import "TFUIPageViewController.h"

@interface TFUIPageViewController ()<UIPageViewControllerDelegate,UIPageViewControllerDataSource>

@property(nonatomic, strong, readwrite) UIPageViewController *containerController;

@property(nonatomic, assign, readwrite) NSInteger currentPage;

@property(nonatomic, strong) NSMutableArray *pageChildViewControllersArray;

@property(nonatomic, weak, readwrite) UIScrollView *scrollView;

@end

@implementation TFUIPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _pageChildViewControllersArray = [NSMutableArray array];
    NSInteger totalPage = [self numbersOfPage];
    for (NSInteger i=0; i<totalPage; i++) {
        [_pageChildViewControllersArray addObject:[self viewControllerAtPage:i]];
    }
    if (_pageChildViewControllersArray.count) {
        [self selectPage:0 animated:NO];
    }
    
    self.containerController.view.frame = self.view.bounds;
    [self addChildViewController:_containerController];
    [self.view addSubview:_containerController.view];

    for (UIView *subView in _containerController.view.subviews) {
        if ([subView isKindOfClass:[UIScrollView class]]) {
            _scrollView = subView;
            break;
        }
    }
    _scrollView.delegate = self;
}


- (NSInteger)numbersOfPage {
    return 0;
}

- (UIViewController *)viewControllerAtPage:(NSInteger)page {
    return nil;
}

- (void)selectPage:(NSInteger)page animated:(BOOL)animated {
    UIPageViewControllerNavigationDirection direction = UIPageViewControllerNavigationDirectionForward;
    if (page<_currentPage) {
        direction = UIPageViewControllerNavigationDirectionReverse;
    }
    _currentPage = page;
    UIViewController *pageChildVC = _pageChildViewControllersArray[page];
    [self.containerController setViewControllers:@[pageChildVC] direction:direction animated:animated completion:nil];
    [self didSelectViewController:pageChildVC atPage:[_pageChildViewControllersArray indexOfObject:pageChildVC]];
}

- (void)didSelectViewController:(__kindof UIViewController *)viewController atPage:(NSInteger)page {
    
}

#pragma mark - UIPageViewControllerDelegate,UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSInteger index = [self.pageChildViewControllersArray indexOfObject:viewController];
    if (index-1<0 || index==NSNotFound) {
        return nil;
    }
    return [self.pageChildViewControllersArray objectAtIndex:index-1];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    NSInteger index = [self.pageChildViewControllersArray indexOfObject:viewController];
    if (index+1>=self.pageChildViewControllersArray.count || index==NSNotFound) {
        return nil;
    }
    return [self.pageChildViewControllersArray objectAtIndex:index+1];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
    UIViewController *pageChildVC = [pageViewController.viewControllers lastObject];
    _currentPage = [_pageChildViewControllersArray indexOfObject:pageChildVC];
    [self didSelectViewController:pageChildVC atPage:_currentPage];
}

#pragma mark - lazy load.

- (UIPageViewController *)containerController {
    if (!_containerController) {
        NSDictionary *options = @{UIPageViewControllerOptionInterPageSpacingKey : @(0)};
        _containerController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:options];
        _containerController.delegate = self;
        _containerController.dataSource = self;
    }
    return _containerController;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
