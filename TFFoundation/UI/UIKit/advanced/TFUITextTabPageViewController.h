//
//  TFUITextTabPageViewController.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/5/7.
//

#import "TFUIPageViewController.h"
#import "TFUITabView.h"

@interface TFUITextTabPageViewController : TFUIPageViewController

/**
 初始化
 */
- (void)didInitialize;

@property(nonatomic, strong, nonnull, readonly) TFUITextTabView *tabView;
@property(nonatomic, assign) CGFloat tabViewHeight;

@end
