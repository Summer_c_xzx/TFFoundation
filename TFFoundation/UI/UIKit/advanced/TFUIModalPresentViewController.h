//
//  TFUIModalPresentViewController.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/5/9.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, TFUIModalPresentAnimationStyle) {
    TFUIModalPresentAnimationStyleFade,///<逐渐隐藏，默认
    TFUIModalPresentAnimationStylePopup,///<从中心点弹出
    TFUIModalPresentAnimationStyleSpringPopup,///<从中心点弹性弹出
    TFUIModalPresentAnimationStyleSlide,///<从下往上弹出
    TFUIModalPresentAnimationStyleSlideBottomToTop,///<从下往上弹出
    TFUIModalPresentAnimationStyleSlideTopToBottom,///<从上往下弹出
    TFUIModalPresentAnimationStyleSlideLeftToRight,///<从左往右弹出
    TFUIModalPresentAnimationStyleSlideRightToLeft,///<从右往左弹出
};

///<默认动画时间
extern const float kTFUIModalPresentAnimationDuration;

@class TFUIModalPresentViewController;

/**
 模态视图消失时block

 */
typedef void(^TFUIModalPresentViewControllerDismissCompletion)(void);

/**
 模态弹出vc
 */
@interface TFUIModalPresentViewController : UIViewController

/**
 显示在当前的视图控制器上的方法

 @param contentView 内容view
 @param animationStyle 动画类型
 @param animationDuration 动画时长
 @param dismissCompletion 消失时调用的block
 @return TFUIModalPresentViewController
 */
+ (nonnull instancetype)showWithContentView:(nullable UIView *)contentView animationStyle:(TFUIModalPresentAnimationStyle)animationStyle animationDuration:(float)animationDuration dismissCompletion:(nullable TFUIModalPresentViewControllerDismissCompletion)dismissCompletion;
+ (nonnull instancetype)showWithContentView:(nullable UIView *)contentView animationStyle:(TFUIModalPresentAnimationStyle)animationStyle animationDuration:(float)animationDuration;
+ (nonnull instancetype)showWithAnimationStyle:(TFUIModalPresentAnimationStyle)animationStyle;
/**
 隐藏模态试图控制器

 @param completion 消失完成block
 */
+ (void)dismissModalViewControllerCompletion:(nullable TFUIModalPresentViewControllerDismissCompletion)completion;

/**
 显示在某个view上

 @param view 显示的view
 @param contentView 内容view
 @param animationStyle 动画类型
 @param animationDuration 动画时长
 @param dismissCompletion 消失时调用的block
 @return TFUIModalPresentViewController
 */
+ (nonnull instancetype)showInView:(nonnull UIView *)view withContentView:(nullable UIView *)contentView animationStyle:(TFUIModalPresentAnimationStyle)animationStyle animationDuration:(float)animationDuration dismissCompletion:(nullable TFUIModalPresentViewControllerDismissCompletion)dismissCompletion;
+ (nonnull instancetype)showInView:(nonnull UIView *)view withContentView:(nullable UIView *)contentView animationStyle:(TFUIModalPresentAnimationStyle)animationStyle animationDuration:(float)animationDuration;
+ (nonnull instancetype)showInView:(nonnull UIView *)view withAnimationStyle:(TFUIModalPresentAnimationStyle)animationStyle;

/**
 隐藏某个view上的模态控制器

 @param view 显示的view
 @param completion 消失完成block
 */
+ (void)dismissModalViewControllerInView:(nonnull UIView *)view completion:(nullable TFUIModalPresentViewControllerDismissCompletion)completion;

/**
 内容view，此部分view的大小、布局信息需要自己指定，可通过属性直接赋值
 */
@property(nonatomic, strong, nonnull) UIView *contentView;
@property(nonatomic, strong, nonnull) UIView *dimmingView;///<背后的黑色透明遮罩
@property(nonatomic, weak, nullable) UIView *showInView;///<显示在某个view上
@property(nonatomic, assign) TFUIModalPresentAnimationStyle animationStyle;///<动画类型
@property(nonatomic, assign) float animationDuration;///<默认0.6s

/**
 初始化
 */
- (void)didInitialize;

/**
 显示动画，子类可重写此方法实现自定义动画
 */
- (void)showAnimation;

/**
 消失动画，子类可重写此方法实现自定义动画
 */
- (void)dismissAnimation;

@end


@interface UIView (UIModalPresent)

@property(nonatomic, strong, nullable) TFUIModalPresentViewController *tfUI_modalPresentViewController;///<模态控制器

@end


