//
//  NSArray+TFMasonryLayout.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/8/15.
//

#import <Foundation/Foundation.h>
#import <Masonry/Masonry.h>

NS_ASSUME_NONNULL_BEGIN

typedef struct TFMASViewAttributeEdgeInsets {
    MASViewAttribute *top, *left, *bottom, *right;
} TFMASViewAttributeEdgeInsets;


UIKIT_EXTERN const TFMASViewAttributeEdgeInsets TFMASViewAttributeEdgeInsetsNone;

UIKIT_STATIC_INLINE TFMASViewAttributeEdgeInsets TFMASViewAttributeEdgeInsetsMake(MASViewAttribute *top, MASViewAttribute *left, MASViewAttribute *bottom, MASViewAttribute *right) {
    TFMASViewAttributeEdgeInsets insets = {top, left, bottom, right};
    return insets;
}

@interface NSArray (TFMasonryLayout)

/**
 布局相等间距，宽度自适应相等的布局，提供可相对的头尾约束

 @param axisType 方向
 @param fixedSpacing 间距
 @param leadAttribute 首部约束
 @param leadSpacing 首部约束的间距
 @param tailAttribute 尾部约束
 @param tailSpacing 尾部约束的间距
 */
- (void)mas_distributeViewsAlongAxis:(MASAxisType)axisType withFixedSpacing:(CGFloat)fixedSpacing leadAttribute:(nonnull MASViewAttribute *)leadAttribute leadSpacing:(CGFloat)leadSpacing tailAttribute:(nonnull MASViewAttribute *)tailAttribute tailSpacing:(CGFloat)tailSpacing;

/**
 布局固定相等宽度，间距自适应相等的布局，提供可相对的头尾约束

 @param axisType 方向
 @param fixedItemLength 间距
 @param leadAttribute 首部约束
 @param leadSpacing 首部约束的间距
 @param tailAttribute 尾部约束
 @param tailSpacing 尾部约束的间距
 */
- (void)mas_distributeViewsAlongAxis:(MASAxisType)axisType withFixedItemLength:(CGFloat)fixedItemLength leadAttribute:(nonnull MASViewAttribute *)leadAttribute leadSpacing:(CGFloat)leadSpacing tailAttribute:(nonnull MASViewAttribute *)tailAttribute tailSpacing:(CGFloat)tailSpacing;


/**
 布局相等间距，宽度自适应的类似collectionView布局

 @param axisType 方向
 @param sectionInsets section留白
 @param sectionAttributeEdgeInsets section相对的约束，为nil时，则相对的就是父试图
 @param lineNumber 每列数量
 @param interItemSpacing 内部间距
 @param lineSpacing 列之间的间距
 */
- (void)mas_distributeViewsAlongAxis:(MASAxisType)axisType likeCollectionViewWithSectionInsets:(UIEdgeInsets)sectionInsets sectionAttributeEdgeInsets:(TFMASViewAttributeEdgeInsets)sectionAttributeEdgeInsets lineNumber:(NSInteger)lineNumber interItemSpacing:(CGFloat)interItemSpacing lineSpacing:(CGFloat)lineSpacing;

/**
 布局相等宽度，间距自适应的类似collectionView布局

 @param axisType 方向
 @param sectionInsets section留白
 @param sectionAttributeEdgeInsets section相对的约束，为nil时，则相对的就是父试图
 @param lineNumber 每列数量
 @param fixedItemLength 固定长度
 @param lineSpacing 列之间的间距
 */
- (void)mas_distributeViewsAlongAxis:(MASAxisType)axisType likeCollectionViewWithSectionInsets:(UIEdgeInsets)sectionInsets sectionAttributeEdgeInsets:(TFMASViewAttributeEdgeInsets)sectionAttributeEdgeInsets lineNumber:(NSInteger)lineNumber withFixedItemLength:(CGFloat)fixedItemLength lineSpacing:(CGFloat)lineSpacing;

@end

NS_ASSUME_NONNULL_END
