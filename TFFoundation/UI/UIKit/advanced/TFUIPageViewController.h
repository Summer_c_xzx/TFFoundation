//
//  TFUIPageViewController.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/5/7.
//

#import <UIKit/UIKit.h>

@interface TFUIPageViewController : UIViewController<UIScrollViewDelegate>

@property(nonatomic, strong, nonnull, readonly) UIPageViewController *containerController;///<内容view
@property(nonatomic, assign, readonly) NSInteger currentPage;///<当前页
@property(nonatomic, weak, nullable, readonly) UIScrollView *scrollView;

/**
 页数

 @return NSInteger
 */
- (NSInteger)numbersOfPage;

/**
 对应索引下的ViewController

 @param page 索引
 @return UIViewController
 */
- (nonnull __kindof UIViewController *)viewControllerAtPage:(NSInteger)page;

/**
 选择页数

 @param page 页数
 @param animated 是否动画
 */
- (void)selectPage:(NSInteger)page animated:(BOOL)animated;

/**
 选中某页

 @param viewController viewController
 @param page 页数
 */
- (void)didSelectViewController:(nonnull __kindof UIViewController *)viewController atPage:(NSInteger)page;

@end
