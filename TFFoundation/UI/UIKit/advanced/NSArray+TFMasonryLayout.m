//
//  NSArray+TFMasonryLayout.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/8/15.
//

#import "NSArray+TFMasonryLayout.h"

const TFMASViewAttributeEdgeInsets TFMASViewAttributeEdgeInsetsNone = {nil,nil,nil};

@implementation NSArray (TFMasonryLayout)

- (void)mas_distributeViewsAlongAxis:(MASAxisType)axisType withFixedSpacing:(CGFloat)fixedSpacing leadAttribute:(MASViewAttribute *)leadAttribute leadSpacing:(CGFloat)leadSpacing tailAttribute:(MASViewAttribute *)tailAttribute tailSpacing:(CGFloat)tailSpacing {
    if (self.count < 2) {
        NSAssert(self.count>1,@"views to distribute need to bigger than one");
        return;
    }
    if (axisType == MASAxisTypeHorizontal) {
        UIView *prev = nil;
        for (int i = 0; i < self.count; i++) {
            UIView *v = self[i];
            [v mas_makeConstraints:^(MASConstraintMaker *make) {
                if (prev) {
                    make.width.equalTo(prev);
                    make.left.equalTo(prev.mas_right).offset(fixedSpacing);
                    if (i == self.count - 1) {//last one
                        make.right.equalTo(tailAttribute).offset(-tailSpacing);
                    }
                }
                else {//first one
                    make.left.equalTo(leadAttribute).offset(leadSpacing);
                }
            }];
            prev = v;
        }
    }
    else {
        UIView *prev = nil;
        for (int i = 0; i < self.count; i++) {
            UIView *v = self[i];
            [v mas_makeConstraints:^(MASConstraintMaker *make) {
                if (prev) {
                    make.height.equalTo(prev);
                    make.top.equalTo(prev.mas_bottom).offset(fixedSpacing);
                    if (i == self.count - 1) {//last one
                        make.bottom.equalTo(tailAttribute).offset(-tailSpacing);
                    }
                }
                else {//first one
                    make.top.equalTo(leadAttribute).offset(leadSpacing);
                }
                
            }];
            prev = v;
        }
    }
    
}

- (void)mas_distributeViewsAlongAxis:(MASAxisType)axisType withFixedItemLength:(CGFloat)fixedItemLength leadAttribute:(nonnull MASViewAttribute *)leadAttribute leadSpacing:(CGFloat)leadSpacing tailAttribute:(nonnull MASViewAttribute *)tailAttribute tailSpacing:(CGFloat)tailSpacing {
    if (self.count < 2) {
        NSAssert(self.count>1,@"views to distribute need to bigger than one");
        return;
    }
    if (axisType == MASAxisTypeHorizontal) {
        UIView *prev = nil;
        for (int i = 0; i < self.count; i++) {
            UIView *v = self[i];
            [v mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(fixedItemLength));
                if (prev) {
                    if (i == self.count - 1) {//last one
                        make.right.equalTo(tailAttribute).offset(-tailSpacing);
                    }
                    else {
                        CGFloat offset = (1-(i/((CGFloat)self.count-1)))*(fixedItemLength+leadSpacing)-i*tailSpacing/(((CGFloat)self.count-1));
                        make.right.equalTo(tailAttribute).multipliedBy(i/((CGFloat)self.count-1)).with.offset(offset);
                    }
                }
                else {//first one
                    make.left.equalTo(leadAttribute).offset(leadSpacing);
                }
            }];
            prev = v;
        }
    }
    else {
        UIView *prev = nil;
        for (int i = 0; i < self.count; i++) {
            UIView *v = self[i];
            [v mas_makeConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@(fixedItemLength));
                if (prev) {
                    if (i == self.count - 1) {//last one
                        make.bottom.equalTo(tailAttribute).offset(-tailSpacing);
                    }
                    else {
                        CGFloat offset = (1-(i/((CGFloat)self.count-1)))*(fixedItemLength+leadSpacing)-i*tailSpacing/(((CGFloat)self.count-1));
                        make.bottom.equalTo(tailAttribute).multipliedBy(i/((CGFloat)self.count-1)).with.offset(offset);
                    }
                }
                else {//first one
                    make.top.equalTo(leadAttribute).offset(leadSpacing);
                }
            }];
            prev = v;
        }
    }
}


- (void)mas_distributeViewsAlongAxis:(MASAxisType)axisType likeCollectionViewWithSectionInsets:(UIEdgeInsets)sectionInsets sectionAttributeEdgeInsets:(TFMASViewAttributeEdgeInsets)sectionAttributeEdgeInsets lineNumber:(NSInteger)lineNumber interItemSpacing:(CGFloat)interItemSpacing lineSpacing:(CGFloat)lineSpacing {
    
    NSInteger vCount = self.count;
    
    NSInteger rowCount = vCount/lineNumber;
    if (vCount%lineNumber) {
        rowCount += 1;
    }
    UIView *tempSuperView = [self tf_mas_commonSuperviewOfViews];
    if (axisType==MASAxisTypeHorizontal) {
        UIView *prev = nil;
        for (NSInteger i=0; i<rowCount*lineNumber; i++) {
            UIView *v = nil;
            if (i<=vCount-1) {
                 v = self[i];
            }
            else {
                v = [[UIView alloc] init];
                [tempSuperView addSubview:v];
            }
            
            [v mas_makeConstraints:^(MASConstraintMaker *make) {
                //每行起始第一个
                if (i%lineNumber==0) {
                    make.left.mas_equalTo(sectionAttributeEdgeInsets.left?:tempSuperView).mas_offset(sectionInsets.left);
                    if (prev) {
                        make.top.mas_equalTo(prev.mas_bottom).mas_offset(lineSpacing);
                    }
                    else {
                        make.top.mas_equalTo(sectionAttributeEdgeInsets.top?:tempSuperView).mas_offset(sectionInsets.top);
                    }
                }
                else {
                    make.top.mas_equalTo(prev);
                    make.width.mas_equalTo(prev);
                    make.left.mas_equalTo(prev.mas_right).mas_offset(interItemSpacing);
                    //是否每行最后一个
                    if ((i+1)%lineNumber==0) {
                        make.right.mas_equalTo(sectionAttributeEdgeInsets.right?:tempSuperView).mas_offset(-sectionInsets.right);
                    }
                }
                //判断是否最后一行
                if (i/lineNumber>=rowCount-1) {
                    make.bottom.mas_equalTo(sectionAttributeEdgeInsets.bottom?:tempSuperView).mas_offset(-sectionInsets.bottom);
                }
            }];
            prev = v;
        }
    }
    else {
        UIView *prev = nil;
        for (NSInteger i=0; i<rowCount*lineNumber; i++) {
            UIView *v = nil;
            if (i<=vCount-1) {
                v = self[i];
            }
            else {
                v = [[UIView alloc] init];
                [tempSuperView addSubview:v];
            }
            
            [v mas_makeConstraints:^(MASConstraintMaker *make) {
                //每行起始第一个
                if (i%lineNumber==0) {
                    
                    make.top.mas_equalTo(sectionAttributeEdgeInsets.top?:tempSuperView).mas_offset(sectionInsets.top);
                    if (prev) {
                        make.left.mas_equalTo(prev.mas_right).mas_offset(lineSpacing);
                    }
                    else {
                        make.left.mas_equalTo(sectionAttributeEdgeInsets.left?:tempSuperView).mas_offset(sectionInsets.left);
                    }
                }
                else {
                    make.left.mas_equalTo(prev);
                    make.height.mas_equalTo(prev);
                    make.top.mas_equalTo(prev.mas_bottom).mas_offset(interItemSpacing);
                    //是否每行最后一个
                    if ((i+1)%lineNumber==0) {
                        make.bottom.mas_equalTo(sectionAttributeEdgeInsets.bottom?:tempSuperView).mas_offset(-sectionInsets.bottom);
                    }
                }
                //判断是否最后一行
                if (i/lineNumber>=rowCount-1) {
                    make.right.mas_equalTo(sectionAttributeEdgeInsets.right?:tempSuperView).mas_offset(-sectionInsets.right);
                }
            }];
            prev = v;
        }
    }
}

- (void)mas_distributeViewsAlongAxis:(MASAxisType)axisType likeCollectionViewWithSectionInsets:(UIEdgeInsets)sectionInsets sectionAttributeEdgeInsets:(TFMASViewAttributeEdgeInsets)sectionAttributeEdgeInsets lineNumber:(NSInteger)lineNumber withFixedItemLength:(CGFloat)fixedItemLength lineSpacing:(CGFloat)lineSpacing {
    
    NSInteger vCount = self.count;
    
    NSInteger rowCount = vCount/lineNumber;
    if (vCount%lineNumber) {
        rowCount += 1;
    }
    UIView *tempSuperView = [self tf_mas_commonSuperviewOfViews];
    if (axisType==MASAxisTypeHorizontal) {
        UIView *prev = nil;
        for (NSInteger i=0; i<rowCount*lineNumber; i++) {
            UIView *v = nil;
            if (i<=vCount-1) {
                v = self[i];
            }
            else {
                v = [[UIView alloc] init];
                [tempSuperView addSubview:v];
            }
            
            [v mas_makeConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(fixedItemLength);
                //每行起始第一个
                if (i%lineNumber==0) {
                    make.left.mas_equalTo(sectionAttributeEdgeInsets.left?:tempSuperView).mas_offset(sectionInsets.left);
                    if (prev) {
                        make.top.mas_equalTo(prev.mas_bottom).mas_offset(lineSpacing);
                    }
                    else {
                        make.top.mas_equalTo(sectionAttributeEdgeInsets.top?:tempSuperView).mas_offset(sectionInsets.top);
                    }
                }
                else {
                    make.top.mas_equalTo(prev);
                    //是否每行最后一个
                    if ((i+1)%lineNumber==0) {
                        make.right.mas_equalTo(sectionAttributeEdgeInsets.right?:tempSuperView).mas_offset(-sectionInsets.right);
                    }
                    else {
                        NSInteger lineIndex = i%lineNumber;
                        CGFloat offset = (1-(lineIndex/((CGFloat)lineNumber-1)))*(fixedItemLength+sectionInsets.left)-lineIndex*sectionInsets.right/(((CGFloat)lineNumber-1));
                        make.right.equalTo(sectionAttributeEdgeInsets.right?:tempSuperView).multipliedBy(lineIndex/((CGFloat)lineNumber-1)).with.offset(offset);
                    }
                }
                //判断是否最后一行
                if (i/lineNumber>=rowCount-1) {
                    make.bottom.mas_equalTo(sectionAttributeEdgeInsets.bottom?:tempSuperView).mas_offset(-sectionInsets.bottom);
                }
            }];
            prev = v;
        }
    }
    else {
        UIView *prev = nil;
        for (NSInteger i=0; i<rowCount*lineNumber; i++) {
            UIView *v = nil;
            if (i<=vCount-1) {
                v = self[i];
            }
            else {
                v = [[UIView alloc] init];
                [tempSuperView addSubview:v];
            }
            
            [v mas_makeConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(fixedItemLength);
                //每行起始第一个
                if (i%lineNumber==0) {
                    
                    make.top.mas_equalTo(sectionAttributeEdgeInsets.top?:tempSuperView).mas_offset(sectionInsets.top);
                    if (prev) {
                        make.left.mas_equalTo(prev.mas_right).mas_offset(lineSpacing);
                    }
                    else {
                        make.left.mas_equalTo(sectionAttributeEdgeInsets.left?:tempSuperView).mas_offset(sectionInsets.left);
                    }
                }
                else {
                    make.left.mas_equalTo(prev);
                    //是否每行最后一个
                    if ((i+1)%lineNumber==0) {
                        make.bottom.mas_equalTo(sectionAttributeEdgeInsets.bottom?:tempSuperView).mas_offset(-sectionInsets.bottom);
                    }
                    else {
                        NSInteger lineIndex = i%lineNumber;
                        CGFloat offset = (1-(lineIndex/((CGFloat)lineNumber-1)))*(fixedItemLength+sectionInsets.top)-lineIndex*sectionInsets.bottom/(((CGFloat)lineNumber-1));
                        make.bottom.equalTo(sectionAttributeEdgeInsets.bottom?:tempSuperView).multipliedBy(lineIndex/((CGFloat)lineNumber-1)).with.offset(offset);
                    }
                }
                //判断是否最后一行
                if (i/lineNumber>=rowCount-1) {
                    make.right.mas_equalTo(sectionAttributeEdgeInsets.right?:tempSuperView).mas_offset(-sectionInsets.right);
                }
            }];
            prev = v;
        }
    }
}


- (UIView *)tf_mas_commonSuperviewOfViews
{
    UIView *commonSuperview = nil;
    if (self.count>1) {
        UIView *previousView = nil;
        for (id object in self) {
            if ([object isKindOfClass:[UIView class]]) {
                UIView *view = (UIView *)object;
                if (previousView) {
                    commonSuperview = [view mas_closestCommonSuperview:commonSuperview];
                } else {
                    commonSuperview = view;
                }
                previousView = view;
            }
        }
        NSAssert(commonSuperview, @"Can't constrain views that do not share a common superview. Make sure that all the views in this array have been added into the same view hierarchy.");
        return commonSuperview;
    }
    else if (self.count==1) {
        commonSuperview = [[self firstObject]superview];
    }
    
    return commonSuperview;
}


@end
