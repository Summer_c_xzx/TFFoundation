//
//  TFUIButton.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/17.
//

#import <UIKit/UIKit.h>

/// 控制图片在UIButton里的位置，默认为TFUIButtonImagePostionTop
typedef NS_ENUM(NSUInteger, TFUIButtonImagePostion) {
    TFUIButtonImagePostionTop,///<图片在上
    TFUIButtonImagePostionLeft,///<图片在左
    TFUIButtonImagePostionBottom,///<图片在下
    TFUIButtonImagePostionRight,///<图片在右
};

NS_ASSUME_NONNULL_BEGIN

/**
 自定义UIButton，提供更改图片位置，调整图标和问题距离
 */
@interface TFUIButton : UIButton

/**
 * 设置按钮里图标和文字的相对位置，默认为TFUIButtonImagePostionLeft<br/>
 * 可配合imageEdgeInsets、titleEdgeInsets、contentHorizontalAlignment、contentVerticalAlignment使用
 */
@property(nonatomic, assign) TFUIButtonImagePostion imagePosition;

/**
 * 设置按钮里图标和文字之间的间隔，会自动响应 imagePosition 的变化而变化，默认为0。<br/>
 * 系统默认实现需要同时设置 titleEdgeInsets 和 imageEdgeInsets，同时还需考虑 contentEdgeInsets 的增加（否则不会影响布局，可能会让图标或文字溢出或挤压），使用该属性可以避免以上情况。<br/>
 * @warning 会与 imageEdgeInsets、 titleEdgeInsets、 contentEdgeInsets 共同作用。
 */
@property(nonatomic, assign) IBInspectable CGFloat spacingBetweenImageAndTitle;

@end

NS_ASSUME_NONNULL_END
