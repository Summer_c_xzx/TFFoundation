//
//  TFUIStateView.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/19.
//

#import "TFUIStateView.h"
#import <Masonry/Masonry.h>


@interface TFUIStateView ()

@property (nonatomic, strong, readwrite) UIView *contentView;

@property (nonatomic, strong, readwrite) UIView *customView;

@property (nonatomic, strong, readwrite) UIImageView *imageView;

@property (nonatomic, strong, readwrite) TFUILabel *titleLabel;

@property (nonatomic, strong, readwrite) TFUILabel *detailLabel;

@property (nonatomic, strong, readwrite) TFUIButton *button;

@end

@implementation TFUIStateView

- (instancetype)initWithCustomView:(__kindof UIView *)customView {
    self = [super init];
    if (self) {
        self.customView = customView;
        [self addSubview:self.customView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (!self.customView) {
            [self didInitialize];
            [self addSubview:self.contentView];
            [self.contentView addSubview:self.imageView];
            [self.contentView addSubview:self.titleLabel];
            [self.contentView addSubview:self.detailLabel];
            [self.contentView addSubview:self.button];
            [self makeSubViewsConstraints];
        }
    }
    return self;
}

- (void)didInitialize {
    _contentViewOffset = UIOffsetZero;
    _verticalSpace = 10.0;
    _imageTitlePadding = 0.0;
    _titleDetailPadding = 0.0;
    _detailButtonPadding = 0.0;
}

- (void)setVerticalSpace:(CGFloat)verticalSpace {
    _verticalSpace = verticalSpace;
    [_titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.imageView.mas_bottom).mas_offset(self.verticalSpace+self.imageTitlePadding);
    }];
    [_detailLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(self.verticalSpace+self.titleDetailPadding);
    }];
    [_button mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.detailLabel.mas_bottom).mas_offset(self.verticalSpace+self.detailButtonPadding);
    }];
    [self p_updateViews];
}

- (void)setImageTitlePadding:(CGFloat)imageTitlePadding {
    _imageTitlePadding = imageTitlePadding;
    [_titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.imageView.mas_bottom).mas_offset(self.verticalSpace+self.imageTitlePadding);
    }];
    [self p_updateViews];
}

- (void)setTitleDetailPadding:(CGFloat)titleDetailPadding {
    _titleDetailPadding = titleDetailPadding;
    [_detailLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(self.verticalSpace+self.titleDetailPadding);
    }];
    [self p_updateViews];
}

- (void)setDetailButtonPadding:(CGFloat)detailButtonPadding {
    _detailButtonPadding = detailButtonPadding;
    [_button mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.detailLabel.mas_bottom).mas_offset(self.verticalSpace+self.detailButtonPadding);
    }];
    [self p_updateViews];
}

- (void)p_updateViews {
    [self.contentView layoutIfNeeded];
    [self layoutIfNeeded];
}

- (void)makeSubViewsConstraints {
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(CGPointMake(_contentViewOffset.x, _contentViewOffset.y));
    }];
    
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0.0);
        make.centerX.mas_equalTo(0.0);
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.imageView.mas_bottom).mas_offset(self.verticalSpace+self.imageTitlePadding);
        make.left.right.mas_equalTo(0.0);
    }];
    
    [_detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.titleLabel.mas_bottom).mas_offset(self.verticalSpace+self.titleDetailPadding);
        make.left.right.mas_equalTo(0.0);
    }];
    
    [_button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.detailLabel.mas_bottom).mas_offset(self.verticalSpace+self.detailButtonPadding);
        make.bottom.mas_equalTo(0.0);
        make.centerX.mas_equalTo(0.0);
    }];
}

- (void)setButtonActionBlock:(TFUIStateViewButtonActionBlock)buttonActionBlock {
    _buttonActionBlock = buttonActionBlock;
    if (buttonActionBlock) {
        [_button addTarget:self action:@selector(p_buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)p_buttonAction:(TFUIButton *)sender {
    if (_buttonActionBlock) {
        _buttonActionBlock(self,sender);
    }
}

#pragma mark - lazy load.

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [[UIView alloc] init];
    }
    return _contentView;
}

- (UIImageView *)imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
    }
    return _imageView;
}

- (TFUILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[TFUILabel alloc] init];
        _titleLabel.numberOfLines = 0;
    }
    return _titleLabel;
}

- (TFUILabel *)detailLabel {
    if (!_detailLabel) {
        _detailLabel = [[TFUILabel alloc] init];
        _detailLabel.numberOfLines = 0;
    }
    return _detailLabel;
}

- (TFUIButton *)button {
    if (!_button) {
        _button = [TFUIButton buttonWithType:UIButtonTypeCustom];
        
    }
    return _button;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
