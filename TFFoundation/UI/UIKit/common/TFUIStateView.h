//
//  TFUIStateView.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/19.
//

#import <UIKit/UIKit.h>
#import <TFFoundation/TFUILabel.h>
#import <TFFoundation/TFUIButton.h>

NS_ASSUME_NONNULL_BEGIN

@class TFUIStateView;

typedef void(^TFUIStateViewButtonActionBlock)(TFUIStateView * _Nonnull stateView, TFUIButton * _Nonnull sender);///<按钮动作block

/**
 状态view，从上至下，由imageView、title、detail组成
 */
@interface TFUIStateView : UIView

/**
 根据自定义view创建stateview,

 @param customView 自定义view
 @warning 自定义view的布局需要自己指定
 @return TFUIStateView
 */
- (instancetype)initWithCustomView:(__kindof  UIView * _Nonnull)customView;
@property (nonatomic, strong, readonly, nonnull) __kindof UIView *customView;///<自定义view

/**
 内容view
 */
@property (nonatomic, strong, readonly, nonnull) UIView *contentView;
@property (nonatomic, strong, readonly, nonnull) UIImageView *imageView;///<图片
@property (nonatomic, strong, readonly, nonnull) TFUILabel *titleLabel;///<标题
@property (nonatomic, strong, readonly, nonnull) TFUILabel *detailLabel;///<详情
@property (nonatomic, strong, readonly, nonnull) TFUIButton *button;///<按钮
@property(nonatomic, copy) TFUIStateViewButtonActionBlock buttonActionBlock;///<按钮点击block


/**
 设置子view的约束，可重写此方法改写布局。
 */
- (void)makeSubViewsConstraints;

@property(nonatomic, assign) UIOffset contentViewOffset;///<内容的偏移，默认UIOffsetZero
@property(nonatomic, assign) CGFloat verticalSpace;///<竖直方向间距，默认为10
@property(nonatomic, assign) CGFloat imageTitlePadding;///<图片和标题之间增加的间距，默认值为0，真实间距为：verticalSpace+imageTitlePadding
@property(nonatomic, assign) CGFloat titleDetailPadding;///<标题和详情之间增加的间距，默认值为0，真实间距为：verticalSpace+titleDetailPadding
@property(nonatomic, assign) CGFloat detailButtonPadding;///<详情和按钮之间增加的间距，默认值为0，真实间距为：verticalSpace+detailButtonPadding

@end

NS_ASSUME_NONNULL_END
