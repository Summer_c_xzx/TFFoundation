//
//  TFUITextView.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 自定义textView，支持placeholder
 */
@interface TFUITextView : UITextView

/**
 *   placeholder 的文字
 */
@property(nonatomic, copy) IBInspectable NSString *placeholder;

/**
 *  placeholder 文字的颜色，默认[UIColor lightGrayColor]
 */
@property(nonatomic, strong) IBInspectable UIColor *placeholderColor;

/**
 *  placeholder 在默认位置上的偏移（默认位置会自动根据 textContainerInset、contentInset 来调整）
 */
@property(nonatomic, assign) UIEdgeInsets placeholderMargins;

@end

NS_ASSUME_NONNULL_END
