//
//  TFUILabel.h
//  Pods-TFFoundation
//
//  Created by 夏之祥 on 2019/4/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TFUILabel : UILabel

/**
 控制label内容的padding，默认为UIEdgeInsetsZero
 */
@property(nonatomic,assign) UIEdgeInsets contentEdgeInsets;

@end

NS_ASSUME_NONNULL_END
