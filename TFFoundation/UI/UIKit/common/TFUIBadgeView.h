//
//  TFUIBadgeView.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/24.
//

#import <TFFoundation/TFUILabel.h>

/**
 角标view，用于未读消息等红色小圆点+数字，默认为圆形，当文字长度过长时，为圆角显示。
 */
@interface TFUIBadgeView : TFUILabel


@end
