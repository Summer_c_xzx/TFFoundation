//
//  TFUITextField.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TFUITextField : UITextField

/**
 *  修改 placeholder 的颜色，默认[UIColor lightGrayColor]。
 */
@property(nonatomic, strong) IBInspectable UIColor *placeholderColor;

/**
 *  文字在输入框内的 padding。如果出现 clearButton，则 textInsets.right 会控制 clearButton 的右边距
 *
 *  默认为 TextFieldTextInsets
 */
@property(nonatomic, assign) UIEdgeInsets textInsets;

@end

NS_ASSUME_NONNULL_END
