//
//  TFUITabView.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/26.
//

#import <UIKit/UIKit.h>

/**
 指示器样式
 */
typedef NS_ENUM(NSUInteger, TFUITabIndicatorViewStyle) {
    TFUITabIndicatorViewStyleLine,///<线
    TFUITabIndicatorViewStyleOval,///<椭圆
    TFUITabIndicatorViewStyleCustom,///<自定义
};

/**
 默认指示器view
 */
@interface TFUITabIndicatorView : UIView

/**
 根据样式创建指示器view
 
 @param style 样式类型
 @return TFUITabIndicatorView
 */
- (nonnull instancetype)initWithStyle:(TFUITabIndicatorViewStyle)style;
@property(nonatomic, assign) TFUITabIndicatorViewStyle style;///<样式

@end



@class TFUITabView;

/**
 TFUITabViewDelegate代理
 */
@protocol TFUITabViewDelegate <NSObject>

/**
 选择某个标签时调用此代理方法

 @param tabView 标签view
 @param index 索引
 @param animated 是否动画
 */
- (void)tabView:(nonnull TFUITabView *)tabView didSelectTabAtIndex:(NSInteger)index animated:(BOOL)animated;

@end

/**
 选中某个标签的block

 @param tabView 标签view
 @param index 索引
 @param animated 是否动画
 */
typedef void(^TFUITabViewDidSelectTabBlock)(TFUITabView * _Nonnull  tabView, NSInteger index, BOOL animated);

/**
 切换标签页
 */
@interface TFUITabView : UIView

/**
 数据源数组
 
 @param dataSourceArray 数据源
 @return TFUITabView
 */
- (nonnull instancetype)initWithDataSourceArray:(nonnull NSArray *)dataSourceArray;
@property(nonatomic, strong, nonnull) NSArray *dataSourceArray;///<标题数组

/**
 初始化后调用此方法，可在此方法里进行设置一些默认配置
 */
- (void)didInitialize;

/**
 显示的Collectonview
 */
@property(nonatomic, strong, readonly, nonnull) UICollectionView *collectionView;

/**
 标签索引，若需选中某个索引调用方法：selectTabAtIndex
 */
@property(nonatomic, assign, readonly) NSInteger tabIndex;

@property(nonatomic, weak, nullable) id<TFUITabViewDelegate> delegate;///<代理
@property(nonatomic, copy, nullable) TFUITabViewDidSelectTabBlock didSelectTabBlock;///<选中某个标签的block

@property(nonatomic, strong, nonnull) TFUITabIndicatorView *indicatorView;///<指示器view

/**
 根据当前指示器样式和对应的cellframe返回对应的frame，返回对应的cell最终状态下的frame，
 TFUITabIndicatorViewStyleLine样式frame:UIEdgeInsetsMake(cellFrame.size.height-2.0, 0.0, 0.0, 0.0)
 TFUITabIndicatorViewStyleOval样式frame:UIEdgeInsetsInsetRect(cellFrame, UIEdgeInsetsMake(4.0, -10.0, 4.0, -10.0))
 TFUITabIndicatorViewStyleCustom样式frame:需子类自己制定，默认返回的frame为CGRectZero
 @param cellFrame cellFrame
 @param cellIndex cellIndex
 @return CGRect
 */
- (CGRect)indicatorViewFrameInCurrentStyleWithCellFrame:(CGRect)cellFrame cellIndex:(NSInteger)cellIndex;

/**
 更新指示器view

 @param targetFrame 目标大小
 @param animated 是否动画
 */
- (void)updateIndicatorViewWithTargetFrame:(CGRect)targetFrame animated:(BOOL)animated;

/**
 过渡过程中调用此方法更新样式
 
 @param currentCell 当前cell
 @param currentCellIndex 左边cell索引
 @param nextCell 右边cell
 @param nextCellIndex 右边cell索引
 @param percent 进度
 */
- (void)transitionWithCurrentCell:(nonnull __kindof UICollectionViewCell *)currentCell
                 currentCellIndex:(NSInteger)currentCellIndex
                         nextCell:(nonnull __kindof UICollectionViewCell *)nextCell
                    nextCellIndex:(NSInteger)nextCellIndex
                          percent:(float)percent;

/**
 cell的类，默认返回值为UICollectionViewCell class，会在didInitialize方法中注册此class的cell，若多种样式则重写didInitialize即可

 @return Class
 */
- (nonnull Class)cellClass;

/**
 cell对应的size

 @param index 索引
 @return CGSize
 */
- (CGSize)cellSizeAtIndex:(NSInteger)index;

/**
 更新对应的cell方法

 @param cell UICollectionViewCell
 @param index 索引
 */
- (void)updateCell:(nonnull __kindof UICollectionViewCell *)cell atIndex:(NSInteger)index;

/**
 选中某个cell时调用
 
 @param cell cell
 @param index 索引
 */
- (void)didSelectCell:(nonnull __kindof UICollectionViewCell *)cell atIndex:(NSInteger)index;

/**
 取消选中某个cell时调用
 
 @param cell cell
 @param index 索引
 */
- (void)didDeselectCell:(nonnull __kindof UICollectionViewCell *)cell atIndex:(NSInteger)index;

/**
 选择标签索引时调用
 
 @param index 索引
 @param animated 是否动画
 */
- (void)selectTabAtIndex:(NSInteger)index animated:(BOOL)animated;

/**
 相关联的scrollView滚动时调用此方法

 @param associatedScrollView 相关联的ScrollView
 */
- (void)associatedScrollViewDidScroll:(nonnull UIScrollView *)associatedScrollView;

/**
 相关联的scrollView滚动时调用此方法，若不需要调整位移调用associatedScrollViewDidScroll
 
 @param associatedScrollView 相关联的ScrollView
 @param contentOffset 当前位移，计算时根据此位移计算页数以及进度，可通过此属性实现类似UIPageViewController中重用的scrollView自身位移不准确的问题
 @warning 需在相关联的scroll实时滚动时手动调用此方法
 */
- (void)associatedScrollView:(nonnull UIScrollView *)associatedScrollView contentOffsetDidChange:(CGPoint)contentOffset;

@end


/**
 文本类的切换view
 */
@interface TFUITextTabView : TFUITabView

@property(nonatomic, assign) CGFloat textMinSpacing;///<文本间的最小间距，默认值为10
@property(nonatomic, assign) CGFloat textLeftInset;///<文本最左边的留白，默认值为10
@property(nonatomic, assign) CGFloat textRightInset;///<文本最右边的留白，默认值为10
@property(nonatomic, strong, nonnull) UIFont *textFont;///<文本字体，默认14
@property(nonatomic, strong, nonnull) UIColor *textColor;///<文本颜色，默认black
@property(nonatomic, strong, nonnull) UIFont *textSelectedFont;///<文本选中后的字体，默认14
@property(nonatomic, strong, nonnull) UIColor *textSelectedColor;///<文本选择后的颜色，默认blue

@end


