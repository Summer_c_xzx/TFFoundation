//
//  TFUIBadgeView.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/24.
//

#import "TFUIBadgeView.h"
//ui
#import "TFUIStyle.h"

@interface TFUIBadgeView ()


@end

@implementation TFUIBadgeView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.textAlignment = NSTextAlignmentCenter;
        self.backgroundColor = TFUICurrentStyle().redColor;
        self.textColor = TFUICurrentStyle().whiteColor;
        self.font = TFUIFont(8.0);
        self.contentEdgeInsets = UIEdgeInsetsMake(4.0, 4.0, 4.0, 4.0);
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.layer.cornerRadius = CGRectGetHeight(self.frame)/2.0;
    self.layer.masksToBounds = YES;
}


- (CGSize)sizeThatFits:(CGSize)size {
    size = [super sizeThatFits:size];
    size.width = MAX(size.width, size.height);
    return size;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
