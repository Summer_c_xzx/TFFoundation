//
//  TFUILabel.m
//  Pods-TFFoundation
//
//  Created by 夏之祥 on 2019/4/17.
//

#import "TFUILabel.h"

@implementation TFUILabel


- (void)setContentEdgeInsets:(UIEdgeInsets)contentEdgeInsets {
    _contentEdgeInsets = contentEdgeInsets;
    [self setNeedsDisplay];
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGFloat horizontalInset = self.contentEdgeInsets.left + self.contentEdgeInsets.right;
    CGFloat verticalInset = self.contentEdgeInsets.top + self.contentEdgeInsets.bottom;

    size = [super sizeThatFits:CGSizeMake(size.width - horizontalInset, size.height - verticalInset)];
    size.width += horizontalInset;
    size.height += verticalInset;
    return size;
}

- (CGSize)intrinsicContentSize {
    CGFloat preferredMaxLayoutWidth = self.preferredMaxLayoutWidth;
    if (preferredMaxLayoutWidth <= 0) {
        preferredMaxLayoutWidth = CGFLOAT_MAX;
    }
    return [self sizeThatFits:CGSizeMake(preferredMaxLayoutWidth, CGFLOAT_MAX)];
}

- (void)drawTextInRect:(CGRect)rect {
    rect = UIEdgeInsetsInsetRect(rect, self.contentEdgeInsets);
    // 在某些情况下文字位置错误，因此做了如下保护
    if (self.numberOfLines == 1 && (self.lineBreakMode == NSLineBreakByWordWrapping || self.lineBreakMode == NSLineBreakByCharWrapping)) {
        rect.size.height = CGRectGetHeight(rect) + self.contentEdgeInsets.top * 2;
    }
    [super drawTextInRect:rect];
}



@end
