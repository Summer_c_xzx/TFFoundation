//
//  TFWebViewController.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/4/14.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

/**
 用户浏览web网页的视图浏览器
 */
@interface TFWebViewController : UIViewController

/**
 打开的url地址，可为本地地址，或者网络地址
 */
@property (nonatomic, strong, nullable) NSURL *url;

/**
 打开url的request，与上面的url属性二者选一
 */
@property (nonatomic, strong, nullable) NSURLRequest *request;

/**
 web view
 */
@property (nonatomic, strong, readonly, nonnull) WKWebView *webView;

/**
 加载进度view
 */
@property (nonatomic, strong, readonly, nonnull) UIProgressView *progressView;

@end
