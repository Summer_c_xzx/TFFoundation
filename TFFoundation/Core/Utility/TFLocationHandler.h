//
//  TFLocationHandler.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/7/14.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <CoreLocation/CoreLocation.h>

/**
 定位结果block

 @param location 定位结果
 @param error 错误
 */
typedef void(^TFLocationHandlerLocationResultBlock)(CLLocation * _Nullable location, NSError * _Nullable error);

/**
 定位和反解析结果block

 @param location 定位结果
 @param place 反解析位置
 @param error 错误
 */
typedef void(^TFLocationHandlerLocationAndGeoResultBlock)(CLLocation * _Nullable location, CLPlacemark * _Nullable place, NSError * _Nullable error);

/**
 系统定位工具类，使用时需将TFPhotoLocationHelper对象设置为属性.
 */
@interface TFLocationHandler : NSObject

/**
 当前位置坐标
 */
@property (nonatomic, strong, nullable,readonly) CLLocation *location;

/**
 重新开始定位
 */
- (void)restartLocate;

/**
 开始定位获取定位结果
 
 @param resultBlock 结果返回block
 */
- (void)startLocateWithResult:(nullable TFLocationHandlerLocationResultBlock)resultBlock;

/**
 开始定位并进行反编码解析
 
 @param geoResultBlock 结果返回block
 */
- (void)startLocateAndGetGeoResult:(nullable TFLocationHandlerLocationAndGeoResultBlock)geoResultBlock;

@end
