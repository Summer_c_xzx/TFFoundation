//
//  NSDate+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/13.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 NSDate常用的类别
 */
@interface NSDate (TFCore)

///=============================================================================
/// @name 常见日期处理和获取
///=============================================================================

@property (nonatomic, assign, readonly) NSInteger tf_year;///<年份
@property (nonatomic, assign, readonly) NSInteger tf_month;///<月份
@property (nonatomic, assign, readonly) NSInteger tf_day;///<日
@property (nonatomic, assign, readonly) NSInteger tf_hour;///<小时
@property (nonatomic, assign, readonly) NSInteger tf_minute;///<分钟
@property (nonatomic, assign, readonly) NSInteger tf_second;///<秒数
@property (nonatomic, assign, readonly) NSInteger tf_weekday;///<周几
@property (nonatomic, assign, readonly) BOOL tf_isLeapYear;///<是否闰年
@property (nonatomic, assign, readonly) BOOL tf_isLeapMonth;///<是否闰月
@property (nonatomic, assign, readonly) BOOL tf_isToday;///<是否今天
@property (nonatomic, assign, readonly) BOOL tf_isYesterday;///<是否昨天

/**
 获取日期中的元素的值，比如年月日等

 @param unit 单元类型
 @return NSInteger
 */
- (NSInteger)tf_componentWithCalendarUnit:(NSCalendarUnit)unit;

/**
 一天开始的日期

 @return NSDate
 */
- (nonnull NSDate *)tf_startDateOfDay;

/**
 一天的结束日期
 
 @return NSDate.
 */
- (nonnull NSDate *)tf_endDateOfDay;

/**
 一周的起始日期

 @return NSDate.
 */
- (nonnull NSDate *)tf_startDateOfWeek;

/**
 一周的结束日期
 
 @return NSDate.
 */
- (nonnull NSDate *)tf_endDateOfWeek;

/**
 一月的起始日期
 
 @return NSDate.
 */
- (nonnull NSDate *)tf_startDateOfMonth;

/**
 一月的结束日期
 
 @return NSDate.
 */
- (nonnull NSDate *)tf_endDateOfMonth;

/**
 一年的起始日期
 
 @return NSDate.
 */
- (nonnull NSDate *)tf_startDateOfYear;

/**
 一年的结束日期
 
 @return NSDate.
 */
- (nonnull NSDate *)tf_endDateOfYear;

/**
 根据当前日期通过添加年数获取新的日期

 @param year 添加的year数,若为负数则为减少
 @return NSDate
 */
- (nonnull NSDate *)tf_dateByAddingYear:(NSInteger)year;

/**
 根据当前日期通过添加月数获取新的日期

 @param month 添加的月数，若为负数则减少
 @return NSDate
 */
- (nonnull NSDate *)tf_dateByAddingMonth:(NSInteger)month;

/**
 根据当前日期通过添加天数获取新的日期

 @param day 添加的天数，若为负数则减小
 @return NSDate
 */
- (nonnull NSDate *)tf_dateByAddingDay:(NSInteger)day;

/**
 根据当前日期通过添加小时数获取新的日期

 @param hour 添加的小时数，若为负数则减小
 @return NSDate
 */
- (nonnull NSDate *)tf_dateByAddingHour:(NSInteger)hour;

/**
 根据当前日期通过添加分钟数获取新的日期

 @param minute 添加的分钟数，若为负数则减小
 @return NSDate
 */
- (nonnull NSDate *)tf_dateByAddingMinute:(NSInteger)minute;

/**
 根据当前日期通过添加秒数获取新的日期

 @param second 添加的秒数，若为负数则减小
 @return NSDate
 */
- (nonnull NSDate *)tf_dateByAddingSecond:(NSInteger)second;

/**
 两个日期间相隔多少年

 @param date 比较的日期
 @return NSInteger，若为负数，则在为之前的日期，若为正数则在这之后的日期.
 */
- (NSInteger)tf_yearsCompareToDate:(nonnull NSDate *)date;

/**
 两个日期间相隔多少个月

 @param date 比较的日期
 @return NSInteger，若为负数，则在为之前的日期，若为正数则在这之后的日期.
 */
- (NSInteger)tf_monthsCompareToDate:(nonnull NSDate *)date;

/**
 两个日期间相隔多少天

 @param date 比较的日期
 @return NSInteger，若为负数，则在为之前的日期，若为正数则在这之后的日期.
 */
- (NSInteger)tf_daysCompareToDate:(nonnull NSDate *)date;

/**
 两个日期间相隔多少小时
 
 @param date 比较的日期
 @return NSInteger，若为负数，则在为之前的日期，若为正数则在这之后的日期.
 */
- (NSInteger)tf_hoursCompareToDate:(nonnull NSDate *)date;

/**
 两个日期间相隔多少分钟

 @param date 比较的日期
 @return NSInteger，若为负数，则在为之前的日期，若为正数则在这之后的日期.
 */
- (NSInteger)tf_minutesCompareToDate:(nonnull NSDate *)date;

/**
 获取两个日期间相差的Components.

 @param date 比较的日期
 @param unitFlags 比较的单位
 @return NSDateComponents
 */
- (nonnull NSDateComponents *)tf_dateComponentsCompareToDate:(nonnull NSDate *)date withUnitFlags:(NSCalendarUnit)unitFlags;

/**
 根据格式化字符串获取对应的日期字符串

 @param format 格式化字符串,列：日期为：2017.03.13 22:22,根据格式化字符串：“年:yyyy,月:MM,MMM,MMMM,日:d,dd,星期:EEE,EEEE,小时:hh,h,HH,H,分钟:mm”进行格式化结果如下
 > >
 美国：年:2017,月:03,Mar,March,日:13,13,星期:Mon,Monday,小时:10,10,22,22,分钟:22
 > >
 中国：年:2017,月:03,3月,三月,日:13,13,星期:周一,星期一,小时:10,10,22,22,分钟:22
 @see 格式化字符串格式请看：http://www.jianshu.com/p/e1e83dd9241b
 @return NSString
 */
- (nonnull NSString *)tf_dateStringWithFormat:(nonnull NSString *)format;

/**
 获取ISO：yyyy-MM-dd HH:mm:ss格式化对应的字符串
 
 @return NSString
 */
- (nonnull NSString *)tf_dateISOFormatString;

/**
 获取yyyy-MM-dd HH:mm对应的格式化字符串

 @return NSString
 */
- (nonnull NSString *)tf_dateYMDHMFormatString;

/**
 获取yyyy-MM-dd格式化对应的日期字符串

 @return NSString
 */
- (nonnull NSString *)tf_dateYMDFormatString;

/**
 获取yyyy年 MMM dd日格式化对应的字符串

 @return NSString
 */
- (nonnull NSString *)tf_dateChineseYMDFormatString;

/**
 根据日期和格式化字符串获取NSDate

 @param dateString 日期字符串36
 @param format 格式化字符串
 @return NSDate
 */
+ (nonnull NSDate *)tf_dateWithDateString:(nonnull NSString *)dateString format:(nonnull NSString *)format;

/**
 获取对应的时间描述日期字符串：x分钟前/x小时前/昨天/x天前/x个月前/x年前

 @return NSString
 */
- (nonnull NSString *)tf_timeString;

@end
