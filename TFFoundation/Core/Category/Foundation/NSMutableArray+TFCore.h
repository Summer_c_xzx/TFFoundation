//
//  NSMutableArray+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/10.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSArray+TFCore.h"

/**
 NSMutableArray常用的类别
 */
@interface NSMutableArray (TFCore)

///=============================================================================
/// @name 快速处理数据方法--包含筛选、匹配、排序等等。
///=============================================================================

/**
 从可变数组中筛选出符合条件的元素

 @param predicateBlock > 筛选条件的block
 >
 根据返回的BOOL值返回是否筛选的为该元素，参数如下：
 > >
 obj 数组中的元素
 */
- (void)tf_filterWithPredicateBlock:(nonnull TFArrayPredicateBlock)predicateBlock;

/**
 从可变数组中根据谓词筛选出符合条件的元素

 @param predicateFormat 谓词格式化字符串
 @param ... 格式化字符串
 */
- (void)tf_filterWithPredicateFormat:(nonnull NSString *)predicateFormat, ...;

/**
 根据是否升序排列数据

 @param ascending 是否升序
 */
- (void)tf_sortWithAscending:(BOOL)ascending;

/**
 根据描述字典排序数组

 @param descriptorDic > 筛选字典格式如下:
 >>
 key:筛选的字符串，可以为字典的key或者数据模型的属性，以及属性的属性.
 >>
 value:是否升序
 >>
 示例：@{@"propertyNameOrKeyName":@NO}
 
 */
- (void)tf_sortWithDescriptorDic:(nonnull NSDictionary<NSString *,NSNumber *> *)descriptorDic;

@end
