//
//  NSData+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/13.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 NSData的常用类别
 */
@interface NSData (TFCore)

/**
 通过NSData转换成md5格式的NSData

 @return NSData
 */
- (nonnull NSData *)tf_md5Data;

/**
 通过NSData获取小写的md5加密格式字符串

 @return NSString
 */
- (nonnull NSString *)tf_md5String;

/**
 根据NSData获取UTF8编码格式的字符串

 @return NSString
 */
- (nonnull NSString *)tf_utf8EncodedString;

/**
 将字符串转经过base64格式编码之后，转换成NSData

 @param string 字符串
 @return NSData
 */
+ (nonnull NSData *)tf_dataWithBase64EncodedString:(nonnull NSString *)string;

/**
 根据NSData获取Base64编码格式的字符串

 @return NSString
 */
- (nonnull NSString *)tf_base64EncodedString;

/**
 根据NSData和换行长度获取Base64编码格式的字符串

 @param wrapWidth 换行长度
 @return NSString
 */
- (nonnull NSString *)tf_base64EncodedStringWithWrapWith:(NSInteger)wrapWidth;

/**
 进行AES加密数据

 @param key 长度一般为16（AES算法所能支持的密钥长度可以为128,192,256位（也即16，24，32个字节））
 @param iv iv description
 @return NSData
 */
- (nonnull NSData *)tf_encryptedWithAESUsingKey:(nonnull NSString *)key iv:(nullable NSData *)iv;

/**
 进行AES解密数据

 @param key 长度一般为16 （AES算法所能支持的密钥长度可以为128,192,256位（也即16，24，32个字节））
 @param iv iv description
 @return NSData
 */
- (nonnull NSData *)tf_decryptedWithAESUsingKey:(nonnull NSString *)key iv:(nullable NSData *)iv;

/**
 进行DES加密数据
 
 @param key 长度一般为8
 @param iv iv description
 @return NSData
 */
- (nonnull NSData *)tf_encryptedWithDESUsingKey:(nonnull NSString *)key iv:(nullable NSData *)iv;

/**
 进行DES解密数据
 
 @param key 长度一般为8
 @param iv iv description
 @return NSData
 */
- (nonnull NSData *)tf_decryptedWithDESUsingKey:(nonnull NSString *)key iv:(nullable NSData *)iv;

/**
 将NSData转换成json对象

 @return id
 */
- (nullable id)tf_decodeToJsonObject;

/**
 对NSData根据压缩级别进行GZip压缩

 @param level 压缩级别
 @return 压缩后的NSData
 */
- (nonnull NSData *)tf_gZippedDataWithCompressionLevel:(float)level;

/**
 对NSData进行GZip压缩，默认级别-1.

 @return 压缩后的NSData.
 */
- (nonnull NSData *)tf_gZippedData;

/**
 对NSData进行GZip解压缩

 @return 解压缩后的NSData
 */
- (nonnull NSData *)tf_gUnZippedData;

/**
 判断一个NSData是否GZip压缩后的数据

 @return BOOL
 */
- (BOOL)tf_isGZippedData;

@end
