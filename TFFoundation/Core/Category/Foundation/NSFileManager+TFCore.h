//
//  NSFileManager+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/14.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 NSFileManager常用类别
 */
@interface NSFileManager (TFCore)

/**
 根据目录类型获取对应的NSURL

 @param directory 目录类型
 @return NSURL
 */
+ (nonnull NSURL *)tf_urlForDirectory:(NSSearchPathDirectory)directory;

/**
 根据目录类型获取对应的路径字符串

 @param directory 目录类型
 @return NSString
 */
+ (nonnull NSString *)tf_pathForDirectory:(NSSearchPathDirectory)directory;

/**
 获取沙盒中文档的路径对应的NSURL

 @return NSURL
 */
+ (nonnull NSURL *)tf_documentURL;

/**
 获取沙盒中文档的路径地址

 @return NSString
 */
+ (nonnull NSString *)tf_documentPath;

/**
 获取沙盒中library的路径对应的NSURL
 
 @return NSURL
 */
+ (nonnull NSURL *)tf_libraryURL;

/**
 获取沙盒中library的路径地址
 
 @return NSString
 */
+ (nonnull NSString *)tf_libraryPath;

/**
 获取沙盒中caches的路径对应的NSURL
 
 @return NSURL
 */
+ (nonnull NSURL *)tf_cachesURL;

/**
 获取沙盒中caches的路径地址
 
 @return NSString
 */
+ (nonnull NSString *)tf_cachesPath;

/**
 获取某个路径下的文件大小，单位字节数，kb

 @param path 文件地址
 @return NSInteger 字节数
 */
+ (NSInteger)tf_fileSizeAtPath:(nonnull NSString *)path;

/**
 获取某个文件夹的大小，单位MB

 @param  path 文件夹路径
 @return double
 */
+ (double)tf_getFolderSizeWithPath:(nonnull NSString *)path;

/**
 根据文件夹路径删除文件夹下的所有文件

 @param  path 文件夹路径
 @return BOOL
 */
+ (BOOL)tf_deleteFolderWithPath:(nonnull NSString *)path;

@end
