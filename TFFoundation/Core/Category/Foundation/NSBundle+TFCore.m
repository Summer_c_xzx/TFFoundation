//
//  NSBundle+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/14.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "NSBundle+TFCore.h"

@implementation NSBundle (TFCore)

+ (UIImage *)tf_appIconImage {
    return [UIImage imageNamed:@"AppIcon60x60"];
}

@end
