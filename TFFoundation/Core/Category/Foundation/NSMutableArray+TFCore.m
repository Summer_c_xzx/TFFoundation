//
//  NSMutableArray+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/10.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "NSMutableArray+TFCore.h"

@implementation NSMutableArray (TFCore)

- (void)tf_filterWithPredicateBlock:(TFArrayPredicateBlock)predicateBlock {
    typeof(self) __weak weakSelf = self;

    [self filterUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        return predicateBlock(evaluatedObject,[weakSelf indexOfObject:evaluatedObject]);
    }]];
}

- (void)tf_filterWithPredicateFormat:(NSString *)predicateFormat, ... {
    va_list args;
    va_start(args, predicateFormat);
    predicateFormat = [[NSString alloc] initWithFormat:predicateFormat arguments:args];
    va_end(args);    
    [self filterUsingPredicate:[NSPredicate predicateWithFormat:predicateFormat]];
}

- (void)tf_sortWithAscending:(BOOL)ascending {
    NSSortDescriptor *sortDesc = [NSSortDescriptor sortDescriptorWithKey:nil ascending:ascending];
    [self sortUsingDescriptors:@[sortDesc]];
}

- (void)tf_sortWithDescriptorDic:(NSDictionary<NSString *,NSNumber *> *)descriptorDic {
    NSMutableArray *descripArray = @[].mutableCopy;
    [descriptorDic enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, NSNumber * _Nonnull obj, BOOL * _Nonnull stop) {
        [descripArray addObject:[NSSortDescriptor sortDescriptorWithKey:key ascending:[obj boolValue]]];
    }];
    return [self sortUsingDescriptors:descripArray];
}

@end
