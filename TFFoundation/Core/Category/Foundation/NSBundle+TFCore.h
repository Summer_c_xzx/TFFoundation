//
//  NSBundle+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/14.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 NSBundle的常用类别
 */
@interface NSBundle (TFCore)

/**
 获取应用图标图片

 @return UIImage
 */
+ (nonnull UIImage *)tf_appIconImage;

@end
