//
//  NSDate+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/13.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "NSDate+TFCore.h"

@implementation NSDate (TFCore)

- (NSInteger)tf_year {
    return [self tf_componentWithCalendarUnit:NSCalendarUnitYear];
}

- (NSInteger)tf_month {
    return [self tf_componentWithCalendarUnit:NSCalendarUnitMonth];
}

- (NSInteger)tf_day {
    return [self tf_componentWithCalendarUnit:NSCalendarUnitDay];
}

- (NSInteger)tf_hour {
    return [self tf_componentWithCalendarUnit:NSCalendarUnitHour];
}

- (NSInteger)tf_minute {
    return [self tf_componentWithCalendarUnit:NSCalendarUnitMinute];
}

- (NSInteger)tf_second {
    return [self tf_componentWithCalendarUnit:NSCalendarUnitSecond];
}

- (NSInteger)tf_weekday {
    return [self tf_componentWithCalendarUnit:NSCalendarUnitWeekday];
}

- (BOOL)tf_isLeapYear {
    NSUInteger year = self.tf_year;
    return ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0)));
}

- (BOOL)tf_isLeapMonth {
    return [[[NSCalendar currentCalendar] components:NSCalendarUnitQuarter fromDate:self] isLeapMonth];
}

- (BOOL)tf_isToday {
    if (fabs(self.timeIntervalSinceNow) >= 60 * 60 * 24) return NO;
    return [NSDate date].tf_day == self.tf_day;
}

- (BOOL)tf_isYesterday {
    NSDate *added = [self tf_dateByAddingDay:1];
    return [added tf_isToday];
}


- (NSInteger)tf_componentWithCalendarUnit:(NSCalendarUnit)unit {
    return [[NSCalendar currentCalendar] component:unit fromDate:self];
}

- (NSDate *)tf_startDateOfDay {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:self];
    return [calendar dateFromComponents:dateComponents];
}

- (NSDate *)tf_endDateOfDay {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:self];
    dateComponents.hour = 23;
    dateComponents.minute = 59;
    dateComponents.second = 59;
    return [calendar dateFromComponents:dateComponents];
}

- (NSDate *)tf_startDateOfWeek {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekday|NSCalendarUnitDay fromDate:self];
    NSUInteger offset = ([components weekday] == [calendar firstWeekday]) ? 6 : [components weekday] - 2;
    [components setDay:[components day] - offset];
    return [calendar dateFromComponents:components];
}

- (NSDate *)tf_endDateOfWeek {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setWeekOfMonth:1];
    return [[calendar dateByAddingComponents:components toDate:[self tf_startDateOfWeek] options:0] dateByAddingTimeInterval:-1];
}

- (NSDate *)tf_startDateOfMonth {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth  fromDate:self];
    return [calendar dateFromComponents:components];
}

- (NSDate *)tf_endDateOfMonth {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setMonth:1];
    return [[calendar dateByAddingComponents:components toDate:[self tf_startDateOfMonth] options:0] dateByAddingTimeInterval:-1];
}

- (NSDate *)tf_startDateOfYear {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear   fromDate:self];
    return [calendar dateFromComponents:components];
}

- (NSDate *)tf_endDateOfYear {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:1];
    return [[calendar dateByAddingComponents:components toDate:[self tf_startDateOfYear] options:0] dateByAddingTimeInterval:-1];
}

- (NSDate *)tf_dateByAddingYear:(NSInteger)year {
    NSDateComponents *addDateComponents = [[NSDateComponents alloc] init];
    addDateComponents.year = year;
    return [[NSCalendar currentCalendar] dateByAddingComponents:addDateComponents toDate:self options:0];
}

- (NSDate *)tf_dateByAddingMonth:(NSInteger)month {
    NSDateComponents *addDateComponents = [[NSDateComponents alloc] init];
    addDateComponents.month = month;
    return [[NSCalendar currentCalendar] dateByAddingComponents:addDateComponents toDate:self options:0];
}

- (NSDate *)tf_dateByAddingDay:(NSInteger)day {
    NSDateComponents *addDateComponents = [[NSDateComponents alloc] init];
    addDateComponents.day = day;
    return [[NSCalendar currentCalendar] dateByAddingComponents:addDateComponents toDate:self options:0];
}

- (NSDate *)tf_dateByAddingHour:(NSInteger)hour {
    NSDateComponents *addDateComponents = [[NSDateComponents alloc] init];
    addDateComponents.hour = hour;
    return [[NSCalendar currentCalendar] dateByAddingComponents:addDateComponents toDate:self options:0];
}

- (NSDate *)tf_dateByAddingMinute:(NSInteger)minute {
    NSDateComponents *addDateComponents = [[NSDateComponents alloc] init];
    addDateComponents.minute = minute;
    return [[NSCalendar currentCalendar] dateByAddingComponents:addDateComponents toDate:self options:0];
}

- (NSDate *)tf_dateByAddingSecond:(NSInteger)second {
    NSDateComponents *addDateComponents = [[NSDateComponents alloc] init];
    addDateComponents.second = second;
    return [[NSCalendar currentCalendar] dateByAddingComponents:addDateComponents toDate:self options:0];
}

- (NSInteger)tf_yearsCompareToDate:(NSDate *)date {
    return [self tf_dateComponentsCompareToDate:date withUnitFlags:NSCalendarUnitYear].year;
}

- (NSInteger)tf_monthsCompareToDate:(NSDate *)date {
    return [self tf_dateComponentsCompareToDate:date withUnitFlags:NSCalendarUnitMonth].month;
}

- (NSInteger)tf_daysCompareToDate:(NSDate *)date {
    NSDate *toDate;
    NSTimeInterval timeInterval = [self timeIntervalSinceDate:date];
    if (timeInterval>0) {
        toDate = [date tf_startDateOfDay];
    }
    else if(timeInterval<0){
        toDate = [date tf_endDateOfDay];
    }
    else {
        toDate = date;
    }
    return [self tf_dateComponentsCompareToDate:toDate withUnitFlags:NSCalendarUnitDay].day;
}

- (NSInteger)tf_hoursCompareToDate:(NSDate *)date {
    return [self tf_dateComponentsCompareToDate:date withUnitFlags:NSCalendarUnitHour].hour;
}

- (NSInteger)tf_minutesCompareToDate:(NSDate *)date {
    return [self tf_dateComponentsCompareToDate:date withUnitFlags:NSCalendarUnitMinute].minute;
}

- (NSDateComponents *)tf_dateComponentsCompareToDate:(NSDate *)date withUnitFlags:(NSCalendarUnit)unitFlags {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [calendar components:unitFlags fromDate:self toDate:date options:0];
}

- (NSString *)tf_dateStringWithFormat:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    return [formatter stringFromDate:self];
}

- (NSString *)tf_dateISOFormatString {
    return [self tf_dateStringWithFormat:@"yyyy-MM-dd HH:mm:ss"];
}

- (NSString *)tf_dateYMDHMFormatString {
    return [self tf_dateStringWithFormat:@"yyyy-MM-dd HH:mm"];
}

- (NSString *)tf_dateYMDFormatString {
    return [self tf_dateStringWithFormat:@"yyyy-MM-dd"];
}

- (NSString *)tf_dateChineseYMDFormatString {
    return [self tf_dateStringWithFormat:@"yyyy年 MMM dd日"];
}

+ (NSDate *)tf_dateWithDateString:(NSString *)dateString format:(NSString *)format {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    return [formatter dateFromString:dateString];
}

- (NSString *)tf_timeString {
    NSTimeInterval seconds = -[self timeIntervalSinceNow];
    if (seconds>0) {
        if (seconds<60.0) {
            return @"刚刚";
        }
        else if (seconds<3600) {
            return [NSString stringWithFormat:@"%.0f分钟前",floorf(seconds/60.0)];
        }
        else {
            NSInteger years = labs([self tf_yearsCompareToDate:[NSDate date]]);
            if (years>0) {
                return [self tf_dateChineseYMDFormatString];
            }
            else {
                NSInteger days = labs([self tf_daysCompareToDate:[NSDate date]]);
                if (days>7) {
                    return [self tf_dateStringWithFormat:@"MMM dd日"];
                }
                else if (days>1) {
                    return [self tf_dateStringWithFormat:@"EEEE"];
                }
                else if (days==1) {
                    return [self tf_dateStringWithFormat:@"昨天 HH:mm"];
                }
                else {
                    NSTimeInterval hours = floorf(seconds/3600.0);
                    if (hours>5) {
                        return [self tf_dateStringWithFormat:@"今天 HH:mm"];
                    }
                    else {
                        return [NSString stringWithFormat:@"%.0f小时前",hours];
                    }
                }
            }
        }
    }
    return @"";
}



@end
