//
//  NSDictionary+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/11.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 根据原字典中的元素返回新的元素

 @param key 原字典中的键
 @param value 原字典中的值
 @return 返回新的对象
 */
typedef __nonnull id(^TFDicionaryMapBlock)(__nonnull id key, __nonnull id value);

/**
 NSDictionary常用的类别
 */
@interface NSDictionary (TFCore)

/**
 根据是否升序获取排序后的所有key的数组

 @param ascending 是否升序
 @return NSArray
 */
- (nonnull NSArray *)tf_sortedAllKeysArrayWithAscending:(BOOL)ascending;

/**
 获取适合格式化的json字符串

 @return NSString
 */
- (nullable NSString *)tf_prettyJsonString;

/**
 根据字典中的元素重新设置元素中的value获取一个新的字典，

 @param block 返回新的元素的block
 @return NSDictionary
 */
- (nonnull NSDictionary *)tf_mapValuesUsingBlock:(nonnull TFDicionaryMapBlock)block;

/**
 根据字典中的元素重新设置元素中的value获取一个新的字典

 @param options 遍历方式
 @param block 返回新的元素的block
 @return NSDictionary
 */
- (nonnull NSDictionary *)tf_mapValuesWithOptions:(NSEnumerationOptions)options usingBlock:(nonnull TFDicionaryMapBlock)block;


@end
