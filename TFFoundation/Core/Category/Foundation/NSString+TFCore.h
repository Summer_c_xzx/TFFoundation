//
//  NSString+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/14.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 NSString常用类别
 */
@interface NSString (TFCore)

///=============================================================================
/// @name 字符串编码和解码以及加密
///=============================================================================

/**
 获取字符串对应的md5字符串

 @return NSString
 */
- (nonnull NSString *)tf_md5String;

/**
 获取字符串对应的base64字符串

 @return NSString
 */
- (nonnull NSString *)tf_base64EncodedString;

/**
 获取字符串对应的url编码过后的字符串

 @return NSString
 */
- (nonnull NSString *)tf_urlEncodedString;

/**
 获取字符串对应的url解码过后的字符串

 @return NSString
 */
- (nonnull NSString *)tf_urlDecodeString;

/**
 对字符串进行aes加密

 @param key 秘钥
 @param iv 向量
 @return NSString
 */
- (nonnull NSString *)tf_encryptedWithAESUsingKey:(nonnull NSString *)key andIV:(nullable NSData*)iv;

/**
 对字符串进行aes解密
 
 @param key 秘钥
 @param iv 向量
 @return NSString
 */
- (nonnull NSString *)tf_decryptedWithAESUsingKey:(nonnull NSString *)key andIV:(nullable NSData*)iv;

/**
 对字符串进行des加密
 
 @param key 秘钥
 @param iv 向量
 @return NSString
 */
- (nonnull NSString *)tf_encryptedWithDESUsingKey:(nonnull NSString *)key andIV:(nullable NSData*)iv;

/**
 对字符串进行des解密
 
 @param key 秘钥
 @param iv 向量
 @return NSString
 */
- (nonnull NSString *)tf_decryptedWithDESUsingKey:(nonnull NSString *)key andIV:(nullable NSData*)iv;

///=============================================================================
/// @name 字符串大小计算
///=============================================================================

/**
 根据字体的大小和限制的大小获取对应的字符串size

 @param font 字体大小
 @param constrainedSize 约束的范围
 @param lineBreakMode 截取字符串的方式
 @return CGSize
 */
- (CGSize)tf_sizeForFont:(nonnull UIFont *)font constrainedSize:(CGSize)constrainedSize mode:(NSLineBreakMode)lineBreakMode;

/**
 根据字体大小获取获取对应字符串的size

 @param font 字体大小
 @return CGSize
 */
- (CGSize)tf_sizeForFont:(nonnull UIFont *)font;

/**
 根据字体的大小获取宽度

 @param font 字体大小
 @return CGFloat
 */
- (CGFloat)tf_widthForFont:(nonnull UIFont *)font;

/**
 根据字体的大小和约束的宽度来获取高度

 @param font 字体大小
 @param width 宽度
 @return CGFloat
 */
- (CGFloat)tf_heightForFont:(nonnull UIFont *)font width:(CGFloat)width;

///=============================================================================
/// @name 中文字符串转汉语拼音
///=============================================================================

/**
 将中文字符串转换成带声调的拼音字符串，例：重庆--chóng qìng

 @return NSString
 */
- (nonnull NSString *)tf_pinyinStringWithPhoneticSymbol;

/**
 将中文字符串转换成不带声调的拼音字符串，例：重庆--chong qing

 @return NSString
 */
- (nonnull NSString *)tf_pinyinString;

/**
 每个字的拼音字符串数组

 @return NSArray<NSString *> *
 */
- (nonnull NSArray<NSString *> *)tf_pinyinCharArray;

/**
 将中文字符转换成一个不带空格的完整字符串，例：重庆--chongqing

 @return NSString
 */
- (nonnull NSString *)tf_pinyinStringWithoutBlank;

/**
 获取字符串汉语拼音的首字母，默认小写

 @return NSString
 */
- (nonnull NSString *)tf_pinyinInitialString;

/**
 获取大写的首字母

 @return NSString
 */
- (nonnull NSString *)tf_pinyinUppercaseInitialString;

///=============================================================================
/// @name 正则表达式相关
///=============================================================================

/**
 *  正则表达式简单说明
 *  语法：
 .       匹配除换行符以外的任意字符
 \w      匹配字母或数字或下划线或汉字
 \s      匹配任意的空白符
 \d      匹配数字
 \b      匹配单词的开始或结束
 ^       匹配字符串的开始
 $       匹配字符串的结束
 *       重复零次或更多次
 +       重复一次或更多次
 ?       重复零次或一次
 {n} 	重复n次
 {n,} 	重复n次或更多次
 {n,m} 	重复n到m次
 \W      匹配任意不是字母，数字，下划线，汉字的字符
 \S      匹配任意不是空白符的字符
 \D      匹配任意非数字的字符
 \B      匹配不是单词开头或结束的位置
 [^x] 	匹配除了x以外的任意字符
 [^aeiou]匹配除了aeiou这几个字母以外的任意字符
 *?      重复任意次，但尽可能少重复
 +?      重复1次或更多次，但尽可能少重复
 ??      重复0次或1次，但尽可能少重复
 {n,m}? 	重复n到m次，但尽可能少重复
 {n,}? 	重复n次以上，但尽可能少重复
 \a      报警字符(打印它的效果是电脑嘀一声)
 \b      通常是单词分界位置，但如果在字符类里使用代表退格
 \t      制表符，Tab
 \r      回车
 \v      竖向制表符
 \f      换页符
 \n      换行符
 \e      Escape
 \0nn 	ASCII代码中八进制代码为nn的字符
 \xnn 	ASCII代码中十六进制代码为nn的字符
 \unnnn 	Unicode代码中十六进制代码为nnnn的字符
 \cN 	ASCII控制字符。比如\cC代表Ctrl+C
 \A      字符串开头(类似^，但不受处理多行选项的影响)
 \Z      字符串结尾或行尾(不受处理多行选项的影响)
 \z      字符串结尾(类似$，但不受处理多行选项的影响)
 \G      当前搜索的开头
 \p{name} 	Unicode中命名为name的字符类，例如\p{IsGreek}
 (?>exp) 	贪婪子表达式
 (?<x>-<y>exp) 	平衡组
 (?im-nsx:exp) 	在子表达式exp中改变处理选项
 (?im-nsx)       为表达式后面的部分改变处理选项
 (?(exp)yes|no) 	把exp当作零宽正向先行断言，如果在这个位置能匹配，使用yes作为此组的表达式；否则使用no
 (?(exp)yes) 	同上，只是使用空表达式作为no
 (?(name)yes|no) 如果命名为name的组捕获到了内容，使用yes作为表达式；否则使用no
 (?(name)yes) 	同上，只是使用空表达式作为no
 
 捕获
 (exp)               匹配exp,并捕获文本到自动命名的组里
 (?<name>exp)        匹配exp,并捕获文本到名称为name的组里，也可以写成(?'name'exp)
 (?:exp)             匹配exp,不捕获匹配的文本，也不给此分组分配组号
 零宽断言
 (?=exp)             匹配exp前面的位置
 (?<=exp)            匹配exp后面的位置
 (?!exp)             匹配后面跟的不是exp的位置
 (?<!exp)            匹配前面不是exp的位置
 注释
 (?#comment)         这种类型的分组不对正则表达式的处理产生任何影响，用于提供注释让人阅读
 
 *  表达式：\(?0\d{2}[) -]?\d{8}
 *  这个表达式可以匹配几种格式的电话号码，像(010)88886666，或022-22334455，或02912345678等。
 *  我们对它进行一些分析吧：
 *  首先是一个转义字符\(,它能出现0次或1次(?),然后是一个0，后面跟着2个数字(\d{2})，然后是)或-或空格中的一个，它出现1次或不出现(?)，
 *  最后是8个数字(\d{8})
 */

/**
 是否符合正则表达式

 @param regex 正则表达式
 @return BOOL
 */
- (BOOL)tf_isMatchRegex:(nonnull NSString *)regex;

/**
 是否电话号码

 @return BOOL
 */
- (BOOL)tf_isPhoneNumber;

/**
 是否邮箱地址

 @return BOOL
 */
- (BOOL)tf_isEmailAddress;

/**
 是否身份证号码

 @return BOOL
 */
- (BOOL)tf_isIdentityCardNumber;

/**
 是否银行卡卡号
 
 @return BOOL
 */
- (BOOL)tf_isBankCardNumber;

/**
 是否网络地址

 @return BOOL
 */
- (BOOL)tf_isWebUrl;

/**
 是否中文

 @return BOOL
 */
- (BOOL)tf_isChinese;

///=============================================================================
/// @name 其他方法
///=============================================================================

/**
 判断一个字符串是否是墨迹表情

 @return BOOL
 */
- (BOOL)tf_isEmoji;

/**
 检测字符串中是否包含墨迹表情

 @return BOOL
 */
- (BOOL)tf_isIncludingEmoji;

/**
 移除所有的墨迹表情之后的字符串

 @return NSString
 */
- (nonnull NSString *)tf_removeEmojiString;

/**
 判断字符串中是否包含中文

 @return BOOL
 */
- (BOOL)tf_isContainChinese;

/**
 将字符串通过UTF8编码转换成NSData

 @return NSData
 */
- (nonnull NSData *)tf_utf8Data;

/**
 转换成对应的json对象

 @return id
 */
- (nullable id)tf_jsonObject;

/**
 将字符串转换成对应的NSNumber

 @param formatterStyle 格式化方式
 @return NSNumber
 */
- (nonnull NSNumber *)tf_numberWithFormatter:(NSNumberFormatterStyle)formatterStyle;

@end
