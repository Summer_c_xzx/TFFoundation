//
//  NSNumber+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/14.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 NSNumber的常用类别
 */
@interface NSNumber (TFCore)

/**
 根据格式将NSNumber转换成对应的字符串

 @param formatterStyle NSNumberFormatterStyle，例：数字---@1511511.3
 各种格式对应如下：
 NSNumberFormatterDecimalStyle:1,511,511.3
 NSNumberFormatterCurrencyStyle:￥1,511,511.30
 NSNumberFormatterPercentStyle:151,151,130%
 NSNumberFormatterScientificStyle:1.5115113E6
 NSNumberFormatterSpellOutStyle:一百五十一万一千五百一十一点三
 NSNumberFormatterOrdinalStyle:第151,1511
 NSNumberFormatterCurrencyISOCodeStyle:CNY1,511,511.30
 NSNumberFormatterCurrencyPluralStyle:1,511,511.30人民币
 NSNumberFormatterCurrencyAccountingStyle:￥1,511,511.30
 @return NSString
 */
- (nonnull NSString *)tf_stringFromNumberWithFormatter:(NSNumberFormatterStyle)formatterStyle;

/**
 根据数字字符串转化成对应的NSNumber

 @param numberString 数字字符串
 @param formatterStyle 格式
 @return NSNumber
 */
+ (nonnull NSNumber *)tf_numberWithNumberString:(nonnull NSString *)numberString formatter:(NSNumberFormatterStyle)formatterStyle;

@end
