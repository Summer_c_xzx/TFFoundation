//
//  NSNumber+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/14.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "NSNumber+TFCore.h"

@implementation NSNumber (TFCore)

- (NSString *)tf_stringFromNumberWithFormatter:(NSNumberFormatterStyle)formatterStyle {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = formatterStyle;
    return [numberFormatter stringFromNumber:self];
}

+ (NSNumber *)tf_numberWithNumberString:(NSString *)numberString formatter:(NSNumberFormatterStyle)formatterStyle {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = formatterStyle;
    return [numberFormatter numberFromString:numberString];
}

@end
