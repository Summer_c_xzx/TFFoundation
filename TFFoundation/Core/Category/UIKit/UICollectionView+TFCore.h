//
//  UICollectionView+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/7/10.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 UICollectionView常用类别
 */
@interface UICollectionView (TFCore)

///=============================================================================
/// @name UICollectionView注册和重用
///=============================================================================

/**
 根据cell class 获取对应的重用cell

 @param aClass cell class
 @param indexPath 索引
 @return UICollectionViewCell
 */
- (nonnull __kindof UICollectionViewCell *)tf_dequeueReusableCellWithClass:(nonnull Class)aClass forIndexPath:(nonnull NSIndexPath *)indexPath;

/**
 根据header view class 获取重用的headerView

 @param aClass header view class
 @param indexPath 索引
 @return UICollectionReusableView
 */
- (nonnull __kindof UICollectionReusableView *)tf_dequeueReusableSectionHeaderViewWithClass:(nonnull Class)aClass forIndexPath:(nonnull NSIndexPath *)indexPath;

/**
 根据footer view class 获取重用的footerView
 
 @param aClass footer view class
 @param indexPath 索引
 @return UICollectionReusableView
 */
- (nonnull __kindof UICollectionReusableView *)tf_dequeueReusableSectionFooterViewWithClass:(nonnull Class)aClass forIndexPath:(nonnull NSIndexPath *)indexPath;

/**
 根据cell class 注册cell

 @param aClass cell class
 */
- (void)tf_registerCellWithClass:(nonnull Class)aClass;

/**
 根据cell class 注册Nib类型cell
 
 @param aClass cell class
 */
- (void)tf_registerNibCellWithClass:(nonnull Class)aClass;

/**
 根据header view cell 注册 header view

 @param aClass header view class
 */
- (void)tf_registerSectionHeaderViewWithClass:(nonnull Class)aClass;

/**
 根据header view cell 注册Nib类型 header view
 
 @param aClass header view class
 */
- (void)tf_registerSectionNibHeaderViewWithClass:(nonnull Class)aClass;

/**
 根据footer view cell 注册 footer view
 
 @param aClass footer view class
 */
- (void)tf_registerSectionFooterViewWithClass:(nonnull Class)aClass;

/**
 根据footer view cell 注册Nib类型 footer view
 
 @param aClass footer view class
 */
- (void)tf_registerSectionNibFooterViewWithClass:(nonnull Class)aClass;





@end
