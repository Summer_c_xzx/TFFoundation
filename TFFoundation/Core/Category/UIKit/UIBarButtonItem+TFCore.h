//
//  UIBarButtonItem+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/28.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFViewActionTarget.h"

/**
 UIBarButtonItem常用类别
 */
@interface UIBarButtonItem (TFCore)

/**
 统一的事件处理对象
 */
@property (nonatomic, copy, nullable) TFViewActionTarget *tf_actionTarget;

/**
 创建标题UIBarButtonItem

 @param title 标题
 @param actionHandler 事件处理
 @return UIBarButtonItem
 */
+ (nonnull instancetype)tf_barButtonItemWithTitle:(nonnull NSString *)title actionHandler:(nullable TFViewActionHandlerBlock)actionHandler;

/**
 创建图片UIBarButtonItem

 @param imageName 图片名字
 @param actionHandler 事件处理
 @return UIBarButtonItem
 */
+ (nonnull instancetype)tf_barButtonItemWithImageName:(nonnull NSString *)imageName actionHandler:(nullable TFViewActionHandlerBlock)actionHandler;

/**
 创建选择和未选择的图片UIBarButtonItem

 @param imageName 图片名字
 @param selectedImageName 选择的图片名字
 @param actionHandler 事件处理
 @return UIBarButtonItem
 */
+ (nonnull instancetype)tf_barButtonItemWithImageName:(nonnull NSString *)imageName selectedImageName:(nonnull NSString *)selectedImageName actionHandler:(nullable TFViewActionHandlerBlock)actionHandler;

@end
