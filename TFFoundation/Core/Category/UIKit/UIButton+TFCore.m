//
//  UIButton+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/4/11.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "UIButton+TFCore.h"
#import "UIView+TFCore.h"

NSString * const kTFUIButtonStateAttributeKey = @"TFUIButtonStateAttribute";

NSString * const kTFUIButtonTitleAttributeKey = @"TFUIButtonTitleAttribute";

NSString * const kTFUIButtonTitleColorAttributeKey = @"TFUIButtonTitleColorAttribute";

NSString * const kTFUIButtonAttributedTitleAttributeKey = @"TFUIButtonAttributedTitleAttribute";

NSString * const kTFUIButtonImageAttributeKey = @"TFUIButtonImageAttribute";

NSString * const kTFUIButtonBackgroundImageAttributeKey = @"TFUIButtonBackgroundImageAttribute";


@implementation UIButton (TFCore)

+ (instancetype)tf_buttonWithAttributesArray:(NSArray<NSDictionary<kTFUIButtonAttributeKey,id> *> *)attributesArray touchUpInsideActionHandler:(nullable TFViewActionHandlerBlock)touchUpInsideActionHandler{
    UIButton *button = [self buttonWithType:UIButtonTypeCustom];
    [button tf_setButtonWithAttributesArray:attributesArray];
    TFViewActionTarget *target = [[TFViewActionTarget alloc] initWithActionHandlerBlock:touchUpInsideActionHandler];
    button.tf_actionTarget = target;
    [button addTarget:target action:@selector(actionHandler:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

- (void)tf_setButtonWithAttributesArray:(NSArray<NSDictionary<kTFUIButtonAttributeKey,id> *> *)attributesArray {
    for (NSDictionary<kTFUIButtonAttributeKey,id> *dic in attributesArray) {
        UIControlState state = UIControlStateNormal;
        if ([dic objectForKey:kTFUIButtonStateAttributeKey]) {
            state = [[dic objectForKey:kTFUIButtonStateAttributeKey] integerValue];
        }
        if ([dic objectForKey:kTFUIButtonTitleAttributeKey]) {
            [self setTitle:[dic objectForKey:kTFUIButtonTitleAttributeKey] forState:state];
        }
        if ([dic objectForKey:kTFUIButtonTitleColorAttributeKey]) {
            [self setTitleColor:[dic objectForKey:kTFUIButtonTitleColorAttributeKey] forState:state];
        }
        if ([dic objectForKey:kTFUIButtonImageAttributeKey]) {
            [self setImage:[dic objectForKey:kTFUIButtonImageAttributeKey] forState:state];
        }
        if ([dic objectForKey:kTFUIButtonAttributedTitleAttributeKey]) {
            [self setAttributedTitle:[dic objectForKey:kTFUIButtonAttributedTitleAttributeKey] forState:state];
        }
        if ([dic objectForKey:kTFUIButtonBackgroundImageAttributeKey]) {
            [self setBackgroundImage:[dic objectForKey:kTFUIButtonBackgroundImageAttributeKey] forState:state];
        }
    }
}

+ (instancetype)tf_buttonWithTitle:(NSString *)title color:(UIColor *)titleColor font:(UIFont *)font touchUpInsideActionHandler:(TFViewActionHandlerBlock)touchUpInsideActionHandler {
    UIButton *button = [UIButton tf_buttonWithAttributesArray:@[@{kTFUIButtonStateAttributeKey:@(UIControlStateNormal),kTFUIButtonTitleAttributeKey:title,kTFUIButtonTitleColorAttributeKey:titleColor}] touchUpInsideActionHandler:touchUpInsideActionHandler];
    button.titleLabel.font = font;
    return button;
}

+ (instancetype)tf_buttonWithImage:(UIImage *)image touchUpInsideActionHandler:(TFViewActionHandlerBlock)touchUpInsideActionHandler {
    UIButton *button = [UIButton tf_buttonWithAttributesArray:@[@{kTFUIButtonStateAttributeKey:@(UIControlStateNormal),kTFUIButtonImageAttributeKey:image}] touchUpInsideActionHandler:touchUpInsideActionHandler];
    return button;
}

+ (instancetype)tf_buttonWithImage:(UIImage *)image selectedImage:(nonnull UIImage *)selectedImage touchUpInsideActionHandler:(nullable TFViewActionHandlerBlock)touchUpInsideActionHandler {
    UIButton *button = [UIButton tf_buttonWithAttributesArray:@[
  @{kTFUIButtonStateAttributeKey:@(UIControlStateNormal),kTFUIButtonImageAttributeKey:image},
  @{kTFUIButtonStateAttributeKey:@(UIControlStateSelected),kTFUIButtonImageAttributeKey:selectedImage}
  ] touchUpInsideActionHandler:touchUpInsideActionHandler];
    return button;
}

+ (instancetype)tf_buttonWithFont:(UIFont *)font title:(NSString *)title titleColor:(UIColor *)titleColor selectedTitle:(NSString *)selectedTitle selectedTitleColor:(UIColor *)selectedTitleColor touchUpInsideActionHandler:(TFViewActionHandlerBlock)touchUpInsideActionHandler {
    UIButton *button = [UIButton tf_buttonWithAttributesArray:@[
                                                                @{kTFUIButtonStateAttributeKey:@(UIControlStateNormal),kTFUIButtonTitleAttributeKey:title,kTFUIButtonTitleColorAttributeKey:titleColor},
                                                                @{kTFUIButtonStateAttributeKey:@(UIControlStateSelected),kTFUIButtonTitleAttributeKey:selectedTitle,kTFUIButtonTitleColorAttributeKey:selectedTitleColor},
                                                                ] touchUpInsideActionHandler:touchUpInsideActionHandler];
    button.titleLabel.font = font;
    return button;
}

@end
