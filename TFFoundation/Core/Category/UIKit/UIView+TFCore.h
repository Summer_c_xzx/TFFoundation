//
//  UIView+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/21.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFViewActionTarget.h"

/**
 UIView常用类别
 */
@interface UIView (TFCore)

///=============================================================================
/// @name frame layout.
///=============================================================================

@property (nonatomic,assign) CGFloat tf_width;///<宽度
@property (nonatomic,assign) CGFloat tf_height;///<高度
@property (nonatomic,assign) CGSize tf_size;///<大小
@property (nonatomic,assign) CGFloat tf_left;///<左边
@property (nonatomic,assign) CGFloat tf_top;///<上边
@property (nonatomic,assign) CGFloat tf_right;///<右边
@property (nonatomic,assign) CGFloat tf_bottom;///<下边
@property (nonatomic,assign) CGPoint tf_origin;///<起始点
@property (nonatomic, assign) CGPoint tf_center;///<中心点
@property (nonatomic,assign) CGPoint tf_rightTop;///<右上角的点
@property (nonatomic,assign) CGPoint tf_leftBottom;///<左下角的点
@property (nonatomic,assign) CGPoint tf_rightBottom;///<右下角的点
@property (nonatomic,assign) CGFloat tf_centerX;///<水平方向中心位置
@property (nonatomic,assign) CGFloat tf_centerY;///<竖直方向中心位置


///=============================================================================
/// @name 常用方法以及属性.
///=============================================================================

@property (nullable, nonatomic, readonly) UIViewController *tf_viewController;///<获取对应的视图控制器
@property (nonatomic, assign) CGFloat tf_cornerRadius;///<圆角
@property (nonatomic, assign) CGFloat tf_borderWidth;///<边框宽度
@property (nonatomic, strong, nullable) UIColor *tf_borderColor;///<边框颜色

/**
 移除所有的子view
 */
- (void)tf_removeAllSubViews;

/**
 屏幕截图

 @return UIImage
 */
- (nonnull UIImage *)tf_snapshotImage;

/**
 获取跟随屏幕改变而改变的截图

 @param afterUpdates 是否跟随改变
 @return UIImage
 */
- (nullable UIImage *)tf_snapshotImageAfterScreenUpdates:(BOOL)afterUpdates;

/**
 根据背景色创建view

 @param backgroudColor 背景色
 @return UIView
 */
+ (nonnull instancetype)tf_viewWithBackgroudColor:(nonnull UIColor *)backgroudColor;

@property (nonatomic, strong, nullable) TFViewActionTarget *tf_actionTarget;///<统一的事件处理对象

/**
 添加tap手势，并处理事件

 @param actionHandler 事件处理
 */
- (void)tf_addTapGestureWithActionHandler:(nullable TFViewActionHandlerBlock)actionHandler;

/**
 *  扩大控件点击事件 eg:UIEdgeInsetMake(-10.f,-10.f,-10.f,-10.f)
 */
@property (nonatomic, assign) UIEdgeInsets tf_touchExtendInset;

/**
 *  设置部分圆角(绝对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 */
- (void)tf_addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii;

/**
 *  设置部分圆角(相对布局)
 *
 *  @param corners 需要设置为圆角的角 UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerAllCorners
 *  @param radii   需要设置的圆角大小 例如 CGSizeMake(20.0f, 20.0f)
 *  @param rect    需要设置的圆角view的rect
 */
- (void)tf_addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii
                 viewRect:(CGRect)rect;



@end
