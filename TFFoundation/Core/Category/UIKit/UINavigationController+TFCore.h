//
//  UINavigationController+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/28.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>



#define TFPopToVC(NAME,ANIMATED) ((NO && (^void(NAME* b){})),[[[UIApplication sharedApplication].delegate.window.rootViewController tf_visibleViewController].navigationController tf_popToViewControllerWithClass:NSClassFromString(@#NAME) animated:ANIMATED])

#define TFPopVC(ANIMATED) ([[[UIApplication sharedApplication].delegate.window.rootViewController tf_visibleViewController].navigationController popViewControllerAnimated:ANIMATED])


/**
 UINavigationController常用类别
 */
@interface UINavigationController (TFCore)

/**
 根据class跳转制定的ViewController
 
 @param popClass viewController对应的class.
 @param animated 是否动画
 */
- (void)tf_popToViewControllerWithClass:(nonnull Class)popClass animated:(BOOL)animated;

/**
 根据pop到前几个vc，来pop

 @param count 数量
 @param animated 是否动画
 */
- (void)tf_popToViewControllerWithCount:(NSUInteger)count animated:(BOOL)animated;

/**
 根据class获取对应的ViewControllers数组中的VC

 @param viewControllerClass viewController对应的class.
 @return UIViewController
 */
- (nullable __kindof UIViewController *)tf_ViewControllerForClass:(nonnull Class)viewControllerClass;

@end
