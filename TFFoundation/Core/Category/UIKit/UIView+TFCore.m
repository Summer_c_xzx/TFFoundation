//
//  UIView+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/21.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "UIView+TFCore.h"
#import "NSObject+TFCore.h"
#import "TFDefine.h"
#import "UIViewController+TFCore.h"

@implementation UIView (TFCore)

+ (void)load {
    TFSwizzleMethod(self, @selector(pointInside:withEvent:), @selector(tf_PointInside:withEvent:));
}

- (BOOL)tf_PointInside:(CGPoint)point withEvent:(UIEvent *)event {
    if (UIEdgeInsetsEqualToEdgeInsets(self.tf_touchExtendInset, UIEdgeInsetsZero) || self.hidden ||
        ([self isKindOfClass:UIControl.class] && !((UIControl *)self).enabled)) {
        return [self tf_PointInside:point withEvent:event];
    }
    CGRect hitFrame = UIEdgeInsetsInsetRect(self.bounds, self.tf_touchExtendInset);
    hitFrame.size.width = MAX(hitFrame.size.width, 0);
    hitFrame.size.height = MAX(hitFrame.size.height, 0);
    return CGRectContainsPoint(hitFrame, point);
}

- (void)setTf_touchExtendInset:(UIEdgeInsets)tf_touchExtendInset {
    [self tf_setAssociateValue:[NSValue valueWithUIEdgeInsets:tf_touchExtendInset] withkey:@selector(tf_touchExtendInset) assoicationPolicy:TFAssociationPolicy_retain_nonatomic];
}

- (UIEdgeInsets)tf_touchExtendInset {
    return [[self tf_getAssociateValueForKey:@selector(tf_touchExtendInset)] UIEdgeInsetsValue];
}

#pragma mark - frame layout.

- (CGFloat)tf_width {
    return CGRectGetWidth(self.frame);
}

- (void)setTf_width:(CGFloat)tf_width {
    
    CGRect frame = self.frame;
    frame.size.width = tf_width;
    self.frame = frame;
}

- (CGFloat)tf_height {
    return CGRectGetHeight(self.frame);
}

- (void)setTf_height:(CGFloat)tf_height {
    CGRect frame = self.frame;
    frame.size.height = tf_height;
    self.frame = frame;
}

-(CGSize)tf_size {
    return self.frame.size;
}

-(void)setTf_size:(CGSize)tf_size {
    CGRect frame = self.frame;
    frame.size = tf_size;
    self.frame = frame;
}

- (CGFloat)tf_left {
    return CGRectGetMinX(self.frame);
}

- (void)setTf_left:(CGFloat)tf_left {
    CGRect frame = self.frame;
    frame.origin.x = tf_left;
    self.frame = frame;
}

- (CGFloat)tf_top {
    return CGRectGetMinY(self.frame);
}

- (void)setTf_top:(CGFloat)tf_top {
    CGRect frame = self.frame;
    frame.origin.y = tf_top;
    self.frame = frame;
}

- (CGFloat)tf_right {
    return CGRectGetMaxX(self.frame);
}

- (void)setTf_right:(CGFloat)tf_right {
    CGRect frame = self.frame;
    frame.origin.x = tf_right-CGRectGetWidth(frame);
    self.frame = frame;
}

- (CGFloat)tf_bottom {
    return CGRectGetMaxY(self.frame);
}

- (void)setTf_bottom:(CGFloat)tf_bottom {
    CGRect frame = self.frame;
    frame.origin.y = tf_bottom - CGRectGetHeight(frame);
    self.frame = frame;
}

- (CGPoint)tf_origin {
    return self.frame.origin;
}

- (void)setTf_origin:(CGPoint)tf_origin {
    CGRect frame = self.frame;
    frame.origin = tf_origin;
    self.frame = frame;
}

- (CGPoint)tf_center {
    CGRect frame = self.frame;
    return CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
}

- (void)setTf_center:(CGPoint)tf_center {
    CGRect frame = self.frame;
    frame.origin.x = tf_center.x - CGRectGetWidth(frame)/2.0;
    frame.origin.y = tf_center.y - CGRectGetHeight(frame)/2.0;
    self.frame = frame;
}

- (CGPoint)tf_rightTop {
    CGRect frame = self.frame;
    return CGPointMake(CGRectGetMaxX(frame), CGRectGetMinY(frame));
}

- (void)setTf_rightTop:(CGPoint)tf_rightTop {
    CGRect frame = self.frame;
    frame.origin.x = tf_rightTop.x-CGRectGetWidth(frame);
    frame.origin.y = tf_rightTop.y;
    self.frame = frame;
}

- (CGPoint)tf_leftBottom {
    CGRect frame = self.frame;
    return CGPointMake(CGRectGetMinX(frame),CGRectGetMaxY(frame));
}

- (void)setTf_leftBottom:(CGPoint)tf_leftBottom {
    CGRect frame = self.frame;
    frame.origin.x = tf_leftBottom.x;
    frame.origin.y = tf_leftBottom.y - CGRectGetHeight(frame);
    self.frame = frame;
}

- (CGPoint)tf_rightBottom {
    CGRect frame = self.frame;
    return CGPointMake(CGRectGetMaxX(frame), CGRectGetMaxY(frame));
}

- (void)setTf_rightBottom:(CGPoint)tf_rightBottom{
    CGRect frame = self.frame;
    frame.origin.x = tf_rightBottom.x - CGRectGetWidth(frame);
    frame.origin.y = tf_rightBottom.y - CGRectGetHeight(frame);
    self.frame = frame;
}

- (CGFloat)tf_centerX {
    return CGRectGetMidX(self.frame);
}

- (void)setTf_centerX:(CGFloat)tf_centerX {
    CGRect frame = self.frame;
    frame.origin.x = tf_centerX - CGRectGetWidth(frame)/2.0;
    self.frame = frame;
}

- (CGFloat)tf_centerY {
    return CGRectGetMidY(self.frame);
}

- (void)setTf_centerY:(CGFloat)tf_centerY {
    CGRect frame = self.frame;
    frame.origin.y = tf_centerY - CGRectGetHeight(frame)/2.0;
    self.frame = frame;
}

#pragma mark - 其他

- (UIViewController *)tf_viewController {
    UIResponder *responder = self.nextResponder;
    do {
        if ([responder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)responder;
        }
        responder = responder.nextResponder;
    } while (responder);
    return nil;
}

- (void)setTf_cornerRadius:(CGFloat)tf_cornerRadius {
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = tf_cornerRadius;
}

- (CGFloat)tf_cornerRadius {
    return self.layer.cornerRadius;
}

- (void)setTf_borderWidth:(CGFloat)tf_borderWidth {
    self.layer.borderWidth = tf_borderWidth;
}

- (CGFloat)tf_borderWidth {
    return self.layer.borderWidth;
}

- (void)setTf_borderColor:(UIColor *)tf_borderColor {
    self.layer.borderColor = tf_borderColor.CGColor;
}

- (UIColor *)tf_borderColor {
    return [UIColor colorWithCGColor:self.layer.borderColor];
}

- (void)tf_removeAllSubViews {
    for (UIView *subView in self.subviews) {
        [subView removeFromSuperview];
    }
}

- (UIImage *)tf_snapshotImage {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [UIScreen mainScreen].scale);
    if( [self respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)])
    {
        [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:NO];
    }
    else
    {
        [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    }
    
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return screenshot;
}

- (UIImage *)tf_snapshotImageAfterScreenUpdates:(BOOL)afterUpdates {
    if (![self respondsToSelector:@selector(drawViewHierarchyInRect:afterScreenUpdates:)]) {
        return [self tf_snapshotImage];
    }
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0);
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:afterUpdates];
    UIImage *snap = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snap;
}

+ (instancetype)tf_viewWithBackgroudColor:(UIColor *)backgroudColor {
    UIView *view = [[self alloc] init];
    view.backgroundColor = backgroudColor;
    return view;
}

- (void)setTf_actionTarget:(TFViewActionTarget *)tf_actionTarget {
    [self tf_setAssociateValue:tf_actionTarget withkey:@selector(tf_actionTarget) assoicationPolicy:TFAssociationPolicy_retain_nonatomic];
}

- (TFViewActionTarget *)tf_actionTarget {
    return [self tf_getAssociateValueForKey:_cmd];
}

- (void)tf_addTapGestureWithActionHandler:(TFViewActionHandlerBlock)actionHandler {
    self.userInteractionEnabled = YES;
    TFViewActionTarget *target = [[TFViewActionTarget alloc] initWithActionHandlerBlock:actionHandler];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:target action:@selector(actionHandler:)];
    [self addGestureRecognizer:tap];
    self.tf_actionTarget = target;
}


- (void)tf_addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii {
    
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:corners cornerRadii:radii];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];    
    self.layer.mask = shape;
}

- (void)tf_addRoundedCorners:(UIRectCorner)corners
                withRadii:(CGSize)radii
                 viewRect:(CGRect)rect {
    
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:corners cornerRadii:radii];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    
    self.layer.mask = shape;
}

@end
