//
//  TFViewActionTarget.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/4/12.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 事件处理block

 @param sender 传递者
 */
typedef void(^TFViewActionHandlerBlock)(id __nullable sender);

/**
 View的事件中转者对象
 */
@interface TFViewActionTarget : NSObject

/**
 通过事件处理block创建中转者

 @param actionHandlerBlock 事件处理block
 @return View的事件中转者对象
 */
- (nonnull instancetype)initWithActionHandlerBlock:(nullable TFViewActionHandlerBlock)actionHandlerBlock;

/**
 动作处理

 @param sender 传递者
 */
- (void)actionHandler:(nullable id)sender;

@end
