//
//  TFViewActionTarget.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/4/12.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "TFViewActionTarget.h"

@interface TFViewActionTarget ()

@property (nonatomic, copy, nullable) TFViewActionHandlerBlock actionHandlerBlock;

@end

@implementation TFViewActionTarget

- (instancetype)initWithActionHandlerBlock:(TFViewActionHandlerBlock)actionHandlerBlock {
    self = [super init];
    if (self) {
        self.actionHandlerBlock = actionHandlerBlock;
    }
    return self;
}

- (void)actionHandler:(id)sender {
    if (_actionHandlerBlock) {
        _actionHandlerBlock(sender);
    }
}

@end
