//
//  UIBarButtonItem+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/28.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "UIBarButtonItem+TFCore.h"
#import "NSObject+TFCore.h"
#import "UIButton+TFCore.h"

@implementation UIBarButtonItem (TFCore)

- (void)setTf_actionTarget:(TFViewActionTarget *)tf_actionTarget {
    [self tf_setAssociateValue:tf_actionTarget withkey:@selector(tf_actionTarget) assoicationPolicy:TFAssociationPolicy_retain_nonatomic];
}

- (TFViewActionTarget *)tf_actionTarget {
    return [self tf_getAssociateValueForKey:_cmd];
}

+ (instancetype)tf_barButtonItemWithTitle:(NSString *)title actionHandler:(TFViewActionHandlerBlock)actionHandler {
    TFViewActionTarget *target = [[TFViewActionTarget alloc] initWithActionHandlerBlock:actionHandler];
    UIBarButtonItem *item = [[self alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:target action:@selector(actionHandler:)];
    item.tf_actionTarget = target;
    return item;
}

+ (instancetype)tf_barButtonItemWithImageName:(NSString *)imageName actionHandler:(TFViewActionHandlerBlock)actionHandler {
    TFViewActionTarget *target = [[TFViewActionTarget alloc] initWithActionHandlerBlock:actionHandler];
    UIBarButtonItem *item = [[self alloc] initWithImage:[[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:target action:@selector(actionHandler:)];
    item.tf_actionTarget = target;
    return item;
}

+ (instancetype)tf_barButtonItemWithImageName:(NSString *)imageName selectedImageName:(nonnull NSString *)selectedImageName actionHandler:(nullable TFViewActionHandlerBlock)actionHandler {
    UIButton *button = [UIButton tf_buttonWithImage:[UIImage imageNamed:imageName] selectedImage:[UIImage imageNamed:selectedImageName] touchUpInsideActionHandler:actionHandler];
    [button sizeToFit];
    UIBarButtonItem *item = [[self alloc] initWithCustomView:button];
    return item;
}

@end
