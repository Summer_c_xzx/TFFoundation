//
//  UIViewController+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/9.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NSString *kTFOpenURLParamsKey NS_STRING_ENUM;

///<通过params参数设置跳转方式的key，默认TFOpenURLJumpStylePush
_Nonnull FOUNDATION_EXPORT kTFOpenURLParamsKey const kTFOpenURLJumpStyleKey;
///<通过params参数设置storyboard name key，默认为nil
_Nonnull FOUNDATION_EXPORT kTFOpenURLParamsKey const kTFOpenURLStoryboardNameKey;
///<通过params参数设置跳转的类型key 默认TFOpenURLTypeViewControllerURL
_Nonnull FOUNDATION_EXPORT kTFOpenURLParamsKey const kTFOpenURLTypeKey;
///<通过params自定义viewController的初始化
_Nonnull FOUNDATION_EXPORT kTFOpenURLParamsKey const kTFOpenURLViewControllerInitBlockKey;
///<通过params参数设置push方式的加入的vc的索引，默认为0，若为-1则去掉UINavigationController的ViewControllers中的最后一个元素，若为-2则去掉最后两个元素，以此类推
_Nonnull FOUNDATION_EXPORT kTFOpenURLParamsKey const kTFOpenURLJumpStylePushIndexKey;
///<通过params参数设置跳转的时候是否使用动画
_Nonnull FOUNDATION_EXPORT kTFOpenURLParamsKey const kTFOpenURLJumpAnimatedKey;
///<通过params参数设置present方式时跳转结束的block
_Nonnull FOUNDATION_EXPORT kTFOpenURLParamsKey const kTFOpenURLPresentCompletionBlockKey;
///<通过params参数设置跳转vc的title
_Nonnull FOUNDATION_EXPORT kTFOpenURLParamsKey const kTFOpenURLViewControllerTitleKey;
///<试图加载时的通知
extern NSString * _Nonnull const kTFViewControllerViewDidLoadNotification;
///<试图将要出现时的通知
extern NSString * _Nonnull const kTFViewControllerViewWillAppearNotification;
///<试图将要消失时的通知
extern NSString * _Nonnull const kTFViewControllerViewWillDisappearNotification;
///<试图出现的时候
extern NSString * _Nonnull const kTFViewControllerViewDidAppearNotification;
///<试图消失的时候
extern NSString * _Nonnull const kTFViewControllerViewDidDisappearNotification;

/**
 跳转方式
 */
typedef NS_ENUM(NSUInteger, TFOpenURLJumpStyle) {
    TFOpenURLJumpStylePush,///<push方式
    TFOpenURLJumpStylePresent,///<present方式
};

/**
 跳转的类型
 */
typedef NS_ENUM(NSUInteger, TFOpenURLType) {
    TFOpenURLTypeViewControllerURL,///<viewcontroller地址
    TFOpenURLTypeWebURL,///<网络地址
    TFOpenURLTypeAppURL,///<第三方app
};

/**
 自定义跳转vc的初始化方法的block

 @return UIViewController
 */
typedef UIViewController * _Nonnull (^TFOpenURLViewControllerInitBlock)(void);


/**
 建议使用的跳转vc的方法

 @param NAME vc的class
 @param params 参数
 @return UIViewController
 */

#define TFOpenVC(NAME, params) ((NAME *)TFOpenURL(@#NAME, params))

/**
 通过URL跳转某个VC的快捷方法，默认URL为ViewController的ClassName

 @param url 跳转的url，可以是vc或者网络地址
 @param params 附带的参数信息，参数中可通过设置上述几个key值，实现自定义跳转
 @warning 默认跳转方式为push
 @return UIViewController 打开的vc
 */
UIViewController * _Nonnull TFOpenURL(NSString * _Nonnull url,NSDictionary * __nullable params);

/**
 通过ViewController的class name 进行跳转 -- 建议使用的方法

 @param vcClass ViewController的class name
 @param params 参数
 @return UIViewController 打开的vc
 */
UIViewController * _Nonnull TFOpenVCClass(_Nonnull Class vcClass,NSDictionary * __nullable params);

/**
 UIViewController 常用类别
 */
@interface UIViewController (TFCore)

/**
 根据storyboard的name和id初始化vc

 @param storyboardName storyboard name
 @param storyboardId storyboard id
 @return UIViewController
 */
+ (nullable __kindof UIViewController *)tf_viewControllerWithStoryboardName:(nonnull NSString *)storyboardName storyboardId:(nonnull NSString *)storyboardId;

/**
 根据storyboard id初始化vc，默认从Main中获取。

 @param storyboardId storyboard id
 @return UIViewController
 */
+ (nullable __kindof UIViewController *)tf_viewControllerWithMainStoryboardId:(nonnull NSString *)storyboardId;

/**
 获取storyboard中的vc，默认从Main中获取，且storyboardId为当前类的名字

 @return UIViewController
 */
+ (nullable __kindof UIViewController *)tf_viewControllerWithMainStoryboardAndClassNameAsStoryBoardId;

@property (nonatomic, strong, nullable) NSMutableDictionary *params;///<附带参数，若通过TFOpenURL方式跳转会自动将params赋值到params

/**
 获取UINavigationViewController下或者present下的可视的vc

 @return UIViewController
 */
- (nonnull UIViewController *)tf_visibleViewController;

/**
 设置导航栏完全透明
 */
- (void)tf_setNavigationBarFullTranslucent;

/**
 设置导航栏到正常状态
 */
- (void)tf_recoverNavigationBarToNormal;

/**
 是否返回，当点击导航栏按钮时调用

 @return 是否返回
 */
- (BOOL)tf_shouldPopBack;

@end
