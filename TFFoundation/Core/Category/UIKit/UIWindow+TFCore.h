//
//  UIWindow+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/9.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 UIWindow 的常用类别
 */
@interface UIWindow (TFCore)

/**
 获取当前显示的vc

 @return UIViewController
 */
- (nonnull UIViewController *)tf_visibleViewController;

/**
 设置rootVC时是否使用动画形式

 @param rootViewController 根vc
 @param animated 是否动画
 @param completion 动画结束的block，只有动画结束时才会调用
 */
- (void)tf_setRootViewController:(nonnull UIViewController *)rootViewController withAnimated:(BOOL)animated completion:(void (^ __nullable)(BOOL finished))completion;

/**
 设置rootVC动画方式和时长

 @param rootViewController 根vc
 @param duration 动画时长
 @param options 动画种类
 @param completion 动画结束的block，只有动画结束时才会调用
 */
- (void)tf_setRootViewController:(nonnull UIViewController *)rootViewController withDuration:(NSTimeInterval)duration options:(UIViewAnimationOptions)options completion:(void (^ __nullable)(BOOL finished))completion;

@end
