//
//  UIDevice+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/28.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 UIDevice常用类别
 */
@interface UIDevice (TFCore)

/**
 获取系统版本

 @return double
 */
+ (double)tf_systemVersion;

/**
 获取设备id

 @return NSString
 */
+ (nonnull NSString *)tf_deviceID;

/**
 是否ipad
 */
@property (nonatomic, readonly) BOOL tf_isPad;

/**
 设备名 "iPhone6,1" "iPad4,6"
 */
@property (nonatomic, readonly, nonnull) NSString *tf_machineModel;

/**
 设备详细名字 "iPhone 5s" "iPad mini 2"
 */
@property (nonatomic, readonly, nonnull) NSString *tf_machineModelName;


@end
