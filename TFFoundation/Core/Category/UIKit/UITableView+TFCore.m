//
//  UITableView+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/7/10.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "UITableView+TFCore.h"

@implementation UITableView (TFCore)

- (UITableViewCell *)tf_dequeueReusableCellWithClass:(Class)aClass {
    return [self dequeueReusableCellWithIdentifier:NSStringFromClass(aClass)];
}

- (UITableViewHeaderFooterView *)tf_dequeueReusableHeaderFooterViewWithClass:(Class)aClass {
    return [self dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass(aClass)];
}

- (void)tf_registerCellWithClass:(Class)aClass {
    [self registerClass:aClass forCellReuseIdentifier:NSStringFromClass(aClass)];
}

- (void)tf_registerNibCellWithClass:(Class)aClass {
    [self registerNib:[UINib nibWithNibName:NSStringFromClass(aClass) bundle:[NSBundle mainBundle]] forCellReuseIdentifier:NSStringFromClass(aClass)];
}

- (void)tf_registerHeaderFooterViewWithClass:(Class)aClass {
    [self registerClass:aClass forHeaderFooterViewReuseIdentifier:NSStringFromClass(aClass)];
}

- (void)tf_registerNibHeaderFooterViewWithClass:(Class)aClass {
    [self registerNib:[UINib nibWithNibName:NSStringFromClass(aClass) bundle:[NSBundle mainBundle]] forHeaderFooterViewReuseIdentifier:NSStringFromClass(aClass)];
}

@end
