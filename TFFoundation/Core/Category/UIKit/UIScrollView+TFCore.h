//
//  UIScrollView+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/8/7.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 特效头部变化处理block
 
 @param parallaxHeaderView 投效头部
 @param originalHeight 原始高度
 @param headerRatio 高度比例
 */
typedef void(^TFParallaxHeaderViewHeightChangeBlock)(__kindof UIView *parallaxHeaderView,CGFloat originalHeight ,float headerRatio);

/**
 UIScrollView常用类别
 */
@interface UIScrollView (TFCore)

@property (nonatomic, strong, readonly) __kindof UIView *tf_parallaxHeaderView;///<特效头部view
@property (nonatomic, assign, readonly) CGFloat tf_originParallaxHeaderHeight;///<原始的高度
@property (nonatomic, assign, readonly) CGFloat tf_minParallaxHeaderHeight;///<最小高度

/**
 特效头部变化block
 */
@property (nonatomic, copy) TFParallaxHeaderViewHeightChangeBlock tf_parallaxHeaderViewHeightChangeBlock;

/**
 添加自定义的特效头部
 
 @param headerView 自定义头部view.
 */
- (void)tf_addParallaxHeaderView:(__kindof UIView *)headerView;

/**
 添加自定义的特效头部，并设置最小头部高度
 
 @param headerView 自定义头部view.
 @param minHeaderHeight 头部view最小的高度.
 */
- (void)tf_addParallaxHeaderView:(__kindof UIView *)headerView
              minHeaderHeight:(CGFloat)minHeaderHeight;


/**
 添加根据图片名字特效头部
 
 @param image 显示的图片
 */
- (void)tf_addParallaxHeaderWithImage:(UIImage *)image;


/**
 添加根据图片名字特效头部，并设置最小头部高度
 
 @param image 显示的图片.
 @param minHeaderHeight 最小高度.
 */
- (void)tf_addParallaxHeaderWithImage:(UIImage *)image minHeaderHeight:(CGFloat)minHeaderHeight;



@end
