//
//  UIApplication+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/27.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 UIApplication常用类别
 */
@interface UIApplication (TFCore)

@property (nonatomic, strong, readonly, nonnull) UIWindow *tf_currentWindow;///<获取当前的window
@property (nonnull, nonatomic, readonly) NSString *tf_appBundleName;///<bundle name.
@property (nonnull, nonatomic, readonly) NSString *tf_appBundleDisplayName;///<bundle display name,即app的名字
@property (nonnull, nonatomic, readonly) NSString *tf_appBundleID;///<唯一标识
@property (nonnull, nonatomic, readonly) NSString *tf_appVersion;///<版本号，例：1.0.0
@property (nonnull, nonatomic, readonly) NSString *tf_appBuildVersion;///<构建版本号，例：2016100901001
@property (nonatomic, assign, readonly) CGFloat tf_statusBarHeight;///<状态栏高度
@property (nonatomic, readonly) int64_t tf_memoryUsage;///<内存占用率
@property (nonatomic, readonly) float tf_cpuUsage;///<cpu占用率

/**
 打电话

 @param phone 电话号码
 */
- (void)tf_callPhone:(nonnull NSString *)phone;

/**
 跳转app评论页

 @param appId APP中的id
 */
- (void)tf_openAppReviewPageWithAppId:(nonnull NSString *)appId;

@end
