//
//  UITableView+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/7/10.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 UITableview常用类别
 */
@interface UITableView (TFCore)

/**
 根据cell class获取重用cell

 @param aClass cell class
 @return UITableViewCell
 */
- (nullable __kindof UITableViewCell *)tf_dequeueReusableCellWithClass:(nonnull Class)aClass;

/**
 根据header or footer view class获取重用的header or footer view

 @param aClass header or footer view class
 @return UITableViewHeaderFooterView
 */
- (nullable __kindof UITableViewHeaderFooterView *)tf_dequeueReusableHeaderFooterViewWithClass:(nonnull Class)aClass;

/**
 根据cell class 注册cell

 @param aClass cell class
 */
- (void)tf_registerCellWithClass:(nonnull Class)aClass;

/**
 根据cell class 注册nib类型的cell

 @param aClass cell class
 */
- (void)tf_registerNibCellWithClass:(nonnull Class)aClass;

/**
 根据 header or footer view class 注册view

 @param aClass header or footer view class.
 */
- (void)tf_registerHeaderFooterViewWithClass:(nonnull Class)aClass;

/**
 根据 header or footer view class 注册nib类型的view
 
 @param aClass header or footer view class.
 */
- (void)tf_registerNibHeaderFooterViewWithClass:(nonnull Class)aClass;

@end
