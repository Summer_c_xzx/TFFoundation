//
//  UIAlertController+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/14.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "UIAlertController+TFCore.h"

NSString * const kUIAlertControllerActionEnableKey = @"UIAlertControllerActionEnable";

NSString * const kUIAlertControllerActionStyleKey = @"UIAlertControllerActionStyle";

NSString * const kUIAlertControllerActionTitleKey = @"UIAlertControllerActionTitle";

@implementation UIAlertController (TFCore)

+ (instancetype)tf_showInViewController:(UIViewController *)viewController withStyle:(UIAlertControllerStyle)style title:(NSString *)title message:(NSString *)message actionsArray:(NSArray<NSDictionary<kUIAlertControllerActionKey,id> *> *)actionsArray actionsHandlerBlock:(TFAlertControllerActionsHandlerBlock)actionsHandlerBlock {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    typeof(alertVC) __weak weakAlertVC = alertVC;
    // add actions
    for (NSDictionary *actionDic in actionsArray) {
        
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:actionDic[kUIAlertControllerActionTitleKey] style:[actionDic[kUIAlertControllerActionStyleKey] integerValue] handler:^(UIAlertAction * _Nonnull action) {
            if (actionsHandlerBlock) {
                actionsHandlerBlock(weakAlertVC,action,[weakAlertVC.actions indexOfObject:action]);
            }
        }];
        BOOL enable = YES;
        if (actionDic[kUIAlertControllerActionEnableKey]) {
            enable = [actionDic[kUIAlertControllerActionEnableKey] boolValue];
        }
        alertAction.enabled = enable;
        [alertVC addAction:alertAction];
    }
    [viewController presentViewController:alertVC animated:YES completion:nil];
    return alertVC;
}

+ (instancetype)tf_showAlertInViewController:(UIViewController *)viewController withTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle ohterButtonsTitles:(NSArray<NSString *> *)ohterButtonsTitles actionsHandlerBlock:(TFAlertControllerActionsHandlerBlock)actionsHandlerBlock {
    NSMutableArray *actionsArray = [NSMutableArray array];
    if (cancelButtonTitle.length) {
        [actionsArray addObject:@{kUIAlertControllerActionStyleKey:@(UIAlertActionStyleCancel),kUIAlertControllerActionTitleKey:cancelButtonTitle}];
    }
    for (NSString *buttonTitle in ohterButtonsTitles) {
        [actionsArray addObject:@{kUIAlertControllerActionStyleKey:@(UIAlertActionStyleDefault),kUIAlertControllerActionTitleKey:buttonTitle}];
    }
    return [UIAlertController tf_showInViewController:viewController withStyle:UIAlertControllerStyleAlert title:title message:message actionsArray:actionsArray actionsHandlerBlock:actionsHandlerBlock];
}

+ (instancetype)tf_showActionSheetInViewController:(UIViewController *)viewController withTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelButtonTitle destructiveButtonTitle:(NSString *)destructiveButtonTitle ohterButtonsTitles:(NSArray<NSString *> *)ohterButtonsTitles actionsHandlerBlock:(TFAlertControllerActionsHandlerBlock)actionsHandlerBlock {
    NSMutableArray *actionsArray = [NSMutableArray array];
    if (cancelButtonTitle.length) {
        [actionsArray addObject:@{kUIAlertControllerActionStyleKey:@(UIAlertActionStyleCancel),kUIAlertControllerActionTitleKey:cancelButtonTitle}];
    }
    if (destructiveButtonTitle.length) {
        [actionsArray addObject:@{kUIAlertControllerActionStyleKey:@(UIAlertActionStyleDestructive),kUIAlertControllerActionTitleKey:destructiveButtonTitle}];
    }
    for (NSString *buttonTitle in ohterButtonsTitles) {
        [actionsArray addObject:@{kUIAlertControllerActionStyleKey:@(UIAlertActionStyleDefault),kUIAlertControllerActionTitleKey:buttonTitle}];
    }
    return [UIAlertController tf_showInViewController:viewController withStyle:UIAlertControllerStyleActionSheet title:title message:nil actionsArray:actionsArray actionsHandlerBlock:actionsHandlerBlock];
}


@end
