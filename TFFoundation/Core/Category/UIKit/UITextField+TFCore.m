//
//  UITextField+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/30.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "UITextField+TFCore.h"
#import "TFDefine.h"
#import "UIView+TFCore.h"

@implementation UITextField (TFCore)

- (void)tf_insetLeftSpace:(CGFloat)space {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, space, 1.0)];
    self.leftView = view;
    self.leftViewMode = UITextFieldViewModeAlways;
}

- (void)tf_insetLeftImageWithImageName:(NSString *)imageName contentInsets:(UIEdgeInsets)contentInsets {
        
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [imageView sizeToFit];
    imageView.contentMode = UIViewContentModeCenter;
    imageView.frame = CGRectMake(0, 0, imageView.tf_width+contentInsets.left+contentInsets.right, imageView.tf_height+contentInsets.top+contentInsets.bottom);
    self.leftView = imageView;
    self.leftViewMode = UITextFieldViewModeAlways;
    
}

- (void)tf_insetRightSpace:(CGFloat)space {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, space, 1.0)];
    self.rightView = view;
    self.rightViewMode = UITextFieldViewModeAlways;
}

- (void)tf_insetRightImageWithImageName:(NSString *)imageName contentInsets:(UIEdgeInsets)contentInsets {
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [imageView sizeToFit];
    imageView.contentMode = UIViewContentModeCenter;
    imageView.frame = CGRectMake(0, 0, imageView.tf_width+contentInsets.left+contentInsets.right, imageView.tf_height+contentInsets.top+contentInsets.bottom);
    self.rightView = imageView;
    self.rightViewMode = UITextFieldViewModeAlways;
}

@end
