//
//  UIAlertController+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/14.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 UIAlertController统一的按钮事件处理

 @param action UIAlertAction
 */
typedef void(^TFAlertControllerActionsHandlerBlock)(UIAlertController * __nonnull alertController,UIAlertAction * __nonnull action, NSInteger index);

typedef NSString *kUIAlertControllerActionKey NS_STRING_ENUM;

///<按钮的标题，对应的value为字符串
__nonnull FOUNDATION_EXPORT kUIAlertControllerActionKey const kUIAlertControllerActionTitleKey;
///<按钮的样式，对应的value为 UIAlertActionStyle 枚举的NSNumber形式.
__nonnull FOUNDATION_EXPORT kUIAlertControllerActionKey const kUIAlertControllerActionStyleKey;
///<按钮是否可操作，对应的value为 BOOL的NSNumber形式.
__nonnull FOUNDATION_EXPORT kUIAlertControllerActionKey const kUIAlertControllerActionEnableKey;

/**
 UIAlertController常用类别
 */
@interface UIAlertController (TFCore)

/**
 弹出视图公共方法

 @param viewController 从哪个视图控制器弹出
 @param style 方式
 @param title 标题
 @param message 内容
 @param actionsArray 按钮数组
 @param actionsHandlerBlock 按钮动作控制块
 @return UIAlertController
 */
+ (nonnull instancetype)tf_showInViewController:(nonnull UIViewController *)viewController
                                   withStyle:(UIAlertControllerStyle)style
                                       title:(nullable NSString *)title
                                     message:(nullable NSString *)message
                                actionsArray:(nullable NSArray<NSDictionary<kUIAlertControllerActionKey,id> *> *)actionsArray
                         actionsHandlerBlock:(nullable TFAlertControllerActionsHandlerBlock)actionsHandlerBlock;

/**
 弹出alert

 @param viewController 从哪个视图控制器弹出
 @param title 标题
 @param message 内容
 @param cancelButtonTitle 取消按钮
 @param ohterButtonsTitles 其他按钮数组
 @param actionsHandlerBlock 按钮动作处理块
 @return UIAlertController
 */
+ (nonnull instancetype)tf_showAlertInViewController:(nonnull UIViewController *)viewController
                                        withTitle:(nullable NSString *)title
                                          message:(nullable NSString *)message
                                cancelButtonTitle:(nullable NSString *)cancelButtonTitle
                               ohterButtonsTitles:(nullable NSArray<NSString *> *)ohterButtonsTitles
                              actionsHandlerBlock:(nullable TFAlertControllerActionsHandlerBlock)actionsHandlerBlock;

/**
 弹出actionSheet

 @param viewController 从哪个视图控制器弹出
 @param title 标题
 @param cancelButtonTitle 内容
 @param destructiveButtonTitle 取消按钮
 @param ohterButtonsTitles 其他按钮数组
 @param actionsHandlerBlock 按钮动作处理块
 @return UIAlertController
 */
+ (nonnull instancetype)tf_showActionSheetInViewController:(nonnull UIViewController *)viewController
                                              withTitle:(nullable NSString *)title
                                      cancelButtonTitle:(nullable NSString *)cancelButtonTitle
                                 destructiveButtonTitle:(nullable NSString *)destructiveButtonTitle
                                     ohterButtonsTitles:(nullable NSArray<NSString *> *)ohterButtonsTitles
                                    actionsHandlerBlock:(nullable TFAlertControllerActionsHandlerBlock)actionsHandlerBlock;




@end
