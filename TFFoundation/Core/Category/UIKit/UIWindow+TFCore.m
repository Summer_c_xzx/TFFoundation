//
//  UIWindow+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/9.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "UIWindow+TFCore.h"
#import "UIViewController+TFCore.h"

@implementation UIWindow (TFCore)

- (UIViewController *)tf_visibleViewController {
    UIViewController *rootVC = self.rootViewController;
    return [rootVC tf_visibleViewController];
}

- (void)tf_setRootViewController:(UIViewController *)rootViewController withAnimated:(BOOL)animated completion:(void (^)(BOOL))completion {
    
    UIViewAnimationOptions options = UIViewAnimationOptionTransitionNone;
    if (animated) {
        options = UIViewAnimationOptionTransitionCrossDissolve;
    }
    [self tf_setRootViewController:rootViewController withDuration:0.5 options:options completion:completion];
}

- (void)tf_setRootViewController:(UIViewController *)rootViewController withDuration:(NSTimeInterval)duration options:(UIViewAnimationOptions)options completion:(void (^ _Nullable)(BOOL))completion{
    
    [UIView transitionWithView:self duration:duration options:options animations:^{
        
        BOOL oldState = [UIView areAnimationsEnabled];
        [UIView setAnimationsEnabled:NO];
        self.rootViewController = rootViewController;
        [UIView setAnimationsEnabled:oldState];
        
    } completion:^(BOOL finished) {
        if (completion) {
            completion(finished);
        }
    }];
    
}

@end
