//
//  UIScrollView+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/8/7.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "UIScrollView+TFCore.h"

#import "NSObject+TFCore.h"
#import "TFDefine.h"

@interface UIScrollView ()

@property (nonatomic, strong, readwrite) UIView *tf_parallaxHeaderView;

@property (nonatomic, assign, readwrite) CGFloat tf_originParallaxHeaderHeight;

@property (nonatomic, assign, readwrite) CGFloat tf_minParallaxHeaderHeight;

@end

@implementation UIScrollView (TFCore)

+ (void)load {
    [super load];
    
    TFSwizzleMethod([self class], NSSelectorFromString(@"dealloc"), @selector(p_tf_dealloc));
}

- (void)p_tf_dealloc {
    if (self.tf_parallaxHeaderView) {
        [self removeObserver:self forKeyPath:NSStringFromSelector(@selector(contentOffset))];
    }
    [self p_tf_dealloc];
}

#pragma mark - public methods.

- (void)tf_addParallaxHeaderView:(__kindof UIView *)headerView {
    [self tf_addParallaxHeaderView:headerView minHeaderHeight:0.0];
}

- (void)tf_addParallaxHeaderView:(__kindof UIView *)headerView minHeaderHeight:(CGFloat)minHeaderHeight {
    headerView.clipsToBounds = YES;
    headerView.contentMode = UIViewContentModeScaleAspectFill;
    self.tf_originParallaxHeaderHeight = CGRectGetHeight(headerView.frame);
    self.tf_minParallaxHeaderHeight = minHeaderHeight;
    self.contentInset = UIEdgeInsetsMake(self.tf_originParallaxHeaderHeight, 0.0, 0.0, 0.0);
    //添加监听者
    [self addObserver:self forKeyPath:NSStringFromSelector(@selector(contentOffset)) options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    if (headerView.superview) {
        [headerView removeFromSuperview];
    }
    [self addSubview:headerView];
    self.tf_parallaxHeaderView = headerView;
}

- (void)tf_addParallaxHeaderWithImage:(UIImage *)image {
    [self tf_addParallaxHeaderWithImage:image minHeaderHeight:0.0];
}

- (void)tf_addParallaxHeaderWithImage:(UIImage *)image minHeaderHeight:(CGFloat)minHeaderHeight {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    CGFloat imageViewWidth = CGRectGetWidth(self.frame);
    CGSize imageSize = imageView.image.size;
    imageView.frame = CGRectMake(0, 0, imageViewWidth, (imageSize.height * imageViewWidth)/imageSize.width);
    [self tf_addParallaxHeaderView:imageView minHeaderHeight:minHeaderHeight];
}

#pragma mark - ObserScrollViewContentOffset

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if (self.tf_parallaxHeaderView && [keyPath isEqualToString:NSStringFromSelector(@selector(contentOffset))]) {
        CGRect lastHeaderFrame = self.tf_parallaxHeaderView.frame;
        CGFloat currentOffsetY = [change[NSKeyValueChangeNewKey] CGPointValue].y;
        CGFloat currentHeaderHeight = - currentOffsetY;
        if (currentHeaderHeight <= self.tf_minParallaxHeaderHeight) {
            currentHeaderHeight = self.tf_minParallaxHeaderHeight;
        }
        lastHeaderFrame.size.height = currentHeaderHeight;
        lastHeaderFrame.origin.y = currentOffsetY;
        CGFloat ratio = (currentHeaderHeight - self.tf_minParallaxHeaderHeight)/(self.tf_originParallaxHeaderHeight - self.tf_minParallaxHeaderHeight);
        if (self.tf_parallaxHeaderViewHeightChangeBlock) {
            self.tf_parallaxHeaderViewHeightChangeBlock(self.tf_parallaxHeaderView, self.tf_originParallaxHeaderHeight, ratio);
        }
        self.tf_parallaxHeaderView.frame = lastHeaderFrame;
    }
}

#pragma mark - custom properties.

- (void)setTf_parallaxHeaderView:(UIView *)tf_parallaxHeaderView {
    [self tf_setAssociateValue:tf_parallaxHeaderView withkey:@selector(tf_parallaxHeaderView) assoicationPolicy:TFAssociationPolicy_retain_nonatomic];
}

- (UIView *)tf_parallaxHeaderView {
    return [self tf_getAssociateValueForKey:_cmd];
}

- (void)setTf_parallaxHeaderViewHeightChangeBlock:(TFParallaxHeaderViewHeightChangeBlock)tf_parallaxHeaderViewHeightChangeBlock {
    [self tf_setAssociateValue:tf_parallaxHeaderViewHeightChangeBlock withkey:@selector(tf_parallaxHeaderViewHeightChangeBlock) assoicationPolicy:TFAssociationPolicy_copy_nonatomic];
}

- (TFParallaxHeaderViewHeightChangeBlock)tf_parallaxHeaderViewHeightChangeBlock {
    return [self tf_getAssociateValueForKey:_cmd];
}

- (void)setTf_originParallaxHeaderHeight:(CGFloat)tf_originParallaxHeaderHeight {
    [self tf_setAssociateValue:@(tf_originParallaxHeaderHeight) withkey:@selector(tf_originParallaxHeaderHeight) assoicationPolicy:TFAssociationPolicy_retain_nonatomic];
}

- (CGFloat)tf_originParallaxHeaderHeight {
    return [[self tf_getAssociateValueForKey:_cmd] floatValue];
}

- (void)setTf_minParallaxHeaderHeight:(CGFloat)tf_minParallaxHeaderHeight {
    return [self tf_setAssociateValue:@(tf_minParallaxHeaderHeight) withkey:@selector(tf_minParallaxHeaderHeight) assoicationPolicy:TFAssociationPolicy_retain_nonatomic];
}

- (CGFloat)tf_minParallaxHeaderHeight {
    return [[self tf_getAssociateValueForKey:_cmd] floatValue];
}


@end
