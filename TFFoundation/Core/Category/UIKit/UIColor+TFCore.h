//
//  UIColor+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/9.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 UIColor常用类别
 */
@interface UIColor (TFCore)

/**
 根据RGB创建颜色
 
 @param red red
 @param green green
 @param blue blue
 @return UIColor
 */
+ (nonnull UIColor *)tf_colorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue;

/**
 根据RGB和透明度创建颜色
 
 @param red red
 @param green green
 @param blue blue
 @param alpha alpha
 @return UIColor
 */
+ (nonnull UIColor *)tf_colorWithRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;

/**
 根据hex字符串创建UIColor

 @param hexString hex字符串
 @return UIColor
 */
+ (nonnull UIColor *)tf_colorWithHexString:(nonnull NSString *)hexString;

/**
 根据hex字符串和透明度创建UIColor
 
 @param hexString hex字符串
 @param alpha 透明度
 @return UIColor
 */
+ (nonnull UIColor *)tf_colorWithHexString:(nonnull NSString *)hexString alpha:(CGFloat)alpha;

/**
 返回一个随机的颜色

 @return UIColor
 */
+ (nonnull UIColor *)tf_randomColor;

/**
 返回一个渐变色

 @param color1 颜色1
 @param color2 颜色2
 @param height 渐变高度
 @return UIColor
 */
+ (nonnull UIColor *)tf_gradientColorFromColor:(nonnull UIColor *)color1 toColor:(nonnull UIColor *)color2 withHeight:(CGFloat)height;

@end
