//
//  UITextField+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/30.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 UITextField常用类别
 */
@interface UITextField (TFCore)

/**
 插入左边距离

 @param space 距离
 */
- (void)tf_insetLeftSpace:(CGFloat)space;

/**
 插入左边图片

 @param imageName 图片名字
 @param contentInsets 内容的扩展insets
 */
- (void)tf_insetLeftImageWithImageName:(NSString *)imageName contentInsets:(UIEdgeInsets)contentInsets;

/**
 插入右边距离

 @param space 距离
 */
- (void)tf_insetRightSpace:(CGFloat)space;


/**
 插入左边图片
 
 @param imageName 图片名字
 @param contentInsets 内容的扩展insets
 */
- (void)tf_insetRightImageWithImageName:(NSString *)imageName contentInsets:(UIEdgeInsets)contentInsets;

@end
