//
//  UIButton+TFCore.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/4/11.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TFViewActionTarget.h"

typedef NSString *kTFUIButtonAttributeKey NS_STRING_ENUM;

///< 按钮状态属性设置key，值为UIControlState的NSNumber形式,这个值若不设置，则默认UIControlStateNormal
_Nonnull FOUNDATION_EXPORT kTFUIButtonAttributeKey const kTFUIButtonStateAttributeKey;
///< 按钮标题属性设置key，值为NSString
_Nonnull FOUNDATION_EXPORT kTFUIButtonAttributeKey const kTFUIButtonTitleAttributeKey;
///< 按钮标题颜色属性设置key，值为UIColor
_Nonnull FOUNDATION_EXPORT kTFUIButtonAttributeKey const kTFUIButtonTitleColorAttributeKey;
///< 按钮属性字符串属性设置key，值为NSAttributedString
_Nonnull FOUNDATION_EXPORT kTFUIButtonAttributeKey const kTFUIButtonAttributedTitleAttributeKey;
///< 按钮图片属性设置key，值为UIImage
_Nonnull FOUNDATION_EXPORT kTFUIButtonAttributeKey const kTFUIButtonImageAttributeKey;
///< 按钮背景图片属性设置key，值为UIImage
_Nonnull FOUNDATION_EXPORT kTFUIButtonAttributeKey const kTFUIButtonBackgroundImageAttributeKey;

/**
 UIButton常用类别
 */
@interface UIButton (TFCore)

/**
 根据属性配置字典，初始化一个UIButton

 @param attributesArray 属性配置字典
 @param touchUpInsideActionHandler 点击事件
 @return UIButton
 */
+ (nonnull instancetype)tf_buttonWithAttributesArray:(nonnull NSArray<NSDictionary<kTFUIButtonAttributeKey,id> *> *)attributesArray touchUpInsideActionHandler:(nullable TFViewActionHandlerBlock)touchUpInsideActionHandler;

/**
 根据标题的文本、颜色、大小创建UIButton

 @param title 标题
 @param titleColor 标题颜色
 @param font 字体大小
 @param touchUpInsideActionHandler 点击事件
 @return UIButton
 */
+ (nonnull instancetype)tf_buttonWithTitle:(nonnull NSString *)title color:(nonnull UIColor *)titleColor font:(nonnull UIFont *)font touchUpInsideActionHandler:(nullable TFViewActionHandlerBlock)touchUpInsideActionHandler;

/**
 根据图片创建UIButton

 @param image 图片
 @param touchUpInsideActionHandler 点击事件
 @return UIButton
 */
+ (nonnull instancetype)tf_buttonWithImage:(nonnull UIImage *)image touchUpInsideActionHandler:(nullable TFViewActionHandlerBlock)touchUpInsideActionHandler;

/**
 根据普通状态下的图片和选中状态下的图片创建UIButton

 @param image 普通状态下的图片
 @param selectedImage 选中状态下的图片
 @param touchUpInsideActionHandler 点击事件
 @return UIButton
 */
+ (nonnull instancetype)tf_buttonWithImage:(nonnull UIImage *)image selectedImage:(nonnull UIImage *)selectedImage touchUpInsideActionHandler:(nullable TFViewActionHandlerBlock)touchUpInsideActionHandler;

/**
 根据普通状态下的标题和选中状态下的标题创建UIButton

 @param font 标题字体
 @param title 普通状态下的标题
 @param titleColor 普通状态下的标题颜色
 @param selectedTitle 选择状态下的标题
 @param selectedTitleColor 选择状态下的标题颜色
 @param touchUpInsideActionHandler 点击事件
 @return UIButton
 */
+ (nonnull instancetype)tf_buttonWithFont:(nonnull UIFont *)font title:(nonnull NSString *)title titleColor:(nonnull UIColor *)titleColor selectedTitle:(nonnull NSString *)selectedTitle selectedTitleColor:(nonnull UIColor *)selectedTitleColor touchUpInsideActionHandler:(nullable TFViewActionHandlerBlock)touchUpInsideActionHandler;

/**
 通过属性配置字典，设置按钮

 @param attributesArray 属性配置字典
 */
- (void)tf_setButtonWithAttributesArray:(nonnull NSArray<NSDictionary<kTFUIButtonAttributeKey,id> *> *)attributesArray;

@end
