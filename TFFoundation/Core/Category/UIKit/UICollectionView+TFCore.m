//
//  UICollectionView+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/7/10.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "UICollectionView+TFCore.h"

@implementation UICollectionView (TFCore)

#pragma mark - UICollectionView注册和重用

- (UICollectionViewCell *)tf_dequeueReusableCellWithClass:(Class)aClass forIndexPath:(NSIndexPath *)indexPath {
    return [self dequeueReusableCellWithReuseIdentifier:NSStringFromClass(aClass) forIndexPath:indexPath];
}

- (UICollectionReusableView *)tf_dequeueReusableSectionHeaderViewWithClass:(Class)aClass forIndexPath:(NSIndexPath *)indexPath{
    return [self dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass(aClass) forIndexPath:indexPath];
}

- (UICollectionReusableView *)tf_dequeueReusableSectionFooterViewWithClass:(Class)aClass forIndexPath:(NSIndexPath *)indexPath {
    return [self dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass(aClass) forIndexPath:indexPath];
}

- (void)tf_registerCellWithClass:(Class)aClass {
    [self registerClass:aClass forCellWithReuseIdentifier:NSStringFromClass(aClass)];
}

- (void)tf_registerNibCellWithClass:(Class)aClass {
    [self registerNib:[UINib nibWithNibName:NSStringFromClass(aClass) bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:NSStringFromClass(aClass)];
}

- (void)tf_registerSectionHeaderViewWithClass:(Class)aClass {
    [self registerClass:aClass forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass(aClass)];
}

- (void)tf_registerSectionNibHeaderViewWithClass:(Class)aClass {
    [self registerNib:[UINib nibWithNibName:NSStringFromClass(aClass) bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass(aClass)];
}

- (void)tf_registerSectionFooterViewWithClass:(Class)aClass {
    [self registerClass:aClass forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass(aClass)];
}

- (void)tf_registerSectionNibFooterViewWithClass:(Class)aClass {
    [self registerNib:[UINib nibWithNibName:NSStringFromClass(aClass) bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:NSStringFromClass(aClass)];
}


@end
