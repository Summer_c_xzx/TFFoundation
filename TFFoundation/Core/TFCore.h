//
//  Header.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/12.
//

#ifndef Header_h
#define Header_h

///--------------------------------
/// @name Define
///--------------------------------

#import <TFFoundation/TFDefine.h>

///--------------------------------
/// @name Category
///--------------------------------

//Foundation
#import <TFFoundation/NSArray+TFCore.h>
#import <TFFoundation/NSBundle+TFCore.h>
#import <TFFoundation/NSData+TFCore.h>
#import <TFFoundation/NSDate+TFCore.h>
#import <TFFoundation/NSDictionary+TFCore.h>
#import <TFFoundation/NSFileManager+TFCore.h>
#import <TFFoundation/NSMutableArray+TFCore.h>
#import <TFFoundation/NSNumber+TFCore.h>
#import <TFFoundation/NSObject+TFCore.h>
#import <TFFoundation/NSString+TFCore.h>

//UIKit
#import <TFFoundation/UIAlertController+TFCore.h>
#import <TFFoundation/UIApplication+TFCore.h>
#import <TFFoundation/UIBarButtonItem+TFCore.h>
#import <TFFoundation/UIButton+TFCore.h>
#import <TFFoundation/UICollectionView+TFCore.h>
#import <TFFoundation/UIColor+TFCore.h>
#import <TFFoundation/UIDevice+TFCore.h>
#import <TFFoundation/UIImage+TFCore.h>
#import <TFFoundation/UINavigationController+TFCore.h>
#import <TFFoundation/UIScrollView+TFCore.h>
#import <TFFoundation/UITableView+TFCore.h>
#import <TFFoundation/UITextField+TFCore.h>
#import <TFFoundation/UIView+TFCore.h>
#import <TFFoundation/UIViewController+TFCore.h>
#import <TFFoundation/UIWindow+TFCore.h>

///--------------------------------
/// @name ViewController
///--------------------------------

#import <TFFoundation/TFWebViewController.h>


///--------------------------------
/// @name Utility
///--------------------------------
#import <TFFoundation/TFWeakTimer.h>
#import <TFFoundation/TFLocationHandler.h>

#endif /* Header_h */
