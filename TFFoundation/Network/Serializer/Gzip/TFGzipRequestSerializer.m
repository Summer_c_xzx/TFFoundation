//
//  TFGzipRequestSerializer.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/20.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "TFGzipRequestSerializer.h"
#import "TFRequesHelper.h"

@implementation TFGzipRequestSerializer

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method URLString:(NSString *)URLString parameters:(id)parameters error:(NSError *__autoreleasing  _Nullable *)error {
    NSMutableURLRequest *request = [super requestWithMethod:method URLString:URLString parameters:parameters error:error];
    if (request.HTTPBody) {
        NSError *compressError = nil;
        NSData *compressedData = [TFRequesHelper tf_request_gZippedDataWithData:request.HTTPBody];
        if (!compressError&&compressedData) {
            [request setValue:@"gzip" forHTTPHeaderField:@"Content-Encoding"];
            [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
            [request setHTTPBody:compressedData];
        }
    }
    return request;
}



@end
