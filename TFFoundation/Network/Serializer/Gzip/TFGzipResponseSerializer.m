//
//  TFGzipResponseSerializer.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/20.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "TFGzipResponseSerializer.h"
#import "TFRequesHelper.h"

@implementation TFGzipResponseSerializer

- (id)responseObjectForResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *__autoreleasing  _Nullable *)error {
    id responseObject = [super responseObjectForResponse:response data:data error:error];
    if (responseObject) {
        if ([TFRequesHelper tf_request_gZippedDataWithData:responseObject]) {
            //解压缩
            responseObject = [TFRequesHelper tf_request_gUnZippedDataWithData:responseObject];
        }
    }
    return responseObject;
}

@end
