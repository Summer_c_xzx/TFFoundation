//
//  TFRequestUtility.h
//  AFNetworking
//
//  Created by 夏之祥 on 2019/4/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TFRequesHelper : NSObject

/**
 压缩gzip

 @param data 数据
 @param level 等级
 @return NSData
 */
+ (NSData *)tf_request_gZippedDataWithData:(NSData *)data compressionLevel:(float)level;

/**
 压缩gzip

 @param data 数据
 @return NSData
 */
+ (NSData *)tf_request_gZippedDataWithData:(NSData *)data;

/**
 解压缩gzip

 @param data 数据
 @return NSData
 */
+ (NSData *)tf_request_gUnZippedDataWithData:(NSData *)data;

/**
 md5字符串

 @param string 字符串
 @return NSString
 */
+ (NSString *)tf_request_md5StringWithSrting:(NSString *)string;


@end

NS_ASSUME_NONNULL_END
