//
//  TFQueueRequest.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/21.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TFRequest.h"

@class TFQueueRequest;

/**
 每个请求结果处理

 @param queueRequest 队列请求类
 @param request 请求类
 @param requestIndex 请求类在队列中的索引
 @param response 返回结果
 @param error 返回错误
 @return BOOL 是否继续下面的请求，返回为NO，取消所有的请求，返回YES则继续下面的请求
 */
typedef BOOL(^TFQueueRequestResponseBlock)(TFQueueRequest * _Nonnull queueRequest, __kindof TFRequest * _Nonnull request, NSInteger requestIndex, _Nullable id response, NSError * _Nullable error);

/**
 队列请求完成时调用block

 @param request 发送请求
 */
typedef void(^TFQueueRequestCompletionBlock)(__kindof TFQueueRequest * _Nonnull request);

typedef NSString * kTFQueueRequestConfigKey NS_STRING_ENUM;
//设置请求的路径
_Nonnull FOUNDATION_EXPORT kTFQueueRequestConfigKey const kTFQueueRequestConfigPathKey;
//设置请求参数
_Nonnull FOUNDATION_EXPORT kTFQueueRequestConfigKey const kTFQueueRequestConfigParamsKey;
//设置请求的对象类
_Nonnull FOUNDATION_EXPORT kTFQueueRequestConfigKey const kTFQueueRequestConfigRequestClassKey;
//设置返回对象的映射类
_Nonnull FOUNDATION_EXPORT kTFQueueRequestConfigKey const kTFQueueRequestConfigReponseMapClassNameKey;

/**
 队列请求类型
 */
typedef NS_ENUM(NSUInteger, TFQueueRequestType) {
    /**
     串行方式，依次请求
     */
    TFQueueRequestTypeSerial,
    /**
     并行方式，同时请求
     */
    TFQueueRequestTypeConcurrent,
};

/**
 队列请求数据类
 */
@interface TFQueueRequest : NSObject

/**
 请求request数组
 */
@property (nonatomic, strong, readonly, nonnull) NSArray<__kindof TFRequest *> *requestArray;

/**
 请求方式串行还是并行，默认并行
 */
@property (nonatomic, assign) TFQueueRequestType requestType;

/**
 已完成的请求数量
 */
@property (nonatomic, assign, readonly) NSInteger finishedRequestCount;

/**
 根据多个请求对象数组，创建队列请求
 
 @param requestArray 请求对象数组
 @return 队列请求对象
 */
- (nonnull instancetype)initWithRequestArray:(nonnull NSArray<__kindof TFRequest *> *)requestArray;

/**
 根据配置字典快速创建队列请求

 @param configArray 配置数组
 @return 队列请求对象
 */
- (nonnull instancetype)initWithRequestConfigArray:(nonnull NSArray<NSDictionary<kTFQueueRequestConfigKey,id> *> *)configArray;

/**
 开启请求

 @param responseBlock 返回的block，每个request返回成功后都会调用
 @param completionBlock 所有的请求都完成后调用
 */
- (void)startRequestWithResponse:(nullable TFQueueRequestResponseBlock)responseBlock completionBlock:(nullable TFQueueRequestCompletionBlock)completionBlock;

@end
