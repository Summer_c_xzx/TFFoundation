Pod::Spec.new do |s|
  s.name         = "TFFoundation"
  s.version      = "2.0.0"
  s.summary      = "iOS基础框架"
  s.homepage     = "https://gitee.com/Summer_c_xzx/TFFoundation.git"
  s.license      = "Copyright (C) 2019 Summer, Inc.  All rights reserved."
  s.author             = { "Summer" => "418463162@qq.com" }
  s.social_media_url   = "https://gitee.com/Summer_c_xzx"
  s.ios.deployment_target = "8.0"
  s.source       = { :git => "https://gitee.com/Summer_c_xzx/TFFoundation.git", :tag => s.version.to_s}
  s.requires_arc = true
  s.license = { :type => "MIT", :file => "LICENSE" }
  s.libraries = 'z', 'sqlite3'
  s.frameworks = 'UIKit', 'QuartzCore'

  s.subspec 'Core' do |core|
  core.source_files = 'TFFoundation/Core/**/*.{h,m,c}'
  core.dependency 'Masonry'
  end

  s.subspec 'Network' do |network|
  network.source_files = 'TFFoundation/Network/**/*.{h,m,c}'
  network.dependency 'AFNetworking'
  end

  s.subspec 'UI' do |ui|
  ui.source_files = 'TFFoundation/UI/**/*.{h,m,c}'
  ui.dependency 'Masonry'
  end

end
