//
//  TestObjectSubObject.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/7/20.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TestObjectSubObject : NSObject

@property (nonatomic, assign) NSInteger age;

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *sex;

@end
