//
//  TestObject.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/7/20.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TestObjectSubObject.h"

@interface TestObject : NSObject

@property (nonatomic, assign) BOOL state;

@property (nonatomic, strong) NSString *info;

@property (nonatomic, assign) NSInteger type;

@property (nonatomic, assign) NSTimeInterval time;

@property (nonatomic, strong) NSDate *date;

@property (nonatomic, strong) NSObject *testModel;

@property (nonatomic, strong) NSFileManager *file;

@property (nonatomic, strong) TestObjectSubObject *data;


- (void)testMethod;

@end
