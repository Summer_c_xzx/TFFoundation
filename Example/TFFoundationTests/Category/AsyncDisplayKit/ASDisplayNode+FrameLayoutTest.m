//
//  ASDisplayNode+FrameLayoutTest.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/6/23.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ASDisplayNode+TFCore.h"

@interface ASDisplayNode_FrameLayoutTest : XCTestCase

@property (nonatomic, strong) ASDisplayNode *testNode;

@end

@implementation ASDisplayNode_FrameLayoutTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _testNode = [[ASDisplayNode alloc] init];
    _testNode.frame = CGRectMake(10.0, 10.0, 100.0, 100.0);
}

- (void)test_tf_width {
    
    XCTAssertEqual(_testNode.tf_width, 100.0);
    CGFloat targetWidth = 1000.0;
    _testNode.tf_width = targetWidth;
    XCTAssertEqual(_testNode.tf_width, targetWidth);
}

- (void)test_tf_height {
    XCTAssertEqual(_testNode.tf_height, 100.0);
    CGFloat targetHeight = 1000.0;
    _testNode.tf_height = targetHeight;
    XCTAssertEqual(_testNode.tf_height, targetHeight);
}

- (void)test_tf_size {
    XCTAssertTrue(CGSizeEqualToSize(_testNode.tf_size, CGSizeMake(100.0, 100.0)));
    CGSize targetSize = CGSizeMake(1000.0, 1000.0);
    _testNode.tf_size = targetSize;
    XCTAssertTrue(CGSizeEqualToSize(_testNode.tf_size, targetSize));
}

- (void)test_tf_left {
    XCTAssertEqual(_testNode.tf_left, 10.0);
    CGFloat targetLeft = 20.0;
    _testNode.tf_left = targetLeft;
    XCTAssertEqual(_testNode.tf_left, targetLeft);
}

- (void)test_tf_top {
    XCTAssertEqual(_testNode.tf_top, 10.0);
    CGFloat targetTop = 20.0;
    _testNode.tf_top = targetTop;
    XCTAssertEqual(_testNode.tf_top, targetTop);
}

- (void)test_tf_right {
    XCTAssertEqual(_testNode.tf_right, 110.0);
    CGFloat targetRight = 20.0;
    _testNode.tf_right = targetRight;
    XCTAssertEqual(_testNode.tf_right, targetRight);
}

- (void)test_tf_bottom {
    XCTAssertEqual(_testNode.tf_bottom, 110.0);
    CGFloat targetBottom = 120.0;
    _testNode.tf_bottom = targetBottom;
    XCTAssertEqual(_testNode.tf_bottom, targetBottom);
}

- (void)test_tf_origin {
    XCTAssertTrue(CGPointEqualToPoint(_testNode.tf_origin, CGPointMake(10.0, 10.0)));
    CGPoint targetOrigin = CGPointMake(30.0, 30.0);
    _testNode.tf_origin = targetOrigin;
    XCTAssertTrue(CGPointEqualToPoint(_testNode.tf_origin, targetOrigin));
}

- (void)test_tf_center {
    XCTAssertTrue(CGPointEqualToPoint(_testNode.tf_center, CGPointMake(60.0, 60.0)));
    CGPoint targetCenter = CGPointMake(30.0, 30.0);
    _testNode.tf_center = targetCenter;
    XCTAssertTrue(CGPointEqualToPoint(_testNode.tf_center, targetCenter));
}

- (void)test_tf_rightTop {
    XCTAssertTrue(CGPointEqualToPoint(_testNode.tf_rightTop, CGPointMake(110.0, 10.0)));
    CGPoint targetRightTop = CGPointMake(130.0, 30.0);
    _testNode.tf_rightTop = targetRightTop;
    XCTAssertTrue(CGPointEqualToPoint(_testNode.tf_rightTop, targetRightTop));
}

- (void)test_tf_leftBottom {
    XCTAssertTrue(CGPointEqualToPoint(_testNode.tf_leftBottom, CGPointMake(10.0, 110.0)));
    CGPoint targetLeftBottom = CGPointMake(20.0, 130.0);
    _testNode.tf_leftBottom = targetLeftBottom;
    XCTAssertTrue(CGPointEqualToPoint(_testNode.tf_leftBottom, targetLeftBottom));
}

- (void)test_tf_rightBottom {
    XCTAssertTrue(CGPointEqualToPoint(_testNode.tf_rightBottom, CGPointMake(110.0, 110.0)));
    CGPoint targetRightBottom = CGPointMake(120.0, 130.0);
    _testNode.tf_rightBottom = targetRightBottom;
    XCTAssertTrue(CGPointEqualToPoint(_testNode.tf_rightBottom, targetRightBottom));
}

- (void)test_tf_centerX {
    XCTAssertEqual(_testNode.tf_centerX, 60.0);
    CGFloat targetCenterX = 100.0;
    _testNode.tf_centerX = targetCenterX;
    XCTAssertEqual(_testNode.tf_centerX, targetCenterX);
}

- (void)test_tf_centerY {
    XCTAssertEqual(_testNode.tf_centerY, 60.0);
    CGFloat targetCenterY = 100.0;
    _testNode.tf_centerY = targetCenterY;
    XCTAssertEqual(_testNode.tf_centerY, targetCenterY);
}


@end
