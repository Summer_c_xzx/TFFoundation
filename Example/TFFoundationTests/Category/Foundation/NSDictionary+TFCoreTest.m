//
//  NSDictionary+TFCoreTest.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/7/18.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSDictionary+TFCore.h"

@interface NSDictionary_TFCoreTest : XCTestCase

@property (nonatomic, strong) NSDictionary *testDic;

@end

@implementation NSDictionary_TFCoreTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _testDic = @{@"name":@"John",@"age":@"20",@"sex":@"男"};
}

- (void)test_tf_sortedAllKeysArrayWithAscending {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    NSArray *sortedAllKeysArray = [_testDic tf_sortedAllKeysArrayWithAscending:YES];
    XCTAssertTrue([[sortedAllKeysArray firstObject] isEqualToString:@"age"]);
}

- (void)test_tf_sortedAllKeysArrayWithDescriptorDic {
    NSString *jsonString = [_testDic tf_prettyJsonString];
    NSLog(@"jsonString:%@",jsonString);
    XCTAssertTrue(jsonString);
}

- (void)test_tf_mapValuesUsingBlock {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    NSDictionary *mapedDic = [_testDic tf_mapValuesUsingBlock:^id _Nonnull(id  _Nonnull key, NSString * _Nonnull value) {
        return [value stringByAppendingString:key];
    }];
    XCTAssertTrue([[mapedDic objectForKey:@"name"]isEqualToString:@"Johnname"]);
}

- (void)test_tf_mapValuesWithOptions {
    NSDictionary *mapedDic = [_testDic tf_mapValuesWithOptions:NSEnumerationReverse usingBlock:^id _Nonnull(id  _Nonnull key, id  _Nonnull value) {
        return [value stringByAppendingString:key];
    }];
    XCTAssertTrue([[mapedDic objectForKey:@"name"]isEqualToString:@"Johnname"]);
}


@end
