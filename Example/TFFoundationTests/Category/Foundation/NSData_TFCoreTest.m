//
//  NSData+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/6/28.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSData+TFCore.h"

@interface NSData_TFCoreTest : XCTestCase

@property (nonatomic, strong) NSData *testData;

@end

@implementation NSData_TFCoreTest

- (void)setUp {
    [super setUp];
    
    _testData = [@"123456" dataUsingEncoding:NSUTF8StringEncoding];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)test_tf_md5Data {
    NSData *md5Data = [_testData tf_md5Data];
    XCTAssertTrue(md5Data);
}

- (void)test_tf_md5String {
    NSString *md5String = [_testData tf_md5String];
    XCTAssertTrue(md5String);
}

- (void)test_tf_utf8EncodedString {
    NSString *utf8EncodedString = [_testData tf_utf8EncodedString];
    XCTAssertTrue(utf8EncodedString);
}

- (void)test_tf_dataWithBase64EncodedString {
    NSData *data = [NSData tf_dataWithBase64EncodedString:@"MTIzNDU2"];
    XCTAssertTrue(data);
}

- (void)test_tf_base64EncodedString {
    NSString *base64EncodedString = [_testData tf_base64EncodedString];
    XCTAssertTrue(base64EncodedString);
}

- (void)test_tf_base64EncodedStringWithWrapWith {
    NSString *base64EncodedString = [_testData tf_base64EncodedStringWithWrapWith:5];
    XCTAssertTrue(base64EncodedString);
}

- (void)test_tf_decodeToJsonObject {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:@{@"name":@"John",@"time":@"2017-06-08"} options:NSJSONWritingPrettyPrinted error:nil];
    id jsonObj = [jsonData tf_decodeToJsonObject];
    XCTAssertTrue(jsonObj);
}

- (void)test_tf_gZippedDataWithCompressionLevel {
    NSData *gZippedData = [_testData tf_gZippedDataWithCompressionLevel:1];
    XCTAssertTrue(gZippedData);
}

- (void)test_tf_gZippedData {
    NSData *gZippedData = [_testData tf_gZippedData];
    XCTAssertTrue(gZippedData);
}

- (void)test_tf_gUnZippedData {
    NSData *gUnZippedData = [_testData tf_gUnZippedData];
    XCTAssertTrue([gUnZippedData isEqual:_testData]);
}

- (void)test_tf_isGZippedData {
    XCTAssertTrue(![_testData tf_isGZippedData]);
}


@end
