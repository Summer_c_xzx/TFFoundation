//
//  NSNumber+TFCoreTest.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/7/20.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSNumber+TFCore.h"

@interface NSNumber_TFCoreTest : XCTestCase

@property (nonatomic, strong) NSNumber *testNumber;

@property (nonatomic, strong) NSString *testNumberString;

@end

@implementation NSNumber_TFCoreTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _testNumber = @(0.4344);
    _testNumberString = @"43.44%";
}

- (void)test_tf_stringFromNumberWithFormatter {
    NSString *formatterString = [_testNumber tf_stringFromNumberWithFormatter:NSNumberFormatterPercentStyle];
    NSLog(@"formatterString:%@",formatterString);
    XCTAssertTrue([formatterString hasSuffix:@"%"]);
}

- (void)test_tf_numberWithNumberString {
    NSNumber *numebr = [NSNumber tf_numberWithNumberString:_testNumberString formatter:NSNumberFormatterPercentStyle];
    NSLog(@"numebr:%@",numebr);
    XCTAssertTrue(numebr);
}

@end
