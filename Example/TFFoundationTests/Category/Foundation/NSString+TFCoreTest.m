//
//  NSString+TFCoreTest.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/7/20.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+TFCore.h"

@interface NSString_TFCoreTest : XCTestCase

@property (nonatomic, strong) NSString *testString;

@end

@implementation NSString_TFCoreTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _testString = @"123456";
}

- (void)test_tf_md5String {
    NSString *encodedString = [_testString tf_md5String];
    NSLog(@"123456_md5String:%@",encodedString);
    XCTAssertTrue(encodedString);
}

- (void)test_tf_base64EncodedString {
    NSString *encodedString = [_testString tf_base64EncodedString];
    NSLog(@"123456_base64EncodedString:%@",encodedString);
    XCTAssertTrue(encodedString);
}

- (void)test_tf_urlEncodedString {
    NSString *encodedString = [_testString tf_urlEncodedString];
    NSLog(@"123456_urlEncodedString:%@",encodedString);
    XCTAssertTrue(encodedString);
}

- (void)test_tf_urlDecodeString {
    NSString *encodedString = [_testString tf_urlEncodedString];
    XCTAssertTrue([[encodedString tf_urlDecodeString]isEqualToString:@"123456"]);
}

- (void)test_tf_pinyinStringWithPhoneticSymbol {
    NSString *chineseString = @"重庆";
    NSString *pinyinString = [chineseString tf_pinyinStringWithPhoneticSymbol];
    NSLog(@"重庆pinyinStringWithPhoneticSymbol:%@",pinyinString);
    XCTAssertTrue(pinyinString);
}

- (void)test_tf_pinyinString {
    NSString *chineseString = @"重庆";
    NSString *pinyinString = [chineseString tf_pinyinString];
    NSLog(@"重庆pinyinString:%@",pinyinString);
    XCTAssertTrue([pinyinString isEqualToString:@"chong qing"]);
}

- (void)test_tf_pinyinCharArray {
    NSString *chineseString = @"重庆";
    NSArray *pinyinArray = [chineseString tf_pinyinCharArray];
    XCTAssertTrue([[pinyinArray firstObject]isEqualToString:@"chong"]);
}

- (void)test_tf_pinyinStringWithoutBlank {
    NSString *chineseString = @"重庆";
    NSString *pinyinString = [chineseString tf_pinyinStringWithoutBlank];
    NSLog(@"重庆pinyinString:%@",pinyinString);
    XCTAssertTrue([pinyinString isEqualToString:@"chongqing"]);
}

- (void)test_tf_pinyinInitialString {
    NSString *chineseString = @"重庆";
    NSString *pinyinInitialString = [chineseString tf_pinyinInitialString];
    XCTAssertTrue([pinyinInitialString isEqualToString:@"c"]);
}

- (void)test_tf_pinyinUppercaseInitialString {
    NSString *chineseString = @"重庆";
    NSString *pinyinInitialString = [chineseString tf_pinyinUppercaseInitialString];
    XCTAssertTrue([pinyinInitialString isEqualToString:@"C"]);
}

- (void)test_tf_isPhoneNumber {
     XCTAssertTrue([@"18567890098" tf_isPhoneNumber]);
}

- (void)test_tf_isEmailAddress {
    XCTAssertTrue([@"timefaceDev@timeface.cn" tf_isEmailAddress]);
}

- (void)test_tf_isIdentityCardNumber {
    XCTAssertTrue([@"110103199304049892" tf_isIdentityCardNumber]);
}

- (void)test_tf_isBankCardNumber {
    XCTAssertTrue([@"6228480402564890018" tf_isBankCardNumber]);
}

- (void)test_tf_isWebUrl {
    XCTAssertTrue([@"https://www.baidu.com" tf_isWebUrl]);
}

- (void)test_tf_isChinese {
    XCTAssertTrue([@"哈" tf_isChinese]);
}

- (void)test_tf_isEmoji {
    XCTAssertTrue([@"😭" tf_isEmoji]);
}

- (void)test_tf_isIncludingEmoji {
    XCTAssertTrue([@"1231ui3😭" tf_isIncludingEmoji]);
}

- (void)test_tf_removeEmojiString {
    XCTAssertTrue([[@"1231ui3😭" tf_removeEmojiString] isEqualToString:@"1231ui3"]);
}

- (void)test_tf_isContainChinese {
    XCTAssertTrue([@"回答ddjaiwodajwdi" tf_isContainChinese]);
}

@end
