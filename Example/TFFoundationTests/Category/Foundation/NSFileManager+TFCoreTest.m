//
//  NSFileManager+TFCoreTest.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/7/20.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSFileManager+TFCore.h"

@interface NSFileManager_TFCoreTest : XCTestCase

@end

@implementation NSFileManager_TFCoreTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

- (void)test_tf_urlForDirectory {
    NSURL *url = [NSFileManager tf_urlForDirectory:NSLibraryDirectory];
    NSLog(@"NSLibraryDirectory__URL:%@",url);
    XCTAssertTrue(url);
}

- (void)test_tf_pathForDirectory {
    NSString *path = [NSFileManager tf_pathForDirectory:NSLibraryDirectory];
    NSLog(@"NSLibraryDirectory__path:%@",path);
    XCTAssertTrue(path);
}

- (void)test_tf_fileSizeAtPath {
    NSInteger fileSize = [NSFileManager tf_fileSizeAtPath:[NSFileManager tf_cachesPath]];
    XCTAssertTrue(fileSize>0);
}

- (void)test_tf_getFolderSizeWithPath {
    double fileSize = [NSFileManager tf_getFolderSizeWithPath:[NSFileManager tf_cachesPath]];
    XCTAssertTrue(fileSize>0);
}


@end
