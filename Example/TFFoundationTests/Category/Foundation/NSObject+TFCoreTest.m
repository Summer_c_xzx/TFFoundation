//
//  NSObject+TFCoreTest.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/7/20.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TestObject.h"
#import "NSObject+TFCore.h"

@interface NSObject_TFCoreTest : XCTestCase

@property (nonatomic, strong) TestObject *testObject;

@property (nonatomic, strong) TestObject *testObject1;


@end

@implementation NSObject_TFCoreTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _testObject = [[TestObject alloc] init];
    _testObject.state = YES;
    _testObject.info = @"success";
    TestObjectSubObject *subObject = [[TestObjectSubObject alloc] init];
    subObject.name = @"Janney";
    subObject.age = 20;
    subObject.sex = @"女";
    _testObject.data = subObject;
    
    _testObject1 = [[TestObject alloc] init];
    _testObject1.state = YES;
    _testObject1.info = @"success";
    TestObjectSubObject *subObject1 = [[TestObjectSubObject alloc] init];
    subObject1.name = @"Janney";
    subObject1.age = 26;
    subObject1.sex = @"女";
    _testObject1.data = subObject1;
}

- (void)test_tf_getMethodNameList {
    NSArray *methodArray = [TestObject tf_getMethodNameList];
    NSLog(@"methodArray:%@",methodArray);
    XCTAssertTrue(methodArray);
}

- (void)test_tf_enumeratePropertyListWithBlock {
    
    [TestObject tf_enumeratePropertyListWithBlock:^(NSString * _Nonnull propertyName, NSString * _Nonnull propertyTypeName, Class  _Nullable __unsafe_unretained propertyClass) {
        NSLog(@"propertyName:%@,propertyTypeName:%@,propertyClass:%@",propertyName,propertyTypeName,propertyClass);
        XCTAssertTrue(propertyName.length);
    }];
}

- (void)test_tf_getPropertyNameList {
    NSArray *propertyArray = [TestObject tf_getPropertyNameList];
    NSLog(@"propertyArray:%@",propertyArray);
    XCTAssertTrue(propertyArray);
}

- (void)test_tf_subClasses {
    NSArray *classArray = [UIControl tf_subClasses];
    NSLog(@"classArray:%@",classArray);
    XCTAssertTrue(classArray);
}

- (void)test_tf_propertyValueForName {
    XCTAssertTrue([[_testObject tf_propertyValueForName:@"info"] isEqualToString:@"success"]);
}

- (void)test_tf_propertyValueForKeyPath {
    XCTAssertTrue([[_testObject tf_propertyValueForKeyPath:@"data.name"] isEqualToString:@"Janney"]);
}

- (void)test_tf_isEqualObjectWithKeyPath {
    XCTAssertTrue([_testObject tf_isEqualObject:_testObject1 withKeyPath:@"data.name"]);
}

- (void)test_tf_isPropertiesEqualObject {
    _testObject1.data.age = _testObject.data.age;
    _testObject1.data.sex = _testObject.data.sex;
    BOOL isEqual = [_testObject1 tf_isPropertiesEqualObject:_testObject];
    XCTAssertTrue(isEqual==NO);
}


@end
