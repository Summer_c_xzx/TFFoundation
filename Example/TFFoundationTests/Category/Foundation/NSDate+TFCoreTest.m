//
//  NSDate+TFCoreTest.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/6/28.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSDate+TFCore.h"

@interface NSDate_TFCoreTest : XCTestCase

@property (nonatomic, strong) NSDate *testDate;

@end

@implementation NSDate_TFCoreTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    //date: 2017/6/28 17:20:26
    _testDate = [NSDate dateWithTimeIntervalSince1970:1498641626];
}

- (void)test_tf_year {
    XCTAssertEqual(_testDate.tf_year, 2017);
}

- (void)test_tf_month {
    XCTAssertEqual(_testDate.tf_month, 6);
}

- (void)test_tf_day {
    XCTAssertEqual(_testDate.tf_day, 28);
}

- (void)test_tf_hour {
    XCTAssertEqual(_testDate.tf_hour, 17);
}

- (void)test_tf_minute {
    XCTAssertEqual(_testDate.tf_minute, 20);
}

- (void)test_tf_second {
    XCTAssertEqual(_testDate.tf_second, 26);
}

- (void)test_tf_weekday {
    XCTAssertEqual(_testDate.tf_weekday, 4);
}

- (void)test_tf_isLeapYear {
    XCTAssertTrue(!_testDate.tf_isLeapYear);
}

- (void)test_tf_isLeapMonth {
    XCTAssertTrue(!_testDate.tf_isLeapMonth);
}

- (void)test_tf_isToday {
    XCTAssertTrue(!_testDate.tf_isToday);
}

- (void)test_tf_isYesterday {
    BOOL isTrue = !_testDate.tf_isYesterday;
    XCTAssertTrue(isTrue);
}

- (void)test_startDateOfDay {
    
    NSDate *startDate = [_testDate tf_startDateOfDay];
    
    XCTAssertTrue(startDate.tf_day==28&&startDate.tf_hour==0&&startDate.tf_minute==0&&startDate.tf_second==0);
}

- (void)test_endDateOfDay {
    NSDate *endDate = [_testDate tf_endDateOfDay];
    XCTAssertTrue(endDate.tf_day==28&&endDate.tf_hour==23&&endDate.tf_minute==59&&endDate.tf_second==59);
}

- (void)test_tf_startDateOfWeek {
    NSDate *startDateOfWeek = [_testDate tf_startDateOfWeek];
    XCTAssertTrue(startDateOfWeek.tf_day==26&&startDateOfWeek.tf_hour==0&&startDateOfWeek.tf_minute==0&&startDateOfWeek.tf_second==0);
}

- (void)test_tf_endDateOfWeek {
    NSDate *endDateOfWeek = [_testDate tf_endDateOfWeek];
    XCTAssertTrue(endDateOfWeek.tf_month==7&&endDateOfWeek.tf_day==2&&endDateOfWeek.tf_hour==23&&endDateOfWeek.tf_minute==59&&endDateOfWeek.tf_second==59);
}

- (void)test_tf_startDateOfMonth {
    NSDate *startDateOfMonth = [_testDate tf_startDateOfMonth];
    XCTAssertTrue(startDateOfMonth.tf_day==1&&startDateOfMonth.tf_hour==0&&startDateOfMonth.tf_minute==0&&startDateOfMonth.tf_second==0);
}

- (void)test_tf_endDateOfMonth {
    NSDate *endDateOfMonth = [_testDate tf_endDateOfMonth];
    XCTAssertTrue(endDateOfMonth.tf_day==30&&endDateOfMonth.tf_hour==23&&endDateOfMonth.tf_minute==59&&endDateOfMonth.tf_second==59);
}

- (void)test_tf_startDateOfYear {
    NSDate *startDateOfYear = [_testDate tf_startDateOfYear];
    XCTAssertTrue(startDateOfYear.tf_month==1&&startDateOfYear.tf_day==1&&startDateOfYear.tf_hour==0&&startDateOfYear.tf_minute==0&&startDateOfYear.tf_second==0);
}

- (void)test_tf_endDateOfYear {
    NSDate *endDateOfYear = [_testDate tf_endDateOfYear];
    XCTAssertTrue(endDateOfYear.tf_month==12&&endDateOfYear.tf_day==31&&endDateOfYear.tf_hour==23&&endDateOfYear.tf_minute==59&&endDateOfYear.tf_second==59);
}

- (void)test_tf_dateByAddingYear {
    NSDate *date = [_testDate tf_dateByAddingYear:-1];
    XCTAssertTrue(date.tf_year==2016);
}

- (void)test_tf_dateByAddingMonth {
    NSDate *date = [_testDate tf_dateByAddingMonth:7];
    XCTAssertTrue(date.tf_month==1&&date.tf_year==2018);
}

- (void)test_tf_dateByAddingDay {
    NSDate *date = [_testDate tf_dateByAddingDay:5];
    XCTAssertTrue(date.tf_month==7&&date.tf_day==3);
}

- (void)test_tf_dateByAddingHour {
    NSDate *date = [_testDate tf_dateByAddingHour:-2];
    XCTAssertTrue(date.tf_hour==15);
}

- (void)test_tf_dateByAddingMinute {
    NSDate *date = [_testDate tf_dateByAddingMinute:-21];
    XCTAssertTrue(date.tf_hour==16&&date.tf_minute==59);
}

- (void)test_tf_dateByAddingSecond {
    NSDate *date = [_testDate tf_dateByAddingSecond:-28];
    XCTAssertTrue(date.tf_minute==19&&date.tf_second==58);
}

- (void)test_tf_yearsCompareToDate {
    NSDate *date = [_testDate tf_dateByAddingYear:3];
    XCTAssertTrue([_testDate tf_yearsCompareToDate:date]==3);
}

- (void)test_tf_monthsCompareToDate {
    NSDate *date = [_testDate tf_dateByAddingMonth:-4];
    XCTAssertTrue([_testDate tf_monthsCompareToDate:date]==-4);
}

- (void)test_tf_daysCompareToDate {
    NSDate *date = [_testDate tf_dateByAddingDay:-5];
    NSInteger day = [_testDate tf_daysCompareToDate:date];
    XCTAssertTrue(day==-5);
}

- (void)test_tf_hoursCompareToDate {
    NSDate *date = [_testDate tf_dateByAddingHour:4];
    XCTAssertTrue([_testDate tf_hoursCompareToDate:date]==4);
}

- (void)test_tf_minutesCompareToDate {
    NSDate *date = [_testDate tf_dateByAddingMinute:20];
    XCTAssertTrue([_testDate tf_minutesCompareToDate:date]==20);
}

- (void)test_tf_dateStringWithFormat {
    NSString *dateString = [_testDate tf_dateStringWithFormat:@"年:yyyy,月:MM,MMM,MMMM,日:d,dd,星期:EEE,EEEE,小时:hh,h,HH,H,分钟:mm"];
    NSLog(@"date:%@,\ndateString:%@",_testDate,dateString);
    XCTAssertTrue(dateString);
}

- (void)test_tf_dateISOFormatString {
    NSString *dateString = [_testDate tf_dateISOFormatString];
    NSLog(@"date:%@,\ndateString:%@",_testDate,dateString);
    XCTAssertTrue(dateString);
}

- (void)test_tf_dateYMDHMFormatString {
    NSString *dateString = [_testDate tf_dateYMDHMFormatString];
    NSLog(@"date:%@,\ndateString:%@",_testDate,dateString);
    XCTAssertTrue(dateString);
}

- (void)test_tf_dateYMDFormatString {
    NSString *dateString = [_testDate tf_dateYMDFormatString];
    NSLog(@"date:%@,\ndateString:%@",_testDate,dateString);
    XCTAssertTrue(dateString);
}

- (void)test_tf_dateChineseYMDFormatString {
    NSString *dateString = [_testDate tf_dateChineseYMDFormatString];
    NSLog(@"date:%@,\ndateString:%@",_testDate,dateString);
    XCTAssertTrue(dateString);
}

- (void)test_tf_dateWithDateString {
    NSDate *date = [NSDate tf_dateWithDateString:@"2017.12.01 12:33" format:@"yyyy.MM.dd HH:mm"];
    XCTAssertTrue(date.tf_year==2017&&date.tf_month==12&&date.tf_day==1&&date.tf_hour==12&&date.tf_minute==33);
}

- (void)test_tf_timeString_intervalSecondTimeString {
    NSDate *date = [NSDate date];
    NSString *intervalSecondTimeString = [[date tf_dateByAddingSecond:-10] tf_timeString];
    NSLog(@"intervalSecondTimeString:%@",intervalSecondTimeString);
    XCTAssertTrue([intervalSecondTimeString isEqualToString:@"刚刚"]);
}

- (void)test_tf_timeString_intervalMinuteTimeString {
    NSDate *date = [NSDate date];
    NSString *intervalMinuteTimeString = [[date tf_dateByAddingMinute:-2] tf_timeString];
    NSLog(@"intervalMinuteTimeString:%@",intervalMinuteTimeString);
    XCTAssertTrue([intervalMinuteTimeString hasSuffix:@"分钟前"]);
}

- (void)test_tf_timeString_intervalHourTimeString {
    NSDate *date = [NSDate date];
    NSString *intervalHourTimeString = [[date tf_dateByAddingHour:-2] tf_timeString];
    NSLog(@"intervalHourTimeString:%@",intervalHourTimeString);
    XCTAssertTrue([intervalHourTimeString hasSuffix:@"小时前"]);
}

- (void)test_tf_timeString_todayTimeString {
    NSDate *date = [NSDate date];
    NSString *todayTimeString = [[date tf_dateByAddingHour:-6] tf_timeString];
    NSLog(@"todayTimeString:%@",todayTimeString);
    XCTAssertTrue([todayTimeString hasPrefix:@"今天"]);
}

- (void)test_tf_timeString_yesterdayTimeString {
    NSDate *date = [NSDate date];
    NSString *yesterdayTimeString = [[date tf_dateByAddingDay:-1] tf_timeString];
    NSLog(@"yesterdayTimeString:%@",yesterdayTimeString);
    XCTAssertTrue([yesterdayTimeString hasPrefix:@"昨天"]);
}

- (void)test_tf_timeString_intervalWeekTimeString {
    NSDate *date = [NSDate date];
    NSString *intervalWeekTimeString = [[date tf_dateByAddingDay:-2] tf_timeString];
    NSLog(@"intervalWeekTimeString:%@",intervalWeekTimeString);
    XCTAssertTrue(intervalWeekTimeString);
}

- (void)test_tf_timeString_intervalOverWeekTimeString {
    NSDate *date = [NSDate date];
    NSString *intervalOverWeekTimeString = [[date tf_dateByAddingDay:-8] tf_timeString];
    NSLog(@"intervalOverWeekTimeString:%@",intervalOverWeekTimeString);
    XCTAssertTrue([intervalOverWeekTimeString hasSuffix:@"日"]);
}

- (void)test_tf_timeString_intervalYearTimeString {
    NSDate *date = [NSDate date];
    NSString *intervalYearTimeString = [[date tf_dateByAddingYear:-1] tf_timeString];
    NSLog(@"intervalYearTimeString:%@",intervalYearTimeString);
    XCTAssertTrue([intervalYearTimeString containsString:@"年"]);
}




@end
