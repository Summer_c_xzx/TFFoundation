//
//  NSArray+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/6/26.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSArray+TFCore.h"

@interface NSArray_TFCoreTest : XCTestCase

@property (nonatomic, strong) NSArray *testArray;

@end

@implementation NSArray_TFCoreTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    NSMutableArray *array = [NSMutableArray array];
    for (NSInteger i=0; i<100; i++) {
        [array addObject:@(i)];
    }
    _testArray = [NSArray arrayWithArray:array];
}

- (void)test_tf_objectAtIndex {
    NSArray *array = @[@1,@2];
    XCTAssertEqual([array tf_objectAtIndex:3], nil);
}

- (void)test_tf_stringAtIndex {
    NSArray *array = @[@1,@2];
    XCTAssertTrue([[array tf_stringAtIndex:0
                    ] isEqualToString:@"1"]);
}

- (void)test_tf_numberAtIndexWithNumberStyle {
    
    NSArray *array = @[@12,@"92%"];
    
    //number value
    XCTAssertEqual([array tf_numberAtIndex:0 withNumberStyle:NSNumberFormatterNoStyle], @12);
    //string value
    NSNumber *percentNumber = [array tf_numberAtIndex:1 withNumberStyle:NSNumberFormatterPercentStyle];
    XCTAssertTrue([percentNumber isEqualToNumber:[NSNumber numberWithDouble:0.92]]);
    
}

- (void)test_tf_integerWithIndex {
    NSArray *array = @[@111,@"123"];
    XCTAssertEqual([array tf_integerWithIndex:0], 111);
    XCTAssertEqual([array tf_integerWithIndex:1], 123);
}

- (void)test_tf_boolWithIndex {
    NSArray *array = @[@1,@"0"];
    XCTAssertEqual([array tf_integerWithIndex:0], YES);
    XCTAssertEqual([array tf_integerWithIndex:1], NO);
}

- (void)test_tf_floatWithIndex {
    NSArray *array = @[@12.3,@"4.4"];
    XCTAssertEqual([array tf_floatWithIndex:0], 12.3f);
    XCTAssertEqual([array tf_floatWithIndex:1], 4.4f);
}

- (void)test_tf_doubleWithIndex {
    NSArray *array = @[@12.33,@"4.44"];
    XCTAssertEqual([array tf_doubleWithIndex:0], 12.33);
    XCTAssertEqual([array tf_doubleWithIndex:1], 4.44);
}

- (void)test_tf_enumerateUsingBlockPerformanceWithTFArrayEnumerateTraverse {
    //正序
    [self p_tf_enumerateUsingBlockPerformanceWithOptions:TFArrayEnumerateTraverse];
}

- (void)test_tf_enumerateUsingBlockPerformanceWithTFArrayEnumerateReverse {
    //反序
    [self p_tf_enumerateUsingBlockPerformanceWithOptions:TFArrayEnumerateReverse];
}

- (void)test_tf_enumerateUsingBlockPerformanceWithTFArrayEnumerateConcurrent {
    //并发
    [self p_tf_enumerateUsingBlockPerformanceWithOptions:TFArrayEnumerateConcurrent];
}

- (void)p_tf_enumerateUsingBlockPerformanceWithOptions:(TFArrayEnumerateOptions)options {
    
    [self measureBlock:^{
        [_testArray tf_enumerateWithOptions:options usingBlock:^(id  _Nonnull obj, NSInteger idx) {
            NSLog(@"%@",obj);
        }];
    }];
}

- (void)test_tf_mapUsingBlockWithTFArrayEnumerateTraverse {
    NSArray *mapedArray = [self p_tf_mapedArrayFromMapUsingBlockWithOptions:TFArrayEnumerateTraverse];
    XCTAssertTrue([[mapedArray firstObject] isEqualToString:@"0-0"]);
}

- (void)test_tf_mapUsingBlockWithTFArrayEnumerateReverse {
    
    NSArray *mapedArray = [self p_tf_mapedArrayFromMapUsingBlockWithOptions:TFArrayEnumerateReverse];
    XCTAssertTrue([[mapedArray lastObject] isEqualToString:@"0-0"]);
}

- (void)test_tf_mapUsingBlockWithTFArrayEnumerateConcurrent {
    NSArray *mapedArray = [self p_tf_mapedArrayFromMapUsingBlockWithOptions:TFArrayEnumerateConcurrent];
    XCTAssertTrue([[mapedArray firstObject] isEqualToString:@"0-0"]);
}

- (NSArray *)p_tf_mapedArrayFromMapUsingBlockWithOptions:(TFArrayEnumerateOptions)options {
    
   return [_testArray tf_mapWithOptions:options usingBlock:^id _Nullable(id  _Nonnull obj, NSInteger idx) {
       return [NSString stringWithFormat:@"%zd-%@",idx,obj];
    }];    
}

- (void)test_tf_reduceWithInitialValueWithTFArrayEnumerateTraverse {
    NSString *reducedString = [self p_tf_reduceWithInitialValueWithOptions:TFArrayEnumerateTraverse];
    XCTAssertTrue([reducedString isEqualToString:@"123"]);
}

- (void)test_tf_reduceWithInitialValueWithTFArrayEnumerateReverse {
    NSString *reducedString = [self p_tf_reduceWithInitialValueWithOptions:TFArrayEnumerateReverse];
    XCTAssertTrue([reducedString isEqualToString:@"321"]);
}

- (id)p_tf_reduceWithInitialValueWithOptions:(TFArrayEnumerateOptions)options {
    NSArray *array = @[@1,@2,@3];
    return [array tf_reduceWithInitialValue:@"" option:options usingBlock:^id _Nullable(id  _Nonnull reducedObj, id  _Nonnull obj, NSInteger idx) {
        return [reducedObj stringByAppendingString:[NSString stringWithFormat:@"%@",obj]];
    }];
}

- (void)test_tf_matchObjectWithNSEnumerationReverse {
    XCTAssertEqual([self p_tf_matchObjectWithOptions:NSEnumerationReverse], @2);
}

- (void)test_tf_matchObjectWithNSEnumerationConcurrent {
     XCTAssertEqual([self p_tf_matchObjectWithOptions:NSEnumerationConcurrent], @2);
}

- (id)p_tf_matchObjectWithOptions:(NSEnumerationOptions)options {
    NSArray *array = @[@1,@2,@3];
    return [array tf_matchObjectWithOptions:options predicateBlock:^BOOL(id  _Nonnull obj, NSInteger idx) {
        return [obj isEqualToNumber:@2];
    }];
}

- (void)test_anyWithPredicateBlock {
    NSArray *array = @[@1,@2,@3];
    BOOL anyMatch = [array tf_anyWithPredicateBlock:^BOOL(id  _Nonnull obj, NSInteger idx) {
        return [obj isEqualToNumber:@4];
    }];
    XCTAssertEqual(anyMatch,NO);
}

- (void)test_tf_noneWithPredicateBlock {
    NSArray *array = @[@1,@2,@3];
    BOOL noneMatch = [array tf_noneWithPredicateBlock:^BOOL(id  _Nonnull obj, NSInteger idx) {
        return [obj isEqualToNumber:@4];
    }];
    XCTAssertEqual(noneMatch,YES);
}

- (void)test_tf_allWithPredicateBlock {
    NSArray *array = @[@1,@2,@3];
    BOOL allMatch = [array tf_allWithPredicateBlock:^BOOL(id  _Nonnull obj, NSInteger idx) {
        return [obj integerValue]<3;
    }];
    XCTAssertEqual(allMatch,NO);
}

- (void)test_tf_compareWithArray {
    NSArray *array = @[@1,@2,@3];
    NSArray *array1 = @[@"1",@"2",@"3"];
    
    BOOL compare = [array tf_compareWithArray:array1 usingblock:^BOOL(id  _Nonnull obj1, id  _Nonnull obj2, NSInteger idx) {
        return [obj1 integerValue]==[obj2 integerValue];
    }];
    XCTAssertEqual(compare, YES);
}

- (void)test_tf_filteredWithPredicateBlock {
    NSArray *array = @[@1,@2,@3];
    NSArray *filteredArray = [array tf_filteredWithPredicateBlock:^BOOL(id  _Nonnull obj, NSInteger idx) {
        return [obj integerValue]>=3;
    }];
    XCTAssertEqual([[filteredArray lastObject] integerValue], 3);
}

- (void)test_tf_filteredWithPredicateFormat {
    NSArray *array = @[@1,@2,@3];
    NSArray *filteredArray = [array tf_filteredWithPredicateFormat:@"SELF >=3"];
    XCTAssertEqual([[filteredArray lastObject] integerValue], 3);
}

- (void)test_tf_sortedArrayWithAscending {
    NSArray *array = @[@1,@3,@2];
    NSArray *sortedArray = [array tf_sortedArrayWithAscending:NO];
    XCTAssertEqual([[sortedArray lastObject] integerValue], 1);
}

- (void)test_tf_sortedArrayWithDescriptorDic {
    NSArray *array = @[@{@"name":@"John",@"age":@12},
                       @{@"name":@"Tony",@"age":@3},
                       @{@"name":@"Danny",@"age":@9},
                       ];
    NSArray *sortedArray = [array tf_sortedArrayWithDescriptorDic:@{@"age":@YES}];
    XCTAssertTrue([[sortedArray lastObject][@"name"] isEqualToString:@"John"]);
}

- (void)test_tf_removeObject {
    NSArray *array = @[@1,@3,@2];
    XCTAssertTrue([[[array tf_removeObject:@2] lastObject] integerValue] == 3);
}

- (void)test_tf_removeObjectAtIndex {
    NSArray *array = @[@1,@3,@2];
    XCTAssertTrue([[[array tf_removeObjectAtIndex:2] lastObject] integerValue] == 3);
}

- (void)test_tf_removeFirstObject {
    NSArray *array = @[@1,@3,@2];
    XCTAssertTrue([[[array tf_removeFirstObject] firstObject] integerValue] == 3);
}

- (void)test_tf_removeLastObject {
    NSArray *array = @[@1,@3,@2];
    XCTAssertTrue([[[array tf_removeLastObject] lastObject] integerValue] == 3);
}


@end
