//
//  NSMutableArray+TFCore.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/6/28.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSMutableArray+TFCore.h"

@interface NSMutableArray_TFCoreTest : XCTestCase

@property (nonatomic, strong) NSMutableArray *testArray;

@end

@implementation NSMutableArray_TFCoreTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _testArray = [NSMutableArray arrayWithObjects:@1,@3,@2, nil];
}

- (void)test_tf_filterWithPredicateBlock {
    [_testArray tf_filterWithPredicateBlock:^BOOL(id  _Nonnull obj, NSInteger idx) {
        return [obj integerValue]<=2;
    }];
    XCTAssertTrue(_testArray.count==2);
}

- (void)test_tf_filterWithPredicateFormat {
    [_testArray tf_filterWithPredicateFormat:@"SELF <= 2"];
    XCTAssertTrue(_testArray.count==2);
}

- (void)test_tf_sortWithAscending {
    [_testArray tf_sortWithAscending:YES];
    XCTAssertTrue([[_testArray lastObject] integerValue]==3);
}

- (void)test_tf_sortWithDescriptorDic {
    NSMutableArray *array = [NSMutableArray arrayWithArray:@[
                                                             @{@"name":@"John",@"age":@12},
                                                             @{@"name":@"Tony",@"age":@3},
                                                             @{@"name":@"Danny",@"age":@9},]];
    [array tf_sortWithDescriptorDic:@{@"age":@YES}];
    
    XCTAssertTrue([[array lastObject][@"age"] integerValue]==12);
}

@end
