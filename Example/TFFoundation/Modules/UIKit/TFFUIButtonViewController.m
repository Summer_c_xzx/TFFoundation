//
//  TFFUIButtonViewController.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/18.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "TFFUIButtonViewController.h"

#import <TFFoundation/TFUIButton.h>

@interface TFFUIButtonViewController ()


@end

@implementation TFFUIButtonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
 
    TFUIButton *leftButton = [self p_getButtonWithImagePostion:TFUIButtonImagePostionLeft];
    [self.view addSubview:leftButton];
    
    TFUIButton *rightButton = [self p_getButtonWithImagePostion:TFUIButtonImagePostionRight];
    [self.view addSubview:rightButton];
    
    TFUIButton *topButton = [self p_getButtonWithImagePostion:TFUIButtonImagePostionTop];
    [topButton addTarget:self action:@selector(p_topButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:topButton];
    
    TFUIButton *bottomButtom = [self p_getButtonWithImagePostion:TFUIButtonImagePostionBottom];
    [self.view addSubview:bottomButtom];
    
    
    [leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.view.mas_centerX).mas_offset(-20.0);
        make.centerY.mas_equalTo(self.view.mas_centerY);
    }];
    
    [rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_centerX).mas_offset(20.0);
        make.centerY.mas_equalTo(self.view.mas_centerY);
    }];
    
    [topButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.bottom.mas_equalTo(self.view.mas_centerY).mas_offset(-40.0);
    }];
    
    [bottomButtom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.top.mas_equalTo(self.view.mas_centerY).mas_offset(40.0);
    }];
    
    
}

- (TFUIButton *)p_getButtonWithImagePostion:(TFUIButtonImagePostion)imagePostion {
    TFUIButton *button = [TFUIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor whiteColor];
    button.tf_cornerRadius = 5.0;
    button.tf_borderWidth = 1.0;
    button.tf_borderColor = [UIColor lightGrayColor];
    button.imagePosition = imagePostion;
    switch (imagePostion) {
        case TFUIButtonImagePostionTop: {
            [button setTitle:@"image P Top" forState:UIControlStateNormal];
        }
            break;
        case TFUIButtonImagePostionLeft: {
            [button setTitle:@"image P left" forState:UIControlStateNormal];
        }
            break;
        case TFUIButtonImagePostionRight: {
            [button setTitle:@"image P Right" forState:UIControlStateNormal];
        }
            break;
        case TFUIButtonImagePostionBottom: {
            [button setTitle:@"image P Bottom" forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"icon_emotion"] forState:UIControlStateNormal];
    button.contentEdgeInsets = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
    button.spacingBetweenImageAndTitle = 8.0;
    return button;
}

- (void)p_topButtonAction:(TFUIButton *)button {
    [button setTitle:[[button titleForState:UIControlStateNormal] stringByAppendingString:[button titleForState:UIControlStateNormal]] forState:UIControlStateNormal];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
