//
//  TFFUISliderViewController.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/19.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "TFFUISliderViewController.h"
#import <TFFoundation/TFUISlider.h>

@interface TFFUISliderViewController ()


@end

@implementation TFFUISliderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    TFUISlider *slider = [[TFUISlider alloc] init];
    slider.value = .3;
    slider.minimumTrackTintColor = [UIColor cyanColor];
    slider.maximumTrackTintColor = [UIColor lightGrayColor];
    slider.trackHeight = 1;// 支持修改背后导轨的高度
    slider.thumbColor = slider.minimumTrackTintColor;
    slider.thumbSize = CGSizeMake(14, 14);// 支持修改拖拽圆点的大小
    
    // 支持修改圆点的阴影样式
    slider.thumbShadowColor = [slider.minimumTrackTintColor colorWithAlphaComponent:.3];
    slider.thumbShadowOffset = CGSizeMake(0, 2);
    slider.thumbShadowRadius = 3;
    [self.view addSubview:slider];
    
    [slider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10.0);
        make.top.mas_equalTo(self.view.mas_topMargin).mas_offset(20.0);
        make.right.mas_equalTo(-10.0);
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
