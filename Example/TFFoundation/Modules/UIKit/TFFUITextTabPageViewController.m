//
//  TFFUITextTabPageViewController.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/5/8.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "TFFUITextTabPageViewController.h"

@interface TFFUITextTabPageViewController ()

@end

@implementation TFFUITextTabPageViewController

- (void)didInitialize {
    [super didInitialize];
    self.tabView.dataSourceArray = @[@"全部",@"待支付",@"已支付",@"已取消"];
    self.tabView.textSelectedFont = APPSTYLE.boldFont(20);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (UIViewController *)viewControllerAtPage:(NSInteger)page {
    UIViewController *vc = [[UIViewController alloc] init];
    vc.view.backgroundColor = [UIColor tf_randomColor];
    return vc;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    TFLog(@"");
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
