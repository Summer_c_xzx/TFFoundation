//
//  TFFUIGridViewController.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/23.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "TFFUIGridViewController.h"
#import <TFFoundation/TFUIGridView.h>

@interface TFFUIGridViewController ()

@end

@implementation TFFUIGridViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    TFUIGridView *grideView = [[TFUIGridView alloc] init];
    grideView.columnCount = 3;
    grideView.rowHeight = 80.0;
    grideView.separatorColor = APPSTYLE.lightGrayColor;
    grideView.separatorWidth = 4.0;
    [self.view addSubview:grideView];
    
    for (NSInteger i = 0; i<9; i++) {
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor tf_randomColor];
        [grideView addSubview:view];
    }
    
    
    [grideView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(CGPointZero);
        make.left.mas_equalTo(30.0);
        make.right.mas_equalTo(-30.0);
        make.height.mas_equalTo(240);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    TFLog(@"");
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
