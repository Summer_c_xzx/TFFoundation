//
//  TFFUILabelViewController.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/18.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "TFFUILabelViewController.h"
#import <TFFoundation/TFUILabel.h>

@interface TFFUILabelViewController ()

@end

@implementation TFFUILabelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    TFUILabel *label = [[TFUILabel alloc] init];
    label.backgroundColor = [UIColor cyanColor];
    label.text = @"UILabel support content edges";
    label.textColor = [UIColor redColor];
    label.font = [UIFont systemFontOfSize:14];
    label.contentEdgeInsets = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.);
    [self.view addSubview:label];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(20.0);
        make.top.mas_equalTo(self.view.mas_topMargin).mas_offset(50.0);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
