//
//  TFFUIStateViewController.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/22.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TFFUIStateViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
