//
//  TFFUIPageViewController.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/5/7.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "TFFUIPageViewController.h"

@interface TFFUIPageViewController ()

@end

@implementation TFFUIPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    typeof(self) __weak weakSelf = self;
    UIBarButtonItem *nextPageItem = [UIBarButtonItem tf_barButtonItemWithTitle:@"下一页" actionHandler:^(id  _Nullable sender) {
        [weakSelf selectPage:MIN([weakSelf numbersOfPage]-1, weakSelf.currentPage+1) animated:YES];
    }];
    
    UIBarButtonItem *perviousPageItem = [UIBarButtonItem tf_barButtonItemWithTitle:@"上一页" actionHandler:^(id  _Nullable sender) {
        [weakSelf selectPage:MAX(0, weakSelf.currentPage-1) animated:YES];
    }];
    self.navigationItem.rightBarButtonItems = @[nextPageItem,perviousPageItem];
}

- (NSInteger)numbersOfPage {
    return 4;
}

- (UIViewController *)viewControllerAtPage:(NSInteger)page {
    UIViewController *vc = [[UIViewController alloc] init];
    vc.view.backgroundColor = [UIColor tf_randomColor];
    return vc;
}

- (void)didSelectViewController:(__kindof UIViewController *)viewController atPage:(NSInteger)page {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    TFLog(@"");
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
