//
//  TFFUIStateViewController.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/22.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "TFFUIStateViewController.h"

@interface TFFUIStateViewController ()

@end

@implementation TFFUIStateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//   //loading
//    AppUIAddStateViewInView(APPSTYLE.loadingStateView, self.view);
    //empty
    AppUIAddStateViewInView(APPSTYLE.emptyStateView, self.view);
    //error
//    AppUIAddStateViewInView(APPSTYLE.errorStateView, self.view);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
