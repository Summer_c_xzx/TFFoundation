//
//  TFFUITextViewController.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/19.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "TFFUITextViewController.h"
#import <TFFoundation/TFUITextView.h>

@interface TFFUITextViewController ()


@end

@implementation TFFUITextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    TFUITextView *textView = [[TFUITextView alloc] init];
    textView.placeholder = @"请输入内容";
    textView.font = [UIFont systemFontOfSize:14];
    textView.tf_borderColor = [UIColor lightGrayColor];
    textView.tf_borderWidth = 1.0;
    [self.view addSubview:textView];
    
    [textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10.0);
        make.top.mas_equalTo(self.view.mas_topMargin).mas_offset(10.0);
        make.right.mas_equalTo(-10.0);
        make.height.mas_equalTo(240.0);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
