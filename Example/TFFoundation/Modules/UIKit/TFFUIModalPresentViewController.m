//
//  TFFUIModalPresentViewController.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/5/9.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "TFFUIModalPresentViewController.h"

@interface TFFUIModalPresentViewController ()

@end

@implementation TFFUIModalPresentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.contentView.backgroundColor = APPSTYLE.whiteColor;
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(CGPointZero);
        make.size.mas_equalTo(CGSizeMake(280.0, 150.0));
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    TFLog(@"");
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
