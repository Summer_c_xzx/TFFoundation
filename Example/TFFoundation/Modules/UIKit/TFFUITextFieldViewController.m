//
//  TFFUITextFieldViewController.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/19.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "TFFUITextFieldViewController.h"
#import <TFFoundation/TFUITextField.h>

@interface TFFUITextFieldViewController ()



@end

@implementation TFFUITextFieldViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    TFUITextField *textField = [[TFUITextField alloc] init];
    textField.placeholder = @"请输入文本";
    textField.placeholderColor = [UIColor cyanColor];
    textField.tf_borderColor = [UIColor lightGrayColor];
    textField.tf_borderWidth = 1.0;
    textField.textInsets = UIEdgeInsetsMake(0.0, 10.0, 0.0, 10.0);
    [self.view addSubview:textField];
    
    [textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(10.0);
        make.right.mas_equalTo(-10.0);
        make.top.mas_equalTo(self.view.mas_topMargin).mas_offset(20.0);
        make.height.mas_equalTo(40.0);
    }];
    
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
