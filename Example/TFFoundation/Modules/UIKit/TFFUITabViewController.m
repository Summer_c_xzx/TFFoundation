//
//  TFFUITabViewController.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/26.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "TFFUITabViewController.h"
#import <TFFoundation/TFUITabView.h>


@interface TFFUITabViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,TFUITabViewDelegate>

@property(nonatomic, strong) TFUITextTabView *textTabView;

@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation TFFUITabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.textTabView];
    [self.view addSubview:self.collectionView];
    
    [_textTabView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self.view.mas_topMargin);
        make.left.right.mas_equalTo(0.0);
        make.height.mas_equalTo(40.0);
    }];
    
    [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0.0);
        make.top.mas_equalTo(self.textTabView.mas_bottom);
        make.bottom.mas_equalTo(0.0);
    }];
}

#pragma mark - UICollectionViewDelegate,UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _textTabView.dataSourceArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView tf_dequeueReusableCellWithClass:[UICollectionViewCell class] forIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor tf_randomColor];
    return cell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.textTabView associatedScrollView:scrollView contentOffsetDidChange:scrollView.contentOffset];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSInteger index = scrollView.contentOffset.x/CGRectGetWidth(scrollView.frame);
    [self.textTabView selectTabAtIndex:index animated:NO];
}

#pragma mark - TFUITabViewDelegate

- (void)tabView:(TFUITabView *)tabView didSelectTabAtIndex:(NSInteger)index animated:(BOOL)animated {
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:animated];
}

#pragma mark - lazy load.

- (TFUITextTabView *)textTabView {
    if (!_textTabView) {
        _textTabView = [[TFUITextTabView alloc] initWithDataSourceArray:@[@"全部",@"待支付",@"已支付",@"已取消"]];
        _textTabView.backgroundColor = [UIColor cyanColor];
        _textTabView.delegate = self;
        [_textTabView selectTabAtIndex:2 animated:NO];
    }
    return _textTabView;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumLineSpacing = 0.0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.itemSize = CGSizeMake(kScreenWidth, kScreenHeight);
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.pagingEnabled = YES;
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([UICollectionViewCell class])];
    }
    return _collectionView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    TFLog(@"");
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
