//
//  TFFBaseTableViewItemCell.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/22.
//Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import <TFTableViewManager/TFTableViewItemCell.h>
#import "TFFBaseTableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface TFFBaseTableViewItemCell : TFTableViewItemCell

@property (nonatomic, strong) TFFBaseTableViewItem *tableViewItem;


@end

NS_ASSUME_NONNULL_END
