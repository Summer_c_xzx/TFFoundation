//
//  TFFBaseTableViewItem.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/22.
//Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import <TFTableViewManager/TFTableViewItem.h>
#import "TFFBaseTableModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TFFBaseTableViewItem : TFTableViewItem

@property (nonatomic,strong) TFFBaseTableModel *model;


@end

NS_ASSUME_NONNULL_END
