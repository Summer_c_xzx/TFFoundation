//
//  TFFBaseTableViewItemCell.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/22.
//Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "TFFBaseTableViewItemCell.h"

@interface TFFBaseTableViewItemCell ()

@property (nonatomic, strong) UILabel *nameLabel;

@end


@implementation TFFBaseTableViewItemCell

@dynamic tableViewItem;

#pragma mark - Cell life cycle

- (void)cellLoadSubViews {
    [super cellLoadSubViews];
    //add subviews and it's constraints at here.
    [self.contentView addSubview:self.nameLabel];
}

- (void)cellWillAppear {
    [super cellWillAppear];
    //update subviews UI at here.
    _nameLabel.text = self.tableViewItem.model.name;
}

#pragma mark - lazy load.

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = APPSTYLE.grayColor;
        _nameLabel.font = TFUIFont(14);
    }
    return _nameLabel;
}


//- (void)cellDidDisappear {
//    [super cellDidDisappear];
//    //clear subviews UI at here.
//
//}

@end
