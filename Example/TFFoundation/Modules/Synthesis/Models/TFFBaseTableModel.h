//
//  TFFBaseTableModel.h
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/22.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TFFBaseTableModel : NSObject

@property(nonatomic, assign) NSInteger id;

@property(nonatomic, strong) NSString *name;

@end

NS_ASSUME_NONNULL_END
