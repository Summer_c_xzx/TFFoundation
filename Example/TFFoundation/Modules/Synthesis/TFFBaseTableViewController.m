//
//  TFFBaseTableViewController.m
//  TFFoundation
//
//  Created by 夏之祥 on 2019/4/22.
//  Copyright © 2019 TFAppleWork-Summer. All rights reserved.
//

#import "TFFBaseTableViewController.h"
//items
#import "TFFBaseTableViewItem.h"

@interface TFFBaseTableViewController ()

@end

@implementation TFFBaseTableViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.request.path = @"organization/page/organization/list";
        self.request.responseJsonObjectMapClassName = @"TFFBaseTableModel";
    }
    return self;
}

- (void)viewDidLoad {
    [self.request.params setObject:@1 forKey:@"sortType"];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}


- (Class)tableViewManagerItemClass {
    return [TFFBaseTableViewItem class];
}

- (void)tableViewManagerDidSelectItem:(__kindof TFTableViewItem *)item atIndexPath:(NSIndexPath *)indexPath {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
