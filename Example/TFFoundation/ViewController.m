//
//  ViewController.m
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/8.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import "ViewController.h"
#import "TFFUIModalPresentViewController.h"

@interface ViewController ()

@property(nonatomic, strong) NSArray *modulesArray;

@end

@implementation ViewController

- (instancetype)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"TFFoundatrion";
    _modulesArray = @[@{@"title":@"UIKit",@"contents":@[@{@"name":@"TFUILabel",@"vc":@"TFFUILabelViewController",},
                                                        @{@"name":@"TFUIButton",@"vc":@"TFFUIButtonViewController"},
                                                        @{@"name":@"TFUITextView",@"vc":@"TFFUITextViewController"},
                                                        @{@"name":@"TFUITextField",@"vc":@"TFFUITextFieldViewController"},
                                                        @{@"name":@"TFUISlider",@"vc":@"TFFUISliderViewController"},
                                                        @{@"name":@"TFUIStateView",@"vc":@"TFFUIStateViewController"},
                                                        @{@"name":@"TFUIGridView",@"vc":@"TFFUIGridViewController"},
                                                        @{@"name":@"TFFUITabView",@"vc":@"TFFUITabViewController"},
                                                        @{@"name":@"TFFUIPageView",@"vc":@"TFFUIPageViewController"},
                                                        @{@"name":@"TFFUITextTabPage",@"vc":@"TFFUITextTabPageViewController"},
                                                        @{@"name":@"TFFUIModalPresent",@"vc":@"TFFUIModalPresentViewController"},

                                                        ]},
                      @{@"title":@"项目模板",@"contents":@[@{@"name":@"BaseTable",@"vc":@"TFFBaseTableViewController",},
                                                       ]}
                      ];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _modulesArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *contentsArray = [_modulesArray[section]objectForKey:@"contents"];
    return contentsArray.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return _modulesArray[section][@"title"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    NSArray *contentsArray = [_modulesArray[indexPath.section]objectForKey:@"contents"];
    NSDictionary *infoDic = contentsArray[indexPath.row];
    cell.textLabel.text = infoDic[@"name"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *infoDic = [_modulesArray[indexPath.section]objectForKey:@"contents"][indexPath.row];
    if ([infoDic[@"name"] isEqualToString:@"TFFUIModalPresent"]) {
        
        [UIAlertController tf_showActionSheetInViewController:self withTitle:@"选择动画类型" cancelButtonTitle:@"取消" destructiveButtonTitle:nil ohterButtonsTitles:@[@"Fade",@"Popup",@"SpringPopup",@"Slide"] actionsHandlerBlock:^(UIAlertController * _Nonnull alertController, UIAlertAction * _Nonnull action, NSInteger index) {
            TFUIModalPresentAnimationStyle animationStyle = TFUIModalPresentAnimationStyleFade;
            if ([action.title isEqualToString:@"Fade"]) {
                animationStyle = TFUIModalPresentAnimationStyleFade;
            }
            else if ([action.title isEqualToString:@"Popup"]) {
                animationStyle = TFUIModalPresentAnimationStylePopup;
            }
            else if ([action.title isEqualToString:@"SpringPopup"]) {
                animationStyle = TFUIModalPresentAnimationStyleSpringPopup;
            }
            else if ([action.title isEqualToString:@"Slide"]) {
                animationStyle = TFUIModalPresentAnimationStyleSlide;
            }
            [TFFUIModalPresentViewController showInView:self.tableView withAnimationStyle:animationStyle];
        }];
        
    }
    else {
        TFOpenURL(infoDic[@"vc"], @{kTFOpenURLViewControllerTitleKey:infoDic[@"name"]});
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end



