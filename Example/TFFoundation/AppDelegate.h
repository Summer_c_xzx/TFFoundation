//
//  AppDelegate.h
//  TFFoundation
//
//  Created by TFAppleWork-Summer on 2017/3/8.
//  Copyright © 2017年 TFAppleWork-Summer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

